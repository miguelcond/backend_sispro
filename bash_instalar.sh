FRONTEND_URL="http://10.0.100.31"

BACKEND_URLAPI="http://10.0.100.31/ws"

# ==================================
sudo apt-get install ca-certificates
sudo apt-get install build-essential
sudo apt-get install curl

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install v10.24.1

npm install

git config --global user.name "usuario"
git config --global user.email usuario@yopmail.com
git config --list

#cp src/config/config-example.js src/config/config.js
#cp src/config/config-example.json src/config/config.json

echo "Configuracion del sistema:"
echo "Editar el archivo: src/config/config.js"
echo ""
echo "Configuracion de base de datos:"
echo "Editar el archivo: src/config/config.json"
echo ""
echo "=========================="
echo "Descpues de configurar correctamente todo lo necesario"
echo "Ejecutar:"
echo "    - npm run setup --prod"
echo "    - npm run update"
echo "    - npm run start"
