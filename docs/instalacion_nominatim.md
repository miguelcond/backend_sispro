## Instalación de Nominatim para Debian Jessie

Nominatim es una herramienta para buscar datos OSM por nombre y dirección y generar direcciones sintéticas de puntos OSM.

#### Creación de usuario nominatim.
La instalación se realizá sobre un usuario del sistema.

Si se requiere se puede adicionar e instalar sobre el usuario nominatim.
```
sudo adduser nominatim
sudo adduser nominatim sudo
sudo su - nominatim
cd
```

#### Actualizar los repositorios de debian.
```
sudo apt-get update
```
#### Instalación de dependencias.
```
sudo apt-get install -y build-essential libxml2-dev \
libpq-dev libbz2-dev libtool automake libproj-dev \
libboost-dev libboost-system-dev libboost-filesystem-dev \
libboost-thread-dev libexpat-dev gcc proj-bin libgeos-c1 \
libgeos++-dev libexpat-dev php5 php-pear php5-pgsql php5-json \
php-db libapache2-mod-php5 postgresql postgis postgresql-contrib \
postgresql-9.4-postgis-2.1 postgresql-server-dev-9.4 wget nano
```
#### Descargar la aplicación Nominatim y descomprimirlo.
```
wget http://www.nominatim.org/release/Nominatim-2.5.1.tar.bz2
tar xvf Nominatim-2.5.1.tar.bz2
```
#### Cambiar nombre de la carpeta donde se encuentra la aplicación Nominatim, crear el archivo makefile e instalar.
```
mv Nominatim-2.5.1 src
cd src
./configure
make
```
#### Para personalizar la instalación crear el archivo local.php en la carpeta `settings/`.
```
nano settings/local.php
```
#### Agregar las siguientes lineas.
```
<?php

// Versión de PostgresSql instalado en el servidor, cambiar en caso de que sea otra
@define('CONST_Postgresql_Version', '9.4');

// Versión del PostGis instalado en el servidor, cambiar en caso de que sea otra
@define('CONST_Postgis_Version', '2.1');

// Configuración del sitio web o ip donde sera desplegado, cambiar en caso de que sea otra
@define('CONST_Website_BaseURL', 'http://127.0.0.1/nominatim/');
?>
```
Guardar y salir.

#### Creación de usuario postgres este usuario tiene que ser superusuario de PostgreSql

```
sudo -u postgres createuser -s nominatim
```
#### Crear usuario para el sitio web.

Crear el usuario para el sitio web www-data como un rol de base de datos PostgreSQL.
```
createuser -SDR www-data
```
#### Dando permisos de ejecución y ingreso a la carpeta donde se encuentra la aplicación.
```
chmod +x ~/src
chmod +x ~/src/module
```
#### Descargando Planet File de la región de Bolivia.
```
wget http://download.geofabrik.de/south-america/bolivia-latest.osm.pbf
```
#### Corrigiendo error de incompatibilidad de codificación (UTF8).
```
sudo -u postgres psql -c "UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1';"
sudo -u postgres psql -c "DROP DATABASE template1;"
sudo -u postgres psql -c "CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UNICODE';"
sudo -u postgres psql -c "UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1';"
```
#### Importamos la Planet File descargado de Bolivia.
```
./utils/setup.php --osm-file bolivia-latest.osm.pbf --all  2>&1 | tee setup.log
```
#### Añadiendo los códigos y nombres de la región de Bolivia en los indices de búsqueda.
```
./utils/specialphrases.php --countries > data/specialphrases_countries.sql
psql -d nominatim -f data/specialphrases_countries.sql
```
#### Crear el directorio nominatim con permisos donde esta alojado el sitio web en `/var/www/html/`.
```
sudo mkdir -m 755 /var/www/html/nominatim
sudo chown nominatim /var/www/html/nominatim
```
#### Crear sitio web.
```
./utils/setup.php --create-website /var/www/html/nominatim
```
#### Una vez finalizado la instalación la aplicación web este esta levantada en la dirección. `http://127.0.0.1/nominatim/`