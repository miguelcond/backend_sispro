// 'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('menu', [

      // --------------------- ADMINISTRACIÓN -------------------------------
      // 1
      { nombre: 'ADMINISTRACIÓN', descripcion: 'Administración', orden: 100, ruta: '', icono: 'settings', method_get: false, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 2
      { nombre: 'USUARIOS', descripcion: 'Administración de usuarios', orden: 1, ruta: 'usuarios', icono: 'group', method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', fid_menu_padre: 1, _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 3
      { nombre: 'RUTAS', descripcion: 'Administración de rutas', orden: 2, ruta: 'rutas', icono: 'code', method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', fid_menu_padre: 1, _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 4
      { nombre: 'ROLES', descripcion: 'Administración de roles', orden: 3, ruta: 'roles', icono: 'credit_card', method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', fid_menu_padre: 1, _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 5
      { nombre: 'MENÚS', descripcion: 'Administración de menús', orden: 4, ruta: 'menus', icono: 'menu', method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', fid_menu_padre: 1, _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 6
      { nombre: 'PROYECTOS', descripcion: 'Bandeja de Solicitudes', orden: 200, ruta: '', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 7
      { nombre: 'PENDIENTES', descripcion: 'Bandeja de Solicitudes Pendientes', orden: 1, ruta: 'pendientes', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 6, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 8
      { nombre: 'MIS PROYECTOS', descripcion: 'Bandeja de Solicitudes Procesadas', orden: 2, ruta: 'mis-proyectos', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 6, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 9
      { nombre: 'PROYECTOS', descripcion: 'Administracion de Proyectos', orden: 5, ruta: 'admin-proyectos', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 10
      { nombre: 'MIS PENDIENTES', descripcion: 'Bandeja de Solicitudes Pendientes', orden: 3, ruta: 'proyectos-pendientes', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 6, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 11
      { nombre: 'MIS PROCESADOS', descripcion: 'Bandeja de Solicitudes Procesadas', orden: 4, ruta: 'proyectos-registrados', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 6, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 12
      { nombre: 'REPORTE GENERAL', descripcion: 'Reporte General', orden: 300, ruta: '', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 13
      { nombre: 'CSV', descripcion: 'Ver/Exportar Reporte General', orden: 1, ruta: 'reporte-general', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 12, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 14
      { nombre: 'UNIDADES', descripcion: 'Administración de unidades', orden: 5, ruta: 'admin-parametricas', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 15
      { nombre: 'CONFIGURACION', descripcion: 'Administración de Organización Institucional', orden: 6, ruta: 'admin-org-instituciones', icono: 'list', method_get: true, method_post: true, method_put: false, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 16
      { nombre: 'TECHO PRESUPUESTARIO', descripcion: 'Administración de Techos presupuestarios por municipio', orden: 7, ruta: 'admin-techos-presupuestarios', icono: 'list', method_get: true, method_post: true, method_put: true, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // 17
      // { nombre: 'ESTADOS', descripcion: 'Administración de estados del flujo', orden: 8, ruta: 'admin-estados', icono: 'list', method_get: true, method_post: false, method_put: true, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
    ], {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
