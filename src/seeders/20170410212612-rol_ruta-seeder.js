// 'use strict';

module.exports = {
  up(queryInterface) {
    // ADMIN
    let rolesRutasArray = [];
    for (let i = 1; i <= 27; i ++) {
      const obj1 = {
        fid_ruta: i,
        fid_rol: 1,
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      };
      rolesRutasArray.push(obj1);
    }
    let objs = [
      { fid_ruta: 8, fid_rol: 2, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 3, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 4, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 5, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 6, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 7, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 8, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 9, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 4, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 5, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 9, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 4, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 5, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 8, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 9, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 4, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 5, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 9, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 10, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 4, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 5, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 9, method_get: true, method_post: true, method_put: false, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 10, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 2, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 4, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 5, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 6, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 7, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 8, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 9, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 2, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 4, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 5, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 6, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 7, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 8, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 9, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 2, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 4, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 5, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 6, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 7, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 8, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 9, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 2, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 4, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 5, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 6, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 7, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 8, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 9, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 17, fid_rol: 2, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 17, fid_rol: 3, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 17, fid_rol: 4, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 17, fid_rol: 5, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 17, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 18, fid_rol: 4, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 18, fid_rol: 5, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 18, fid_rol: 7, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 19, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 20, fid_rol: 8, method_get: false, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 3, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 4, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 5, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 9, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 10, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Modificar contraseña
      // { fid_ruta: 22, fid_rol: 1, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 2, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 3, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 4, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 5, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 6, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 7, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 9, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 10, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Boleta
      { fid_ruta: 23, fid_rol: 6, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 23, fid_rol: 8, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },

      // Admin proyectos
      // { fid_ruta: 24, fid_rol: 1, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 24, fid_rol: 12, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },

      // rol_ruta para FDI
      // Usuarios
      { fid_ruta: 7, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Proyectos
      { fid_ruta: 8, fid_rol: 11, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 12, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 13, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 14, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 15, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 16, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 8, fid_rol: 18, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Modulos
      { fid_ruta: 9, fid_rol: 12, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 14, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 15, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 16, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 9, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Items
      { fid_ruta: 10, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 16, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 10, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Avances de supervision
      { fid_ruta: 11, fid_rol: 12, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 13, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 16, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 17, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 11, fid_rol: 18, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Fotos
      { fid_ruta: 12, fid_rol: 11, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 12, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 13, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 16, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 17, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 12, fid_rol: 18, method_get: true, method_post: true, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Parametricas
      { fid_ruta: 13, fid_rol: 11, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 12, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 14, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 15, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 16, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 13, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Departamentos
      { fid_ruta: 14, fid_rol: 11, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 14, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Provincias
      { fid_ruta: 15, fid_rol: 11, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 15, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Municipios
      { fid_ruta: 16, fid_rol: 11, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 16, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Personas
      { fid_ruta: 17, fid_rol: 13, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 17, fid_rol: 18, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Estados
      { fid_ruta: 18, fid_rol: 11, method_get: true, method_post: true, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 18, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 18, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Computos metricos
      { fid_ruta: 21, fid_rol: 12, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 16, method_get: true, method_post: true, method_put: true, method_delete: true, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 21, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Modificar contraseña
      { fid_ruta: 22, fid_rol: 11, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 12, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 13, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 14, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 15, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 16, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 17, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 22, fid_rol: 18, method_get: false, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Reportes
      { fid_ruta: 25, fid_rol: 12, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Contenido de formularios
      { fid_ruta: 26, fid_rol: 11, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 12, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 13, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 14, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 15, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 16, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 17, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 26, fid_rol: 18, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      // Techo presupuestario
      // { fid_ruta: 27, fid_rol: 1, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 27, fid_rol: 13, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { fid_ruta: 27, fid_rol: 18, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() }
    ];
    rolesRutasArray = rolesRutasArray.concat(objs);
    return queryInterface.bulkInsert('rol_ruta', rolesRutasArray, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
