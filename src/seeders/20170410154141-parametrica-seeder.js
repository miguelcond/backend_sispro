// 'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('parametrica', [
      // Tipos de Documentos de identidad
      {
        codigo: 'TD_CI',
        tipo: 'TIPO_DOCUMENTO',
        nombre: 'CI',
        descripcion: 'Cédula de Identidad',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TD_PASS',
        tipo: 'TIPO_DOCUMENTO',
        nombre: 'Contrastación',
        descripcion: 'Contrastación de datos',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TP_CIF',
        tipo: 'TIPO_PROYECTO',
        nombre: 'CIF',
        descripcion: '',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TP_CIFE',
        tipo: 'TIPO_PROYECTO',
        nombre: 'CIFE',
        descripcion: '',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TE_INSTITUCION',
        tipo: 'TIPO_EQUIPO',
        nombre: 'INSTITUCION',
        descripcion: 'Equipo Institución',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TE_BENEFICIARIO',
        tipo: 'TIPO_EQUIPO',
        nombre: 'BENEFICIARIO',
        descripcion: 'Equipo Beneficiario',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'SUPERVISOR',
        tipo: 'TIPO_EQUIPO',
        nombre: 'Supervisor de obras',
        descripcion: 'Equipo Supervisor',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'FISCAL',
        tipo: 'TIPO_EQUIPO',
        nombre: 'Fiscal validador de planillas',
        descripcion: 'Equipo Supervisor',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'CG_SUPERVISOR',
        tipo: 'CARGO',
        nombre: 'Supervisor',
        descripcion: 'Supervisor Beneficiario',
        orden: 1,
        referencia: 'TE_BENEFICIARIO',
        tag: '9',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'CG_FISCAL',
        tipo: 'CARGO',
        nombre: 'Fiscal',
        descripcion: 'Fiscal Beneficiario',
        orden: 2,
        referencia: 'TE_BENEFICIARIO',
        tag: '10',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'CG_TECNICO',
        tipo: 'CARGO',
        nombre: 'Técnico Responsable',
        descripcion: 'Técnico responsable',
        orden: 3,
        referencia: 'TE_INSTITUCION',
        tag: '4',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'CG_ENCARGADO',
        tipo: 'CARGO',
        nombre: 'Jefe de la Unidad de Revisión',
        descripcion: 'Encargado de asignación del técnico responsable',
        orden: 4,
        referencia: 'TE_INSTITUCION',
        tag: '5',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_METRO',
        tipo: 'UNIDAD',
        nombre: 'm',
        descripcion: 'Metro lineal - ML',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_METRO_CUAD',
        tipo: 'UNIDAD',
        nombre: 'm2',
        descripcion: 'Metro cuadrado - M2',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_METRO_CUB',
        tipo: 'UNIDAD',
        nombre: 'm3',
        descripcion: 'Metro cúbico - M3',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_PIEZA',
        tipo: 'UNIDAD',
        nombre: 'pza',
        descripcion: 'Pieza - PZA',
        orden: 4,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_PUNTO',
        tipo: 'UNIDAD',
        nombre: 'pto',
        descripcion: 'Punto - PTO',
        orden: 5,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_GLOBAL',
        tipo: 'UNIDAD',
        nombre: 'glb',
        descripcion: 'Global - GLB',
        orden: 6,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_TONELADA',
        tipo: 'UNIDAD',
        nombre: 't',
        descripcion: 'Tonelada',
        orden: 7,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_LITRO',
        tipo: 'UNIDAD',
        nombre: 'l',
        descripcion: 'Litro',
        orden: 8,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_CABEZA',
        tipo: 'UNIDAD',
        nombre: 'cabeza',
        descripcion: 'Cabeza',
        orden: 9,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_JUEGO',
        tipo: 'UNIDAD',
        nombre: 'jgo',
        descripcion: 'Juego',
        orden: 10,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_TALLER',
        tipo: 'UNIDAD',
        nombre: 'taller',
        descripcion: 'Taller',
        orden: 11,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'UN_HECTAREA',
        tipo: 'UNIDAD',
        nombre: 'ha',
        descripcion: 'Hectarea',
        orden: 12,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_DEPORTE',
        tipo: 'SECTOR',
        nombre: 'Deportes',
        descripcion: 'Deportes',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_EDUCACION',
        tipo: 'SECTOR',
        nombre: 'Educación',
        descripcion: 'Educación',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_SOCIAL',
        tipo: 'SECTOR',
        nombre: 'Infraestructura Social',
        descripcion: 'Infraestructura Social',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_VIAL',
        tipo: 'SECTOR',
        nombre: 'Infraestructura Vial',
        descripcion: 'Infraestructura Vial',
        orden: 4,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_PRODUCTIVO',
        tipo: 'SECTOR',
        nombre: 'Productivos',
        descripcion: 'Productivos',
        orden: 5,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_RIEGO',
        tipo: 'SECTOR',
        nombre: 'Riego',
        descripcion: 'Riego',
        orden: 6,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_SALUD',
        tipo: 'SECTOR',
        nombre: 'Salud',
        descripcion: 'Salud',
        orden: 7,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'ST_SANEAMIENTO',
        tipo: 'SECTOR',
        nombre: 'Saneamiento Básico',
        descripcion: 'Saneamiento Básico',
        orden: 8,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TF_EMPRESA',
        tipo: 'TIPO_FOTO_AVANCE',
        nombre: 'Foto avance empresa',
        descripcion: 'Foto avance empresa',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TF_SUPERVISION',
        tipo: 'TIPO_FOTO_AVANCE',
        nombre: 'Foto avance supervisión',
        descripcion: 'Foto avance supervisión',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TB_CORRECTA_INVERSION',
        tipo: 'TIPO_BOLETA',
        nombre: 'Buena inversión de anticipo',
        descripcion: 'Boleta/Póliza de correcta inversión de anticipo',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        codigo: 'TB_CUMPLIMIENTO_CONTRATO',
        tipo: 'TIPO_BOLETA',
        nombre: 'Cumplimiento de contrato',
        descripcion: 'Boleta/Póliza de cumplimiento de contrato',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'TM_ORDEN_TRABAJO',
        tipo: 'TIPO_MODIFICACION',
        nombre: 'Orden de Trabajo',
        descripcion: 'Modificación de Orden de Trabajo',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'TM_ORDEN_CAMBIO',
        tipo: 'TIPO_MODIFICACION',
        nombre: 'Orden de Cambio',
        descripcion: 'Modificación de Orden de Cambio',
        orden: 2,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'TM_CONTRATO_MODIFICATORIO',
        tipo: 'TIPO_MODIFICACION',
        nombre: 'Contrato Modificatorio',
        descripcion: 'Contrato Modificatorio',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // Tipo de proyecto
      {
        codigo: 'OP_TP_PRODUCTIVO',
        tipo: 'OP_TIPO_PROYECTO',
        nombre: 'Productivo',
        descripcion: 'Proyecto productivo',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'OP_TP_RIEGO',
        tipo: 'OP_TIPO_PROYECTO',
        nombre: 'Riego',
        descripcion: 'Proyecto de riego',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'OP_TP_PUENTES',
        tipo: 'OP_TIPO_PROYECTO',
        nombre: 'Puentes',
        descripcion: 'Proyecto de contrcción de puentes',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // Tipo de financiamiento
      {
        codigo: 'OP_TF_PUBLICA',
        tipo: 'OP_TIPO_FINANCIAMIENTO',
        nombre: 'Pública',
        descripcion: 'Pública',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'OP_TF_PRIVADA',
        tipo: 'OP_TIPO_FINANCIAMIENTO',
        nombre: 'Privada',
        descripcion: 'Privada',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'OP_TF_MIXTA',
        tipo: 'OP_TIPO_FINANCIAMIENTO',
        nombre: 'Mixta',
        descripcion: 'Mixta',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // Tamaño de proyecto
      {
        codigo: 'OP_TP_MEDIANO',
        tipo: 'OP_TAMANIO_PROYECTO',
        nombre: 'Medianos',
        descripcion: 'Proyectos medianos',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'OP_TP_MENORES',
        tipo: 'OP_TAMANIO_PROYECTO',
        nombre: 'Menores',
        descripcion: 'Proyectos menores',
        orden: 3,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'JEFE_DEPTO_TECNICO',
        tipo: 'ORG_INSTITUCION',
        nombre: 'Grover Oblitas Roselio',
        descripcion: 'Jefe del Departamento Tecnico de Proyectos',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'DIRECTOR_GRAL_EJECUTIVO',
        tipo: 'ORG_INSTITUCION',
        nombre: 'Lic. Braulio Yucra Duarte',
        descripcion: 'Director General Ejecutivo',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }, {
        codigo: 'ST_PROD_AGRICOLA',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Agrícola',
        descripcion: 'AGRICOLA',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_APICULTURA',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Apicultura',
        descripcion: 'APICULTURA',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_CAMELIDOS',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Camélidos',
        descripcion: 'CAMELIDOS',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_GANADO_MENOR',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Ganado Menor',
        descripcion: 'GANADO MENOR',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_BOVINO',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Bovino',
        descripcion: 'BOVINO',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_LACTEOS',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Lácteos',
        descripcion: 'LACTEOS',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_PISCICOLA',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Piscícola',
        descripcion: 'PISCICOLA',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_TRANSFORMACION',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Transformación',
        descripcion: 'TRANSFORMACION',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_MAQUINARIA',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Maquinaria',
        descripcion: 'MAQUINARIA',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_INFRAESTRUCTURA',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Infraestructura',
        descripcion: 'INFRAESTRUCTURA',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },{
        codigo: 'ST_PROD_AVICOLA',
        tipo: 'ST_PROY_PRODUCTIVO',
        nombre: 'Avicola',
        descripcion: 'AVICOLA',
        orden: 1,
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }
    ], {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
