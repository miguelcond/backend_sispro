import request from 'request';
import config from '../../config/config';
import logger from '../../lib/logger';

/**
 * Obtiene documentación de experiencia de la persona basado en un código.
 * @param  {Cadena} codigo Codigo de reporte para el proyecto
 * @return {Promesa}      Retorna una promesa
 */
module.exports.obtenerDocumentacionExp = (codigo, matricula) => {
  const empresas = config().empresas;
  return new Promise((resolve, reject) => {
    let url = `${empresas.url}${empresas.path}formularios/${matricula}?codigo=${codigo}`;
    logger.info(`[GET] ${url}`);
    request({
      url,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: `${empresas.token}`
      },
      json: true
    }, (err, res) => {
      if (err) {
        switch (err.code) {
          case 'ENOTFOUND':
          case 'EAI_AGAIN':
          case 'ECONNRESET':
          case 'ECONNREFUSED':
            err.message = 'No se pudo establecer conexión con empresas.';
            break;
        }
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};
