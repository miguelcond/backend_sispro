import request from 'request';
import config from '../../config/config';
import logger from '../../lib/logger';

/**
 * Obtiene datos de la empresa basado en la matrícula.
 * @param  {Cadena} matrícula de la empresa
 * @return {Promesa} Retorna una promesa
 */
module.exports.obtenerEmpresa = (matricula) => {
  return new Promise((resolve, reject) => {
    let url = `${config().fundempresa.url}${config().fundempresa.path}matriculas/${matricula}/empresa`;
    logger.info(`[GET] ${url}`);
    request({
      url,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: config().fundempresa.tokenKong
      },
      json: true
    }, (err, res) => {
      if (err) {
        switch (err.code) {
          case 'ENOTFOUND':
          case 'EAI_AGAIN':
          case 'ECONNRESET':
          case 'ECONNREFUSED':
            err.message = 'No se pudo establecer conexión con fundempresa.';
            break;
        }
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};

/**
 * Obtiene datos de la empresa basado en el nit.
 * @param  {Cadena} nit de la empresa
 * @return {Promesa} Retorna una promesa
 */
module.exports.obtenerMatriculas = (nit) => {
  return new Promise((resolve, reject) => {
    let url = `${config().fundempresa.url}${config().fundempresa.path}nit/${nit}/matriculas`;
    logger.info(`[GET] ${url}`);
    request({
      url,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: config().fundempresa.tokenKong
      },
      json: true
    }, (err, res) => {
      if (err) {
        switch (err.code) {
          case 'ENOTFOUND':
          case 'EAI_AGAIN':
          case 'ECONNRESET':
          case 'ECONNREFUSED':
            err.message = 'No se pudo establecer conexión con fundempresa.';
            break;
        }
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};
