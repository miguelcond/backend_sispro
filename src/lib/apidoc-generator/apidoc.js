'use strict';
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');
/**
* Librería que permite crear generar un archivo con la documentación de los servicios.
* EL formato de salida esta basado en los estándares que establece APIDOCJS  http://apidocjs.com/
* @author Wilmer Alex Quispe Quispe
* @version 1.0.0
*
* Caracteristicas:
* - El formato de los campos es el mismo que usa Sequelize.
* - Puede cargar los campos de los modelos a partir de una instancia de la clase Sequelize.
* - Es posible crear campos personalizados (campos que no se encuentran en ningun modelo).
*/
class apidoc {
  /**
  * Configuración inicial del apidoc
  * @param  {Object} properties Propiedades del objeto.
  * @param  {String} properties.filePath Nombre del archivo que almacenará toda la documentación.
  * @param  {Boolean} properties.log Valor que indica si se mostrarán las rutas de los servicios creados.
  */
  static create (properties = {}) {
    apidoc.filePath = properties.output || path.resolve(path.dirname(require.main.filename), './APIDOC.js');
    apidoc.log = (typeof properties.log === 'boolean') ? properties.log : true;
    deleteFile(apidoc.filePath);
  }

  /**
  * Crea el apidoc para un servicio de tipo GET.
  * @param  {String} routePath Ruta del servicio.
  * @param  {Object} options Opciones del servicio.
  */
  static get (routePath, options) {
    apidoc.route('get', routePath, options);
  }

  /**
  * Crea el apidoc para un servicio de tipo POST.
  * @param  {String} routePath Ruta del servicio.
  * @param  {Object} options Opciones del servicio.
  */
  static post (routePath, options) {
    apidoc.route('post', routePath, options);
  }

  /**
  * Crea el apidoc para un servicio de tipo PUT.
  * @param  {String} routePath Ruta del servicio.
  * @param  {Object} options Opciones del servicio.
  */
  static put (routePath, options) {
    apidoc.route('put', routePath, options);
  }

  /**
  * Crea el apidoc para un servicio de tipo DELETE.
  * @param  {String} routePath Ruta del servicio.
  * @param  {Object} options Opciones del servicio.
  */
  static delete (routePath, options) {
    apidoc.route('delete', routePath, options);
  }

  /**
  * Crea el apidoc para un servicio.
  * @param  {String} method Método.
  * @param  {String} routePath Ruta del servicio.
  * @param  {Object} options Opciones del servicio.
  * @param  {String} options.name Nombre único del recurso.
  * @param  {String} options.grouṕ Grupo al que pertenece el recurso.
  * @param  {String} options.description Descripción de lo que hace el recurso.
  * @param  {String} options.version Versión el recurso.
  * @param  {Object} options.input Objeto que contiene el formato de entrada de datos.
  * @param  {Object} options.output Objeto que contiene el formato de salida de datos.
  */
  static route (method, routePath, options) {
    if (!apidoc.filePath) { return; }
    const METHOD = method.toLowerCase();
    const PATH = routePath;
    const NAME = options.name || `[${METHOD}] ${PATH}`;
    const GROUP = options.group || 'Default';
    const DESCRIPTION = options.description || NAME;
    const VERSION = options.version || '1.0.0';
    const INPUT = options.input || { headers: {}, params: {}, body: {}, query: {} };
    const OUTPUT = options.output || { headers: {}, body: {} };
    let apidocContent = readFile(apidoc.filePath) || '';

    apidocContent += encabezado(METHOD, PATH, NAME, GROUP, DESCRIPTION, VERSION);

    apidocContent += createApidoc('', INPUT.headers, '@apiHeader', 'Input - headers');
    apidocContent += createApidoc('', INPUT.params, '@apiParam', 'Input - params');

    apidocContent += createApidoc('', INPUT.body, '@apiParam', 'Input - body');
    if (INPUT.body && ((Object.keys(INPUT.body).length > 0) || (Array.isArray(INPUT.body)))) {
      const example = createExample(INPUT.body);
      apidocContent += `* @apiParamExample {json} Ejemplo Petición: Todos los campos posibles\n${example}`;
    }
    apidocContent += createApidoc('', OUTPUT.body, '@apiSuccess', 'Output - body');
    if (OUTPUT.body && ((Object.keys(OUTPUT.body).length > 0) || (Array.isArray(OUTPUT.body)))) {
      const example = createExample(OUTPUT.body);
      const code = '200 Ok';
      let response = `* HTTP/1.1 ${code}`;
      apidocContent += `* @apiSuccessExample {json} Respuesta Exitosa: ${code}\n${response}\n${example}`;
    }
    apidocContent += `*/\n`;
    createFile(apidoc.filePath, apidocContent);
    if (apidoc.log) {
      console.log(` [${METHOD.toUpperCase()}] ${PATH}`);
    }
  }
}

function createExample (body) {
  let result = '';
  let example = JSON.stringify(createData(body, false), null, 2);
  example.split('\n').forEach(line => {
    result += `* ${line}\n`;
  });
  return result;
}

function encabezado (method, path, name, group, description, version) {
  return `
/**
* @api {${method}} ${path} ${name}
* @apiName ${name}
* @apiGroup ${group}
* @apiDescription ${description}
* @apiVersion ${version}
`;
}

function isSequelizeField (prop) {
  if (prop && prop._modelAttribute && (prop._modelAttribute === true)) {
    return true;
  }
  return false;
}

function createApidoc (fullprop, obj, apidocProperty, type) {
  let apidocContent = '';
  if (Array.isArray(obj)) {
    if (fullprop !== '') {
      apidocContent += `* ${apidocProperty} (${type}) {Object[]} ${fullprop} Lista de objetos **${fullprop}**\n`;
    }
    apidocContent += createApidoc(fullprop, obj[0], apidocProperty, type);
    return apidocContent;
  } else {
    if (typeof(obj)==='object') {
      if (fullprop !== '') {
        apidocContent += `* ${apidocProperty} (${type}) {Object} ${fullprop} Datos del objeto **${fullprop}**\n`;
      }
      for (let prop in obj) {
        let field = obj[prop];
        let property = (fullprop !== '') ? `${fullprop}.${prop}` : prop;
        if (isSequelizeField(field)) {
          // Si se trata de un parámetro de salida (output), solamente devolvemos el tipo de dato.
          let typeField = apidocType(field, apidocProperty === '@apiSuccess');
          // Si se trata de un parámetro de salida (output), solo muestra el nombre del campo sin ningún otro argumento.
          let fieldName = (apidocProperty === '@apiSuccess') ? `[${property}]` : apidocProp(field, property);
          const description = createDescription(field);
          apidocContent += `* ${apidocProperty} (${type}) {${typeField}} ${fieldName} ${description}\n`;
        } else {
          apidocContent += createApidoc(property, obj[prop], apidocProperty, type);
        }

      }
    } else {
      if (fullprop !== '') {
        apidocContent += `* ${apidocProperty} (${type}) {String} ${fullprop} **${fullprop}** ${obj}\n`;
      }
    }
  }
  return apidocContent;
}

function createDescription (field) {
  if (field.comment) {
    return field.comment;
  }
  if (field.xlabel) {
    return field.xlabel;
  }
  return field.name;
}

function apidocProp (field, prop) {
  prop = (typeof field.defaultValue !== 'undefined') ? `${prop}=${field.defaultValue}` : prop;
  return (prop.allowNull === true) ? `[${prop}]` : prop;
}

function apidocType (field, onlyType) {
  if (field.type.key === 'STRING') {
    if (onlyType === true) { return 'String'; }
    return (field.type._length === 1) ? `String{1 caracter}` : `String{de 1 a ${field.type._length} caracteres}`;
  }
  if (field.type.key === 'ENUM') {
    if (onlyType === true) { return 'String'; }
    return `String=${field.values.toString()}`;
  }
  if (field.type.key === 'INTEGER') { return 'Number'; }
  if (field.type.key === 'BOOLEAN') { return 'Boolean'; }
  if (field.type.key === 'ARRAY') {
    if (field.type.type.key === 'STRING') { return 'String[]'; }
    if (field.type.type.key === 'INTEGER') { return 'Number[]'; }
    return 'Example[]';
  }
  if (field.type.key === 'JSON') { return 'JSON'; }
  if (field.type.key === 'JSONB') { return 'JSONB'; }
  return 'String';
}

function createData (obj, onlyRequired) {
  let data;
  let isArray = Array.isArray(obj);
  if (isArray) {
    return [createData(obj[0], onlyRequired)];
  }
  data = {};
  for (let prop in obj) {
    let field = obj[prop];
    if (isSequelizeField(field)) {
      if (onlyRequired === true) {
        if (field.required) {
          data[prop] = getExample(field);
        }
      } else {
        data[prop] = getExample(field);
      }
    } else {
      data[prop] = createData(obj[prop], onlyRequired);
    }
  }
  return data;
}

function getExample (field) {
  if (field.example) { return field.example; }
  if (field.defaultValue) { return field.defaultValue; }
  if (field.type.key === 'STRING') { return 'text'; }
  if (field.type.key === 'INTEGER') { return 1; }
  if (field.type.key === 'BOOLEAN') { return false; }
  if (field.type.key === 'ENUM') { return field.values[0]; }
  if (field.type.key === 'JSON') { return { json: { data: 'value' } }; }
  if (field.type.key === 'JSONB') { return { jsonb: { data: 'value' } }; }
  if (field.type.key === 'ARRAY') {
    if (field.type.type.key === 'STRING') { return ['A', 'B']; }
    if (field.type.type.key === 'INTEGER') { return [1, 2]; }
    return ['example'];
  }
  return 'example';
}

function deleteFile (filePath) {
  try {
    mkdirp.sync(path.dirname(filePath));
    fs.unlinkSync(filePath);
  } catch (error) { }
}

function readFile (filePath) {
  try {
    mkdirp.sync(path.dirname(filePath));
    return fs.readFileSync(filePath, 'utf-8');
  } catch (error) { }
}

function createFile (filePath, content) {
  try {
    mkdirp.sync(path.dirname(filePath));
    fs.writeFileSync(filePath, content);
  } catch (error) { }
}

module.exports = apidoc;
