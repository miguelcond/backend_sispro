/**
* Clase que permite crear el objeto options para realizar consultas sequelize.
*/
class SequelizeOptions {

  /**
  * Libreria que permite crear un objeto options para realizar consultas sequelize.
  * Los tipos de datos válidos son:
  *   str (STRING)
  *   int (INTEGER)
  *   float (FLOAT)
  *   date (DATE_ONLY)
  *   bool (BOOLEAN)
  *   enum (ENUM)
  * Los tipos de llaves válidos son:
  *   pk (PRIMARY_KEY)
  *   fk (FOREIGN_KEY)
  * Cada campo puede contener un tipo de dato y/o un tipo de llave.
  * Por defecto siempre se incluirán las llaves primarias y foráneas.
  * Para obtener todos los campos puede utilizar el filtro recurso?fields=ALL.
  *
  * Pueden realizarse filtros con múltiples campos, separados por comas.
  * Ejemplo:
  *   ../recurso?nro=124
  *   ../recurso?profesion=ingeniero,persona.nombre=carlos
  *   ../recurso?profesion=ingeniero,persona.nombre=carlos,persona.usuario.estado=ACTIVO
  *
  * Pueden aplicarse filtros de ordenación sobre cada campo.
  * para ordenar de forma ascendente, utilizar ../recurso?order=profesion
  * para ordenar de forma descendente, utilizar ../recurso?order=-persona.usuario.estado
  *
  let modeloSalida = {
    model: 'representante',
    fields: {
      id_representante: ['int','pk'],
      num_id: ['str'],
      nombre: ['str'],
      estado: ['str'],
      fid_persona: ['int', 'fk'],
      persona: {
        model: 'persona',
        fields: {
          id_persona: ['int','pk'],
          documento_identidad: ['str'],
          fecha_nacimiento: ['date'],
          nombres: ['str'],
          fid_parametro: ['int', 'fk'],
          tipo_documento_identidad: {
            model: 'parametro',
            fields: {
              id_parametro: ['int', 'pk'],
              sigla: ['str'],
              nombre: ['str'],
            },
          },
        },
      },
    }
  };
  * @param {String[]} query Lista que contiene los campos y filtros (req.query).
  * @param {Object} models Objeto que contiene todos los modelos de sequelize (app.src.db.models).
  * @param {Object} modeloSalida Objeto que describe los campos que puede devolver.
  * @return {Object}
  */
  static createOptions(query, models, modeloSalida) {
    const limit = (query.limit && parseInt(query.limit) >= 0) ? parseInt(query.limit) : 50;
    const offset = (query.page && parseInt(query.page) > 0) ? ((parseInt(query.page) - 1) * limit) : 0;
    const distinct = true;
    let options = { limit, offset };
    let fields = query.fields ? query.fields.split(',') : ['ALL'];
    this.createInclude(modeloSalida.fields, options, models, '', query, fields);
    if (query.order) {
      if (query.order.charAt(0) == '-') {
        options.order = `"${query.order.substring(1, query.order.length)}" DESC`;
      } else if (query.order.charAt(0) == '+') {
        options.order = `"${query.order.substring(1, query.order.length)}" ASC`;
      } else {
        options.order = `"${query.order}" ASC`;
      }
    }
    return options;
  }

  /**
  * Adiciona los modelos relacionados.
  */
  static createInclude(modeloSalida, options, models, location, query, fields) {
    options.attributes = [];
    for (let prop in modeloSalida) {
      let propiedadesCampo = modeloSalida[prop];
      let location2 = (location !== '') ? `${location}.${prop}` : prop;
      if (Array.isArray(modeloSalida[prop])) {
        let adicionado = false;
        if (modeloSalida[prop].includes('pk') || modeloSalida[prop].includes('fk')) {
          options.attributes.push(prop);
          adicionado = true;
        } else {
          for (let i in fields) {
            let field = fields[i];
            if ((field === location2) || (field === 'ALL')) {
              options.attributes.push(prop);
              adicionado = true;
              break;
            }
          }
        }
        if (adicionado === true) {
          for (let fieldFullname in query) {
            if (fieldFullname === location2) {
              if (!options.where) options.where = {};
              if (propiedadesCampo.includes('str')) {
                options.where[prop] = { $iLike: `%${query[location2]}%` };
              }
              if (propiedadesCampo.includes('int')) {
                let datoParseado = parseInt(query[location2]);
                if (isNaN(datoParseado)) {
                  throw new Error(`El valor del filtro '${location2}' debe ser de tipo INTEGER`);
                }
                options.where[prop] = datoParseado;
              }
              if (propiedadesCampo.includes('float')) {
                let datoParseado = parseFloat(query[location2]);
                if (isNaN(datoParseado)) {
                  throw new Error(`El valor del filtro '${location2}' debe ser de tipo FLOAT`);
                }
                options.where[prop] = datoParseado;
              }
              if (propiedadesCampo.includes('bool')) {
                let datoParseado = query[location2] == 'true';
                options.where[prop] = datoParseado;
              }
              if (propiedadesCampo.includes('enum')) {
                options.where[prop] = query[location2];
              }
              break;
            }
          }
        }
        continue;
      }

      let mostrarField = false;
      for (let i in fields) {
        let field = fields[i];
        if (field.startsWith(location2) || (field === 'ALL')) {
          mostrarField = true;
        }
      }

      if (mostrarField) {
        options.include = options.include || [];
        let modelName = modeloSalida[prop].model;
        let includeModel = {
          model: models[modelName],
          as: prop,
        };
        this.createInclude(modeloSalida[prop].fields, includeModel, models, location2, query, fields);
        options.include.push(includeModel);
      }
    }
  }

  // Crear objeto options para realizar la consulta, a partir del query enviado en la URL
  static createQuery(options, query, models, tableName) {
    // Crear objetos
    if (!options || typeof(options)!=='object') {
      options = {};
    }
    if (!options.where || typeof(options.where)!=='object') {
      options.where = {};
    }
    options.limit = (query.limit && parseInt(query.limit) >= 0) ? parseInt(query.limit) : 50;
    options.offset = (query.page && parseInt(query.page) > 0) ? ((parseInt(query.page) - 1) * options.limit) : 0;
    // Parsear order
    if (query.order && models && tableName) {
      let order = 'ASC';
      if (query.order.charAt(0) == '-') {
        order = 'DESC';
        query.order = query.order.substring(1, query.order.length);
      } else if (query.order.charAt(0) == '+') {
        query.order = query.order.substring(1, query.order.length);
      }
      let model2,model3;
      let asociation = query.order.split('.');
      // atributos de tablas asociadas para 3 niveles
      switch (asociation.length) {
        case 1:
          if (models[tableName] && models[tableName].attributes[asociation[0]]) {
            options.order = [[`${asociation[0]}`,`${order}`]];
          }
          break;
        case 2: // 2 niveles
          model2 = models[tableName].associations[asociation[0]];
          if (model2 && model2.target && model2.target.attributes[asociation[1]]) {
            // options.order = [[{model:models.supervision, as:`supervisiones`},   `estado`,          `${order}`]];
            options.order = [[{model:model2.target,as:`${asociation[0]}`},`${asociation[1]}`,`${order}`]];
          }
          break;
        case 3: // 3 niveles
          model2 = models[tableName].associations[asociation[0]];
          if (model2 && model2.target) {
            model3 = model2.target.associations[asociation[1]];
            if (model3 && model3.target && model3.target.attributes[asociation[2]]) {
              // options.order = [[{model:models.supervision, as:`supervisiones`}, {model:models.estado,as:`estado_actual`},`nombre`, `${order}`]];
              options.order = [[{model:model2.target,as:`${asociation[0]}`},{model:model3.target,as:`${asociation[1]}`},`${asociation[2]}`,`${order}`]];
            }
          }
          break;
      }
    }
    // Parsear filters
    let exclude = ['order','limit','page','rol','roles'];
    for (let campo in query) {
      if(exclude.indexOf(campo)<0) {
        // obtener asociaciones
        let asociation = campo.split('.');
        if (asociation.length>1) {
          switch (asociation.length) {
            case 3: // 3 niveles
              for (let i in options.include) {
                if (options.include[i].as==asociation[0]) {
                  for (let j in options.include[i].include) {
                    // Verificar si existen las asociaciones y el atributo
                    if (options.include[i].include[j].as==asociation[1]
                      && models[tableName].associations[asociation[0]].target.associations[asociation[1]].target.attributes[asociation[2]]) {
                      if (!options.include[i].include[j].where) options.include[i].include[j].where = {};
                      options.include[i].include[j].where[asociation[2]] = { $iLike: `%${query[campo]}%` }
                    }
                  }
                }
              }
              break;
            case 2: // 2 niveles
              for (let i in options.include) {
                if (options.include[i].as==asociation[0]
                  && models[tableName].associations[asociation[0]].target.attributes[asociation[1]]) {
                  if (!options.include[i].where) options.include[i].where = {};
                  options.include[i].where[asociation[1]] = { $iLike: `%${query[campo]}%` }
                }
              }
              break;
          }
        } else {
          if(tableName && models && models[tableName]) {
            const tipo = models[tableName].rawAttributes[campo] ? models[tableName].rawAttributes[campo].type.constructor.key : undefined;

            // TODO considerar otros tipos de datos del ORM
            switch (tipo) {
              case 'STRING':
                options.where[campo] = { $iLike: `%${query[campo]}%` };
                break;
              case 'INTEGER':
                if (isNaN(parseInt(query[campo]))) {
                  break;
                }
                options.where[campo] = { $eq: parseInt(query[campo]) };
                break;
              case 'FLOAT':
                if (isNaN(parseFloat(query[campo]))) {
                  break;
                }
                options.where[campo] = { $eq: parseFloat(query[campo]) };
                break;
              case 'DATE':
                options.where[campo] = { $eq: `${query[campo]}` };
                break;
              case 'ENUM':
                options.where[campo] = { $eq: `${query[campo]}` };
                break;
            }
            // if (tipo && tipo !== 'STRING') {
            //   options.where[campo] = { $eq: `${query[campo]}` };
            // } else {
            //   options.where[campo] = { $iLike: `%${query[campo]}%` };
            // }
          }
        }
      }
    }
    return options;
  }

  /**
  * Optimiza el resultado, devloviendo únicamente los datos que se especifican en la query.
  * @param {String[]} query Lista que contiene los campos y filtros (req.query).
  * @param {Object[]} data Lista de objetos que contiene los datos del modelo de Salida.
  * @param {Object} modeloSalida Modelo de salida.
  */
  static createResult(query, data, modeloSalida) {
    let fields = query.fields ? query.fields.split(',') : ['ALL'];
    if (Array.isArray(data)) {
      let result = []
      for (let i in data) {
        let origen = data[i];
        let destino = {};
        this.copy(origen, destino, modeloSalida.fields, '', fields);
        result.push(destino);
      }
      return result;
    } else {
      let result = {}
      this.copy(data, result, modeloSalida.fields, '', fields);
      return result;
    }
  }

  static copy(origen, destino, modeloSalida, location, fields) {
    for (let prop in modeloSalida) {
      let location2 = (location !== '') ? `${location}.${prop}` : prop;
      let mostrarField = false;
      for (let i in fields) {
        let field = fields[i];
        if (field.startsWith(location2) || (field === 'ALL')) {
          mostrarField = true;
        }
      }
      if (mostrarField) {
        if (Array.isArray(modeloSalida[prop])) {
          if (typeof origen[prop] != 'undefined') {
            destino[prop] = origen[prop];
          }
          continue;
        }
        if (origen[prop]) {
          if (Array.isArray(origen[prop])) {
            destino[prop] = [];
            for (let k in origen[prop]) {
              destino[prop][k] = {};
              this.copy(origen[prop][k], destino[prop][k], modeloSalida[prop].fields, location2, fields);
            }
          } else {
            destino[prop] = {};
          }
          this.copy(origen[prop], destino[prop], modeloSalida[prop].fields, location2, fields);
        }
      }
    }
  }
}

module.exports = SequelizeOptions
