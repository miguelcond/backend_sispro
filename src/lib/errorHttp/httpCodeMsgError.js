'use strict';

function HttpCodeMsgError(code, message) {
  this.name = 'HttpCodeMsgError';
  this.code = code;
  this.message = (message || '');
  // this.stack = (new Error()).stack;
}

HttpCodeMsgError.prototype = new Error;
HttpCodeMsgError.prototype.constructor = HttpCodeMsgError;


HttpCodeMsgError.prototype.sendResponse = function(res) {
  var self = this;
  console.log("Revisando e mensaje", self.message);
  res.status(self.code).json({
    tipoMensaje: 'ERROR',
    mensaje: self.message
  });
};

module.exports = HttpCodeMsgError;
