module.exports = app => {
  if (process.env.APIDOC === 'true') {
    console.log(' APIDOC generado exitosamente.');
    process.exit(0);
  }
  const config = app.src.config.config;
  if (process.env.NODE_ENV !== 'test') {
    app.src.db.sequelize.sync().done(() => {
      if (process.env.FORCE || false) {
        console.log('------------BASE DE DATOS CREADA--------------');
        process.exit(0);
      } else {
        app.listen(app.get('port'), () => {
          console.log('\x1b[36m%s\x1b[0m', `
                        ___
                     .-'   \`'.
                    /         \\
                    |         ;
                    |         |           ___.--,
           _.._     |0) ~ (0) |    _.---'\`__.-( (_.
    __.--'\`_.. '.__.\\    '--. \\_.-' ,.--'\`     \`""\`
   ( ,.--'\`   ',__ /./;   ;, '.__.'\`    __
   _\`) )  .---.__.' / |   |\\   \\__..--""  """--.,_
  \`---' .'.''-._.-'\`_./  /\\ '.  \\ _.-~~~\`\`\`\`~~~-._\`-.__.'
        | |  .' _.-' |  |  \\  \\  '.               \`~---\`
         \\ \\/ .'     \\  \\   '. '-._)
          \\/ /        \\  \\    \`=.__\`~-.
          / /\\         \`) )    / / \`"".\`\\
    , _.-'.'\\ \\        / /    ( (     / /
     \`--~\`   ) )    .-'.'      '.'.  | (
            (/\`    ( (\`          ) )  '-;
             \`      '-;         (-'

        .::: SEGUIMIENTO DE PROYECTOS :::.
`);
          console.info(`Iniciando servidor en el puerto ${app.get('port')} `);
          console.info(`API ejecutandose en: http://localhost:${app.get('port')}${config.api.main}`);
          console.info(`API-REST(Autogenerado) ejecutandose en: http://localhost:${app.get('port')}${config.api.main}${config.api.crud}`);
        });
      }
    });
  } else {
    app.src.db.sequelize.sync().done(() => {
      console.log('------------BASE DE DATOS CREADA--------------');
      if (process.env.FORCE || false) {
        process.exit(0);
      }
    });
  }
};
