module.exports = {
  up(queryInterface) {
    let equipos = [];
    // items de los modulos
    // tomar en cuanta que son 3 por proyectos 800
    let num;
    for(let i=0;i<900;i++) {
      equipos.push({
        fid_proyecto: i+1, //proyecto 1
        tipo_equipo: 'TE_UPRE',
        fid_usuario: 8,//tecnico
        fid_rol: 3,
        cargo: 'CG_ENCARGADO',
        estado: 'ACTIVO',
        _usuario_creacion: 8, // encargado regional
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
      equipos.push({
        fid_proyecto: i+1, //proyecto 1
        tipo_equipo: 'TE_UPRE',
        fid_usuario: 20, // tecnico
        fid_rol: 5,
        cargo: 'CG_TECNICO',
        estado: 'ACTIVO',
        _usuario_creacion: 8, //encargado_regional
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
    }

    return queryInterface.bulkInsert('equipo_tecnico', equipos, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
