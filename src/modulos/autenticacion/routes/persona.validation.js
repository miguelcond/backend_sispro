import BaseJoi from 'joi';
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  get: {
    params: {
      ci: Joi.string().regex(/^\d+$/).required(),
      fecha: Joi.date().format('YYYY-MM-DD').required()
    },
    query: {
      tipo: Joi.number().allow([1,2]),
      campo: Joi.object().keys({
        nombres: Joi.string().required(),
      }).optional()
    }
  },
  getContrastacion: {
    params: {
    },
    query: {
      tipo: Joi.number().allow([1,2]),
      // campo: Joi.string(),
      campo: Joi.object().keys({
        // ci: Joi.string().required(),
        // complemento: Joi.string().allow(''),
        nombres: Joi.string().required(),
        // primer_apellido: Joi.string().allow(''),
        // segundo_apellido: Joi.string().allow(''),
        // fecha: Joi.string(),
      }).required(),
    }
  }
};
