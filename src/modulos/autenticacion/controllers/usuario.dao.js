import crypto from 'crypto';
import shortid from 'shortid';
import util from '../../../lib/util';
import Reporte from '../../../lib/reportes';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.usuario = {};
  const models = app.src.db.models;
  const usuarioModel = app.src.db.models.usuario;
  const usuarioRolModel = app.src.db.models.usuario_rol;

  async function crearRecuperarUsuarioEmpresa (nit) {
    const USUARIO_EMPRESA = await models.usuario.findOne({
      where: { nit }
    });
    if (USUARIO_EMPRESA) {
      return USUARIO_EMPRESA;
    }
    const ROL_EMPRESA = util.json(await app.dao.rol.getPorCodigo('EMPRESA'));
    const USUARIO = await models.usuario.create({
      usuario: shortid.generate(),
      nit: nit,
      fcod_departamento: ['01', '02', '03', '04', '05', '06', '07', '08', '09'],
      _usuario_creacion: 1,
      _fecha_creacion: new Date(),
      _fecha_modificacion: new Date()
    });
    await models.usuario_rol.create({
      fid_rol: ROL_EMPRESA.id_rol,
      fid_usuario: USUARIO.id_usuario,
      _usuario_creacion: 1,
      _fecha_creacion: new Date(),
      _fecha_modificacion: new Date()
    });
    return USUARIO;
  }

  // async function crearRecuperar (persona, idRol, cod, idUsuario, t) {
  async function crearRecuperar (persona, user, t) {
    let result = await app.dao.persona.crearRecuperar(persona, user.idUsuario, t);
    let usuario = await usuarioModel.findOne({
      where: { fid_persona: result.id_persona },
      transaction: t
    });
    if (usuario) {
      console.log(user.cod);
      if (!usuario.fcod_departamento.includes(user.cod.substr(0,2))) {
        usuario.fcod_departamento.push(user.cod);
        await usuarioModel.update({
          fcod_departamento: usuario.fcod_departamento
        }, {
          where: { fid_persona: result.id_persona },
          transaction: t
        });
      }
      let usuarioRol = await usuarioRolModel.findOne({
        where: {
          fid_rol: user.idRol,
          fid_usuario: usuario.id_usuario
        },
        transaction: t
      });
      if (!usuarioRol) {
        await usuarioRolModel.create({
          fid_rol: user.idRol,
          fid_usuario: usuario.id_usuario,
          _usuario_creacion: user.idUsuario
        }, {
          transaction: t
        });
      }
    } else {
      let _usuario = result.numero_documento;
      if(result.complemento && result.complemento_visible) _usuario = `${result.numero_documento}-${result.complemento}`;

      usuario = await usuarioModel.create({
        fid_persona: result.id_persona,
        fid_institucion: user.idInstitucion,
        usuario: _usuario,
        contrasena: '',
        fcod_departamento: user.cod === 'TODOS' ? ['01', '02', '03', '04', '05', '06', '07', '08', '09'] : [user.cod],
        cargo: '',
        _usuario_creacion: user.idUsuario
      }, {
        transaction: t
      });
      await usuarioRolModel.create({
        fid_rol: user.idRol,
        fid_usuario: usuario.id_usuario,
        _usuario_creacion: user.idUsuario
      }, {
        transaction: t
      });
    }
    return usuario;
  }

  async function asignarRoles (pDatos, pUsuario, pTr) {
    const usuarioRolesCrear = [];
    const roles = pDatos.roles;
    if (!roles || !roles.length || roles.length === 0) {
      throw new Error('Debe seleccionar al menos un rol.');
    }
    for (const rol in roles) {
      usuarioRolesCrear.push({
        fid_rol: roles[rol],
        fid_usuario: pUsuario.id_usuario,
        _usuario_creacion: pDatos._usuario_creacion
      });
    }
    return usuarioRolModel.bulkCreate(usuarioRolesCrear, pTr);
  }

  async function crearUsuario (pDatos, t) {
    const datos = JSON.parse(JSON.stringify(pDatos));
    datos._usuario_creacion = datos.audit_usuario.id_usuario;

    let persona = await app.dao.persona.crearRecuperar(datos.persona, datos._usuario_creacion, t);
    let usuario = await usuarioModel.findOne({
      where: { fid_persona: persona.id_persona }
    }, {transaction: t});
    if (usuario) throw new Error('Ya existe la persona con el usuario respectivo.');
    persona = JSON.parse(JSON.stringify(persona));
    const password = shortid.generate();
    let strUsuario = persona.numero_documento;
    if (persona.complemento && persona.complemento_visible) {
      strUsuario += `-${persona.complemento}`;
    }
    let usuarioCreado = await usuarioModel.create({
      fid_persona: persona.id_persona,
      usuario: strUsuario,
      fid_institucion: app.src.config.config.sistema.idInstitucion || 1,
      contrasena: crypto.createHash('md5').update(password).digest('hex'),
      _usuario_creacion: datos._usuario_creacion,
      fcod_departamento: datos.fcod_departamento,
      cargo: datos.cargo
    }, {
      transaction: t
    });

    usuarioCreado = JSON.parse(JSON.stringify(usuarioCreado));
    usuarioCreado.persona = persona;
    usuarioCreado.contrasena = password;
    await asignarRoles(datos, usuarioCreado, { transaction: t });
    await enviarCorreoNuevaContrasenia(usuarioCreado);
  }

  async function enviarCorreoNuevaContrasenia (usuario) {
    const PARA = usuario.persona.correo;
    const TITULO = 'Usuario habilitado';
    const reporte = new Reporte('correo_nueva_contrasenia');
    const datos = {
      persona: usuario.persona,
      cargo: 'Técnico en sistemas',
      usuario: usuario.usuario,
      contrasena: usuario.contrasena,
      urlLogo: app.src.config.config.sistema.urlLogo,
      urlLogin: app.src.config.config.sistema.urlLogin
    };
    const HTML = reporte.template(datos);
    await util.enviarEmail(PARA, TITULO, HTML);
  }

  async function actualizarContrasena (idUsuario, pass, idUsuarioModificacion) {
    const contrasenaNueva = crypto.createHash('md5').update(pass).digest('hex');
    await models.usuario.update({
      contrasena: contrasenaNueva,
      _usuario_modificacion: idUsuarioModificacion
    }, {
      where: {
        id_usuario: idUsuario
      }
    });
    return pass;
  }

  async function existe (numeroDocumento, fechaNacimiento) {
    let USUARIO = await models.usuario.findOne({
      include: [{
        model: models.persona,
        as: 'persona',
        where: {
          numero_documento: numeroDocumento
        },
        required: true
      }]
    });
    if (!USUARIO) {
      return false;
    }
    if ((new Date(USUARIO.persona.fecha_nacimiento)).getTime() === (new Date(fechaNacimiento)).getTime()) {
      return true;
    }
    return false;
  }

  async function getPorId (idUsuario) {
    const USUARIO = models.usuario.findById(idUsuario);
    if (!USUARIO) {
      throw new errors.ValidationError(`No existe el usuario indicado.`);
    }
    return USUARIO;
  }

  async function getUsuarioContrasena (idUsuario, usuario, contrasena) {
    let contrasenaMD5 = crypto.createHash('md5').update(contrasena).digest('hex');
    let USUARIO = models.usuario.findOne({
      attributes: ['id_usuario'],
      where: {
        id_usuario: idUsuario,
        usuario,
        contrasena: contrasenaMD5
      }
    });
    if (!USUARIO) {
      throw new errors.ValidationError(`No puede modificar la contraseña.`);
    }
    return USUARIO;
  }

  async function getUsuarioPersona (idUsuario) {
    let USUARIO = models.usuario.findOne({
      attributes: ['id_usuario','usuario','cargo'],
      where: {
        id_usuario: idUsuario
      },
      include: [
        {
          model: models.persona,
          attributes: ['id_persona','nombres','primer_apellido','segundo_apellido','correo'],
          as: 'persona',
          required: true
        }
      ]
    });
    if (!USUARIO) {
      throw new errors.ValidationError(`Usuario no encontrado.`);
    }
    return USUARIO;
  }

  _app.dao.usuario.crearRecuperar = crearRecuperar;
  _app.dao.usuario.asignarRoles = asignarRoles;
  _app.dao.usuario.crearUsuario = crearUsuario;
  _app.dao.usuario.actualizarContrasena = actualizarContrasena;
  _app.dao.usuario.getUsuarioContrasena = getUsuarioContrasena;
  _app.dao.usuario.crearRecuperarUsuarioEmpresa = crearRecuperarUsuarioEmpresa;
  _app.dao.usuario.existe = existe;
  _app.dao.usuario.getPorId = getPorId;
  _app.dao.usuario.getUsuarioPersona = getUsuarioPersona;
  _app.dao.usuario.enviarCorreoNuevaContrasenia = enviarCorreoNuevaContrasenia;
};
