module.exports = (app) => {
  const _app = app;
  _app.dao.rol = {};
  const models = app.src.db.models;

  async function esParteDe (idRol, nombresRoles) {
    const datosRol = await models.rol.findOne({
      where: {
        id_rol: idRol,
        nombre: {
          $in: nombresRoles
        }
      }
    });
    if (datosRol) {
      return true;
    }
    return false;
  }

  async function esSupervisorOFiscal (idRol) {
    return esParteDe(idRol, ['SUPERVISOR', 'FISCAL', 'FISCAL_FDI', 'SUPERVISOR_FDI']);
  };

  async function esLegal (idRol) {
    return esParteDe(idRol, ['LEGAL_UPRE', 'LEGAL_FDI']);
  };

  async function esResponsableGam (idRol) {
    return esParteDe(idRol, ['RESP_GAM_GAIOC']);
  };

  async function esEmpresa (idRol) {
    return esParteDe(idRol, ['EMPRESA']);
  };

  async function esTecnico (idRol) {
    return esParteDe(idRol, ['TECNICO_UPRE', 'TECNICO_FDI']);
  };

  // async function esEncargadoDepartamental (idRol) {
  //   return esParteDe(idRol, ['RESP_DEPARTAMENTAL_UPRE']);
  // };

  async function esJefeDeRevision (idRol) {
    return esParteDe(idRol, ['JEFE_REVISION_FDI']);
  };

  async function esFinanciero (idRol) {
    return esParteDe(idRol, ['FINANCIERO_UPRE', 'FINANCIERO_FDI']);
  };

  async function esMae (idRol) {
    return esParteDe(idRol, ['MAE_FDI']);
  };

  async function esVisorReporte (idRol) {
    return esParteDe(idRol, ['VISOR_REPORTES']);
  };

  async function getId (id_rol) {
    return models.rol.findOne({
      where: { id_rol }
    });
  }

  async function getPorCodigo (codigo) {
    return models.rol.findOne({
      where: { nombre: codigo }
    });
  }

  async function formaParteDeSupervision (idRol) {
    return esParteDe(idRol, getRolesSupervision());
  }

  function getRolesSupervision () {
    // return ['TECNICO_UPRE', 'EMPRESA', 'SUPERVISOR', 'FISCAL', 'ENCAR_REGIONAL_UPRE', 'JEFE_TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE'];
    return ['TECNICO_FDI', 'SUPERVISOR_FDI', 'FISCAL_FDI', 'JEFE_REVISION_FDI'];
  }

  function getRolesGenerales () {
    // return ['LEGAL_UPRE', 'FINANCIERO_UPRE', 'JEFE_TECNICO_UPRE','RESP_RECEPCION_FDI'];
    return ['LEGAL_FDI', 'FINANCIERO_FDI', 'RESP_RECEPCION_FDI'];
  }

  _app.dao.rol.esSupervisorOFiscal = esSupervisorOFiscal;
  _app.dao.rol.esTecnico = esTecnico;
  _app.dao.rol.esLegal = esLegal;
  _app.dao.rol.esResponsableGam = esResponsableGam;
  _app.dao.rol.esEmpresa = esEmpresa;
  // _app.dao.rol.esEncargadoDepartamental = esEncargadoDepartamental;
  _app.dao.rol.esFinanciero = esFinanciero;
  _app.dao.rol.esFinanciero = esFinanciero;
  _app.dao.rol.esMae = esMae;
  _app.dao.rol.esVisorReporte = esVisorReporte;
  _app.dao.rol.getId = getId;
  _app.dao.rol.getPorCodigo = getPorCodigo;
  _app.dao.rol.formaParteDeSupervision = formaParteDeSupervision;
  _app.dao.rol.getRolesGenerales = getRolesGenerales;
};
