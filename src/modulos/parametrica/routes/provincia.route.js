import sequelizeFormly from 'sequelize-formly';
module.exports = (app) => {
  /**
    @apiVersion 1.0.0
    @apiGroup Provincia
    @apiName Get provincia
    @api {get} /provincias/:cod_departamento Obtiene datos de provincias de un departamento

    @apiDescription Get provincias, obtiene datos de provincias de un departamento

    @apiSuccessExample {json} Respuesta:
    HTTP/1.1 200 OK
    [
      {
          "codigo": "0101",
          "nombre": "Oropeza"
      },
      {
          "codigo": "0102",
          "nombre": "Azurduy"
      },
      {
          "codigo": "0103",
          "nombre": "Zudañez"
      },
      {
          "codigo": "0104",
          "nombre": "Tomina"
      },
      {
          "codigo": "0105",
          "nombre": "Hernando Siles"
      },
      ...
    ]
  */
  app.api.get('/provincias/:cod_departamento', app.controller.provincia.get);
  app.api.options('/provincias', sequelizeFormly.formly(app.src.db.models.provincia, app.src.db.models));
};
