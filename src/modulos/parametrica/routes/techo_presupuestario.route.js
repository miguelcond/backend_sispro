import sequelizeFormly from 'sequelize-formly';
const validate = require('express-validation');
// const paramValidation = require('./techo_presupuestario.validation');
module.exports = (app) => {

  /**
   * @apiVersion 1.0.0
   * @apiGroup Techos Presupuestarios
   * @apiName Listar techos presupuestarios
   * @api {get} /api/v1/techo_presupuestario Obtiene una lista de techos presupuestarios
   *
   * @apiDescription Listar techos presupuestarios, obtiene una lista de techos presupuestarios
   * 
   * @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre los techos presupuestarios

   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Datos obtenidos correctamente",
      "datos": {
        "count": 339,
        "rows": [
          {
            "codigo": "010101",
            "estado": "ACTIVO",
            "monto_disponible": 7000000,
            "monto_usado": 0,
            "nombre_municipio": "Sucre",
            "techo_presupuestal": 7000000
          },
          {
            "codigo": "010102",
            "estado": "ACTIVO",
            "monto_disponible": 4000000,
            "monto_usado": 0,
            "nombre_municipio": "Yotala",
            "techo_presupuestal": 4000000
          },
          {
            "codigo": "010103",
            "estado": "ACTIVO",
            "monto_disponible": 5500000,
            "monto_usado": 0,
            "nombre_municipio": "Poroma",
            "techo_presupuestal": 5500000
          }
          ...
        ]
      }
    }
   */
  app.api.get('/techo_presupuestario', app.controller.techo_presupuestario.listar);
  
  /**
   * @apiVersion 1.0.0
   * @apiGroup Techos Presupuestarios
   * @apiName Get techos presupuestarios
   * @api {get} /api/v1/techo_presupuestario/:codigo Obtiene el techo presupuestario de un municipio de acuerdo a su codigo
   * 
   * @apiDescription Get techo presupuestario, obtiene un registro de techo presupuestario
   *
   * @apiParam (Params) {Number} [codigo] Identificador del registro de techo presupuestario
   *
   * @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre los techos presupuestarios
   * 
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Contenido de techo presupuestario recuperado correctamente",
      "datos": {
        "codigo": "010101",
        "monto_disponible": 7000000,
        "monto_usado": 0,
        "nombre_municipio": "Sucre",
        "num_concejales": 11,
        "techo_presupuestal": 7000000
      }
    }
   */
  app.api.get('/techo_presupuestario/:codigo', app.controller.techo_presupuestario.get);
  
  /**
   * @apiVersion 1.0.0
   * @apiGroup Techos Presupuestarios
   * @apiName Reestablecer
   * @api {put} /api/v1/techo_presupuestario/reestablecer Establece el monto usado a 0 y el monto disponible igual al techo presupuestario
   *
   * @apiDescription Put reestablecer, Reestablece techos presupuestarios
   *
   * @apiParamExample {json} Ejemplo de petición:
    {
      "reestablecer": true
    }
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Se han reestablecido los valores de techo presupuestario por defecto correctamente."
      "datos": []
    }
   */
  app.api.put('/techo_presupuestario/reestablecer', app.controller.techo_presupuestario.reestablecer);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Techos Presupuestarios
   * @apiName Put techos presupuestarios
   * @api {put} /api/v1/techo_presupuestario/:codigo Actualiza los datos de techo presupuestario de un municipio
   *
   * @apiDescription Put techo presupuestario, Actualiza un registro de techo presupuestario
   * 
   * @apiParam (codigo) {String} Identificador del registro de techo presupuestario
   *
   * @apiParamExample {json} Ejemplo de petición:
    {
      "codigo": "010101",
      "monto_disponible": 7000000,
      "monto_usado": 0,
      "nombre_municipio": "Sucre",
      "num_concejales": 11,
      "techo_presupuestal": 7000000
    }
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Techo presupuestario modificado correctamente.",
      "datos": [1]
    }
   */
  app.api.put('/techo_presupuestario/:codigo', app.controller.techo_presupuestario.put);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Techos Presupuestarios
   * @apiName Recalcular
   * @api {put} /api/v1/techo_presupuestario/:codigo/recalcular Calcula el monto usado y el monto disponible por un municipio
   *
   * @apiDescription Put recalcular, Calcula el monto usado y monto disponible por municipio
   * 
   * @apiParam (codigo) {String} Identificador del registro de techo presupuestario
   *
   * @apiParamExample {json} Ejemplo de petición:
    {
      "fechaValidos": "2018-07-12T14:17:02.340Z",
      "idProyecto": 1,
      "montoActualizar": 573711
    }
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Techo presupuestario modificado correctamente.",
      "datos": [1]
    }
   */
  app.api.put('/techo_presupuestario/:codigo/recalcular', app.controller.techo_presupuestario.recalcular, app.controller.techo_presupuestario.put);
};
