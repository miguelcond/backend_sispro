const validate = require('express-validation');
const paramValidation = require('./parametrica.validation');
import util from '../../../lib/util';
module.exports = (app) => {
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;
  app.api.get('/cartera', async function(req,res,next){
    
    try{
      let result = await models.cartera.findAll({
        attributes:['id_cartera','num_cartera','descripcion']
      })
      res.status(200).json({
        finalizado: true,
        mensaje: 'lista de carteras obtenidos!',
        datos:result
      })
    }catch(error){
      next(error)
    }

  });
  app.api.post('/cartera', function (req,res, next){
    console.log(req.body,'recibidooooooo')
    return sequelize.transaction(async(t)=> {
      //const USUARIO = util.json(await app.dao.usuario.getPorId(req.body.audit_usuario.id_usuario))
      //console.log(USUARIO.id_usuario,"<=====ID_USUARIO")
      //const ESTADO_INICIAL = util.json(await app.src.db.models.estado.estadoInicial('PROYECTO-FDI', t)).codigo;
      //console.log(ESTADO_INICIAL,"jojojojojojojoj") 
      //console.log(models.proyecto,"======================")
      let result = await models.cartera.create(req.body,{transaction: t})
      console.log(result,"hhhhhehehheheheh")
    }).then((respuesta)=>{
      res.status(200).json({
      finalizado: true,
      mensaje:'Cartera registrado satisfactoriamente!',
      datos:respuesta
      });
    }).catch((error)=>{
      next(error)
    })
    
  });

  app.api.put('/cartera', validate(paramValidation.put), function(req,res){

  });
};