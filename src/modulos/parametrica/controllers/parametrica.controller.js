module.exports = (app) => {
  const _app = app;
  _app.controller.parametrica = {};
  const parametricaController = _app.controller.parametrica;
  const ParametricaModel = app.src.db.models.parametrica;
  const util = app.src.lib.util;
  const sequelize = app.src.db.sequelize;

  parametricaController.get = (req, res) => {
    const consulta = util.formarConsulta(req.query, ParametricaModel, app.src.db.models);
    if (!consulta.condicionParametrica) {
      consulta.condicionParametrica = {
        estado: 'ACTIVO'
      };
    }
    consulta.condicionParametrica.tipo = req.params.tipo;
    if (req.query.order) {
      if (req.query.order.charAt(0) === '-') {
        consulta.order = `${req.query.order.substring(1, req.query.order.length)} DESC`;
      } else {
        consulta.order = `${req.query.order}`;
      }
    } else {
      consulta.order = 'descripcion DESC';
    }
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }

    ParametricaModel.buscarPorTipo(ParametricaModel, consulta)
    .then((pResp) => {
      let datos = {};
      if (pResp) datos = pResp;
      res.json({
        finalizado: true,
        mensaje: 'Parametros obtenidos correctamente.',
        datos
      });
    })
    .catch((pError) => {
      res.status(412).json({
        finalizado: false,
        mensaje: pError.message,
        datos: {}
      });
    });
  };

  parametricaController.post = (req, res, next) => {
    return sequelize.transaction(async(t) => {
      let tipo = req.params.tipo;
      let resp = await _app.dao.parametrica.crear(req.body, tipo, t);
      return {
        codigo: resp.codigo,
        nombre: resp.nombre,
        descripcion: resp.descripcion,
      };
    }).then((result)=>{
      res.json({
        finalizado: true,
        mensaje: 'Parametrica adicionada correctamente.',
        datos: result
      });
    }).catch((error)=>{
      next(error);
    });
  };

  parametricaController.put = (req, res, next) => {
    return sequelize.transaction(async(t) => {
      let tipo = req.params.tipo;
      let codigo = req.body.codigo;
      delete req.body.codigo;
      let resp = _app.dao.parametrica.actualizar(req.body, tipo, codigo, t);
      return {
        codigo,
        nombre: resp.nombre,
        descripcion: resp.descripcion,
      };
    }).then((result)=>{
      res.json({
        finalizado: true,
        mensaje: 'Parametrica modificada correctamente.',
        datos: result
      });
    }).catch((error)=>{
      next(error);
    });
  };
};
