import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;

  const models = app.src.db.models;

  const obtenerPorCodigo = async (codigo) => {
    const PARAMETRICA = models.parametrica.findOne({
      where: {
        codigo: codigo
      }
    });
    if (!PARAMETRICA) {
      throw new errors.ValidationError(`No existe la paramétrica con el código solicitado.`);
    }
    return PARAMETRICA;
  };

  const obtenerPorCodigoTipo = async (codigo,tipo) => {
    const PARAMETRICA = models.parametrica.findOne({
      where: {
        codigo: codigo,
        tipo: tipo
      }
    });
    if (!PARAMETRICA) {
      throw new errors.ValidationError(`No existe la paramétrica con el código solicitado.`);
    }
    return PARAMETRICA;
  };

  const obtenerNombre = async (codigo) => {
    const PARAMETRICA = await obtenerPorCodigo(codigo);
    return PARAMETRICA.nombre;
  };

  const obtenerNombreTipo = async (codigo, tipo) => {
    const PARAMETRICA = await obtenerPorCodigoTipo(codigo,tipo);
    return {nombre: PARAMETRICA.nombre, descripcion: PARAMETRICA.descripcion};
  };

  const crear = async (datos, tipo, t) => {
    datos.tipo = tipo || '';
    datos._usuario_creacion = 1;
    const ultimoRegistro = await obtenerUltimoRegistro(datos.tipo);

    if (ultimoRegistro && ultimoRegistro.orden) {
      datos.orden = parseInt(ultimoRegistro.orden) + 1;
    } else {
      datos.orden = 1;
    }

    let result = util.json(await models.parametrica.create(datos, {
      transaction: t
    }));
    return result;
  };

  const actualizar = async (datos, tipo, codigo, t) => {
    let result = util.json(await models.parametrica.update(datos, {
      where: {
        tipo,
        codigo,
      },
      transaction: t
    }));
    return result;
  };

  const obtenerUltimoRegistro = async (tipoParametrica) => {
    const ultimoRegistro = models.parametrica.findOne({
      where: {
        tipo: tipoParametrica
      },
      order: [
        ['orden','DESC']
      ]
    });

    return ultimoRegistro;
  };

  _app.dao.parametrica = {
    crear,
    actualizar,
    obtenerPorCodigo,
    obtenerPorCodigoTipo,
    obtenerNombre,
    obtenerNombreTipo
  };
};
