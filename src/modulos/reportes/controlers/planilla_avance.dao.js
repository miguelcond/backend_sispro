import util from '../../../lib/util';
import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.planillaAvance = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function crearPorProyecto (rutaPlanillas, datosProyecto, id_proyecto) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`PAC_${datosProyecto.id_proyecto}`);
    const datosReporte = await obtenerDatosPorProyecto(datosProyecto);
    const rep = new Reporte('planilla_avance_consolidada');
    return await rep.pdfDownload(datosReporte, { orientation: 'landscape', output: `${rutaPlanillas}/${nombreArchivo}`, pageSize: 'A3' });
  }

  async function crear (rutaPlanillas, datosProyecto, idAdjudicacion, nroPlanilla) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`PA_${idAdjudicacion}-${nroPlanilla}`);
    const datosReporte = await obtenerDatos(datosProyecto, idAdjudicacion, nroPlanilla);
    const rep = new Reporte('planilla_avance');
    return rep.pdfDownload(datosReporte, { orientation: 'landscape', output: `${rutaPlanillas}/${nombreArchivo}`, pageSize: 'A3' });
  }

  async function obtenerNombreSupervisor (equipoTecnico) {
    for (let i in equipoTecnico) {
      if (equipoTecnico[i].cargo === 'CG_SUPERVISOR') {
        return app.dao.persona.obtenerApellidoNombre(equipoTecnico[i].usuario.persona);
      }
    }
  }

  /**
   * Añadiendo nombres para la primera y última versión
   */
  const _prepararListaHistoricos = (supervision) => {
    const DATOS_INICIALES = {
      tipo_modificacion_proyecto: { nombre: 'Contrato Original' },
      tipo_modificacion: 'INICIAL'
    };
    if (supervision.proyecto.proyecto_historico.length === 0) {
      supervision.proyecto.proyecto_historico.push(DATOS_INICIALES);
    } else {
      Object.assign(supervision.proyecto.proyecto_historico[0], DATOS_INICIALES);
      supervision.proyecto.proyecto_historico.push({
        tipo_modificacion_proyecto: supervision.proyecto.tipo_modificacion_proyecto,
        tipo_modificacion: supervision.proyecto.tipo_modificacion
      });
    }
    return supervision.proyecto.proyecto_historico;
  };

  /**
   * Añadiendo nombres para la primera y última versión de adjudicación
   */
  const _prepararListaHistoricosAdjudicacion = (supervision) => {
    const DATOS_INICIALES = {
      tipo_modificacion_adjudicacion: { nombre: 'Contrato Original' },
      tipo_modificacion: 'INICIAL'
    };
    if (supervision.adjudicacion.adjudicacion_historico.length === 0) {
      supervision.adjudicacion.adjudicacion_historico.push(DATOS_INICIALES);
    } else {
      Object.assign(supervision.adjudicacion.adjudicacion_historico[0], DATOS_INICIALES);
      supervision.adjudicacion.adjudicacion_historico.push({
        tipo_modificacion_adjudicacion: supervision.adjudicacion.tipo_modificacion_adjudicacion,
        tipo_modificacion: supervision.adjudicacion.tipo_modificacion
      });
    }
    return supervision.adjudicacion.adjudicacion_historico;
  };

  /**
   * Agregando datos de versiones anteriores a items que fueron adicionados en contratos modificatorios (su historial
   * tiene datos desde la versión donde fueron creados). Es para mostrarlo en la tabla en la columna que corresponde y
   * que las casillas anteriores tengas las características según las modificaciones anteriores (si existen)
   */
  const _prepararHistoricosItems = (supervision, modificaciones) => {
    // let versionActual = supervision.proyecto.version;
    let versionActual = supervision.adjudicacion.version;
    supervision.modulos.map(modulo => {
      modulo.items.map(item => {
        let tam = item.item_historico.length;
        if (tam < versionActual - 1) {
          for (var i = (versionActual - tam - 1); i > 0; i--) {
            item.item_historico.unshift({
              version : i
            });
          }
          // for (var i = 1; i < (versionActual - tam); i ++) {
          //   item.item_historico.unshift({
          //     version : i
          //   });
          // }
        }
        item.item_historico.map(historico => {
          if (historico.version === 1) {
            historico.modificacion = 'INICIAL';
          } else {
            let buscar = _.find(modificaciones, {version: historico.version });
            if (buscar) {
              historico.modificacion =  buscar.tipo_modificacion;
            }
          }
        });

        // let tam = item.item_historico.length;
        // for (var i = 1; i < versionActual; i++) {
        //   item.item_historico.unshift({
        //     version: (i)
        //   });
        //   if ((i) === 1) {
        //     item.item_historico[0].modificacion = 'INICIAL';
        //   } else {
        //     let buscar = _.find(modificaciones, {version: i });
        //     item.item_historico[0].modificacion =  buscar && buscar.length > 0 ? buscar.tipo_modificacion : supervision.adjudicacion.tipo_modificacion;
        //   }
        // }
        //console.log(item.item_historico);
      });
    });
    return supervision.modulos;
  };

  async function obtenerDatosPorProyecto(datosProyecto, idRol) {
    let datosReporte = {
      fecha_elaboracion_planilla: moment().format('DD/MM/YYYY')
    };
    // datosReporte.datos_proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(datosProyecto.id_proyecto, idRol, false, false);
    datosReporte.datos_proyecto = datosProyecto;
    datosReporte.datos_proyecto.municipio = datosReporte.datos_proyecto.dataValues.dpa.municipio.nombre;
    datosReporte.datos_proyecto.provincia = datosReporte.datos_proyecto.dataValues.dpa.provincia.nombre;
    datosReporte.datos_proyecto.departamento = datosReporte.datos_proyecto.dataValues.dpa.departamento.nombre;
    datosReporte.datos_proyecto.fecha_inicio_obra = datosProyecto.orden_proceder ? moment(datosProyecto.orden_proceder).format('DD/MM/YYYY') : '',
    datosReporte.tipo_planilla = 'CONSOLIDADO';

    let financiamiento = datosProyecto.formularios && datosProyecto.formularios.length > 0 ? datosProyecto.formularios[0] : {};
    let monto_contraparte = financiamiento && financiamiento.form_model ? financiamiento.form_model.monto_contraparte : 0;
    let monto_beneficiarios = financiamiento && financiamiento.form_model ? financiamiento.form_model.monto_beneficiarios : 0;
    let monto_otros = financiamiento && financiamiento.form_model ? financiamiento.form_model.monto_otros : 0;

    datosReporte.datos_proyecto.monto_contrato = util.convertirNumeroAString(datosProyecto.monto_total);
    datosReporte.datos_proyecto.monto_total = util.convertirNumeroAString(datosProyecto.monto_total);
    // monto_convenio: util.convertirNumeroAString(SUPERVISION.proyecto.monto_total_convenio),
    datosReporte.datos_proyecto.monto_fdi = util.convertirNumeroAString(datosProyecto.monto_fdi);
    datosReporte.datos_proyecto.monto_contraparte = util.convertirNumeroAString(monto_contraparte);
    datosReporte.datos_proyecto.monto_beneficiarios = util.convertirNumeroAString(monto_beneficiarios);
    datosReporte.datos_proyecto.monto_otros = util.convertirNumeroAString(monto_otros);
    datosReporte.datos_proyecto.monto_anticipo = util.convertirNumeroAString(datosProyecto.anticipo);
    datosReporte.datos_proyecto.incidencia_anticipo = util.convertirNumeroAString(datosProyecto.porcentaje_anticipo);
    datosReporte.datos_proyecto.monto_adjudicacion = 0;
    datosReporte.datos_proyecto.monto_inicial = 0;
    datosReporte.datos_proyecto.monto_final = 0;

    datosReporte.datos_proyecto.ampliacion_plazo = 0;

    datosReporte.datos_proyecto.fecha_prevista_recepcion_provisional = moment(datosProyecto.fecha_recepcion_provisional).format('DD/MM/YYYY');
    datosReporte.datos_proyecto.fecha_prevista_recepcion_definitiva = moment(datosProyecto.fecha_recepcion_definitiva).format('DD/MM/YYYY');

    datosReporte.monto_desembolso_total = util.convertirNumeroAString(0);
    datosReporte.porcentaje_desembolso_fdi = util.convertirNumeroAString(0);

    datosReporte.totales = {
      monto_desembolsos_acumulados: 0,
      monto_desembolso_actual: 0,
      monto_desembolso_real: 0,
      total_desembolsado: 0,
      monto_actual_ejecutado: 0,
      porcentaje_total_ejecutado: 0,
      monto_total_acumulado_a_la_fecha: 0,
      monto_total_por_ejecutar: 0,
      porcentaje_avance_fisico: 0,
      porcentaje_avance_financiero: 0,
      monto_desembolso_fdi: 0,
      porcentaje_desembolso_fdi: 0
    }

    let primera_supervision = await models.supervision.findOne({
      where:{
        fid_proyecto: datosProyecto.id_proyecto,
        $not: {estado:'INACTIVO'}
      },
      order: sequelize.literal(`fecha_inicio ASC`)}
    );

    datosReporte.periodo_planilla = {};
    if (primera_supervision) {
      datosReporte.periodo_planilla.desde = moment(primera_supervision.fecha_inicio).format('DD/MM/YYYY');
      datosReporte.periodo_planilla.hasta = moment().format('DD/MM/YYYY');
    }

    datosReporte.adjudicaciones = [];

    let adjudicaciones = await models.adjudicacion.findAll({
      where: {
        fid_proyecto: datosProyecto.id_proyecto,
        $not:[{estado:'INACTIVO'}]
      },
      include:[
        {
          attributes:['id_supervision','nro_supervision'],
          model: models.supervision,
          as: 'supervision',
          order: [['nro_supervision','DESC']],
          $not:[{estado:'INACTIVO'}]
        }
      ]
    });

    datosReporte.totales.ponderados = [];

    for (let a = 0; a < adjudicaciones.length; a++) {
      adjudicaciones[a] = util.json(adjudicaciones[a]);
      adjudicaciones[a].ampliacion_plazo = adjudicaciones[a].plazo_ampliacion ? (adjudicaciones[a].plazo_ampliacion.por_volumenes + adjudicaciones[a].plazo_ampliacion.por_compensacion) : 0;
      //datosReporte.datos_proyecto.ampliacion_plazo += adjudicaciones[a].plazo_ampliacion ? adjudicaciones[a].plazo_ampliacion.por_volumenes + adjudicaciones[a].plazo_ampliacion.por_compensacion : 0;
      datosReporte.datos_proyecto.monto_adjudicacion += adjudicaciones[a].monto;
      datosReporte.datos_proyecto.monto_inicial += adjudicaciones[a].version == 1 ? adjudicaciones[a].monto : (await app.dao.adjudicacion_historico.obtenerPorId(adjudicaciones[a].id_adjudicacion,1)).monto;
      datosReporte.datos_proyecto.monto_final += adjudicaciones[a].monto;

      let fecha_recepcion_adjudicacion = await app.dao.adjudicacion.calcularFechasRecepcion(adjudicaciones[a], datosProyecto);
      if (fecha_recepcion_adjudicacion) {
        adjudicaciones[a].fecha_recepcion_provisional = moment(fecha_recepcion_adjudicacion.fecha_recepcion_provisional).format('DD/MM/YYYY');
        adjudicaciones[a].fecha_recepcion_definitiva = moment(fecha_recepcion_adjudicacion.fecha_recepcion_definitiva).format('DD/MM/YYYY');
      } else {
        adjudicaciones[a].fecha_recepcion_provisional = moment(datosProyecto.fecha_recepcion_provisional ).format('DD/MM/YYYY');
        adjudicaciones[a].fecha_recepcion_definitiva = moment(datosProyecto.fecha_recepcion_definitiva ).format('DD/MM/YYYY');
      }

      if (adjudicaciones[a].supervision.length > 0) {
        adjudicaciones[a].modulos = [];
        // let ultima_supervision = await models.supervision.findOne({
        //   where: {
        //     fid_adjudicacion: adjudicaciones[a].id_adjudicacion,
        //     $not:[{estado:'INACTIVO'}]
        //   },
        //   order: [['nro_supervision','DESC']],
        // });
        let sup_numeros = [];
        for (let k = 0; k < adjudicaciones[a].supervision.length; k++) {
          sup_numeros.push(adjudicaciones[a].supervision[k].nro_supervision);
        }
        let ultima_supervision = Math.max.apply(Math, sup_numeros);

        if (ultima_supervision) {
          // let DatosSupervision = await obtenerDatos(datosProyecto, adjudicaciones[a].id_adjudicacion, ultima_supervision.nro_supervision);
          let DatosSupervision = await obtenerDatos(datosProyecto, adjudicaciones[a].id_adjudicacion, ultima_supervision);

          for (let m = 0; m < DatosSupervision.modulos.length; m++) {
            adjudicaciones[a].modulos.push(DatosSupervision.modulos[m]);
            adjudicaciones[a].proyecto_historico = DatosSupervision.datos_proyecto.proyecto_historico;
            adjudicaciones[a].adjudicacion_historico = DatosSupervision.adjudicacion.adjudicacion_historico;
            adjudicaciones[a].modificacion = DatosSupervision.adjudicacion.modificacion;
          }

          datosReporte.totales.monto_desembolsos_acumulados += DatosSupervision.datosSupervision.monto_desembolsos_acumulados;
          datosReporte.totales.monto_desembolso_actual += DatosSupervision.datosSupervision.monto_desembolso_actual;
          datosReporte.totales.total_desembolsado += DatosSupervision.datosSupervision.monto_desembolso_total;
          datosReporte.totales.monto_actual_ejecutado += DatosSupervision.datosSupervision.monto_total_actual_ejecutado;
          datosReporte.totales.monto_total_por_ejecutar += DatosSupervision.datosSupervision.monto_total_por_ejecutar;
          datosReporte.totales.monto_total_acumulado_a_la_fecha += DatosSupervision.datosSupervision.monto_total_acumulado;
          datosReporte.totales.ponderados.push(DatosSupervision.datosSupervision.avance_fisico_ponderado);
          datosReporte.adjudicaciones.push(adjudicaciones[a]);
        }
      }
    }

    let cronograma = await models.formulario.findOne({
      where: {
        fid_proyecto: datosProyecto.id_proyecto,
        nombre: 'cronograma_desembolsos',
        estado: 'ELABORADO'
      }
    });
    cronograma = util.json(cronograma);

    datosReporte.totales.monto_desembolso_fdi = cronograma.form_model.monto_1;
    datosReporte.totales.porcentaje_desembolso_fdi = cronograma.form_model.porcentaje_1;
    let today = moment();
    if (cronograma.form_model.fecha_2) {
      let fecha_2;
      if (cronograma.form_model.fecha_2.split('-')[0].length == 2)
        fecha_2 = moment(cronograma.form_model.fecha_2,'DD-MM-YYYY');
      else
        fecha_2 = moment(cronograma.form_model.fecha_2,'YYYY-MM-DD');
      if (today.diff(fecha_2,'days') >= 0) {
        datosReporte.totales.monto_desembolso_fdi += cronograma.form_model.monto_2;
        datosReporte.totales.porcentaje_desembolso_fdi += cronograma.form_model.porcentaje_2;
        if (cronograma.form_model.fecha_3) {
          let fecha_3;
          if (cronograma.form_model.fecha_3.split('-')[0].length == 2)
            fecha_3 = moment(cronograma.form_model.fecha_3,'DD-MM-YYYY');
          else {
            fecha_3 = moment(cronograma.form_model.fecha_3,'YYYY-MM-DD');
          }
          if (today.diff(fecha_3,'days') >= 0) {
            datosReporte.totales.monto_desembolso_fdi += cronograma.form_model.monto_3;
            datosReporte.totales.porcentaje_desembolso_fdi += cronograma.form_model.porcentaje_3;
          }
        }
      }
    }

    datosReporte.totales.porcentaje_total_ejecutado = util.calcularPorcentaje(datosReporte.totales.monto_actual_ejecutado, datosReporte.totales.monto_total_acumulado_a_la_fecha);
    datosReporte.totales.porcentaje_total_ejecutado = `${util.convertirNumeroAString(datosReporte.totales.porcentaje_total_ejecutado)} %`;

    //avance fisicio
    //console.log(`${datosReporte.totales.monto_total_acumulado_a_la_fecha}, ${datosReporte.monto_final}`);
    // datosReporte.totales.porcentaje_avance_fisico = util.calcularPorcentaje(datosReporte.totales.monto_total_acumulado_a_la_fecha, datosReporte.datos_proyecto.monto_final);
    //avance ponderado (promedio)
    datosReporte.totales.porcentaje_avance_fisico = datosReporte.totales.ponderados.length > 0 ? datosReporte.totales.ponderados.reduce((a,b) => a + b, 0) / datosReporte.totales.ponderados.length : 0;
    datosReporte.totales.porcentaje_avance_fisico = `${util.convertirNumeroAString(datosReporte.totales.porcentaje_avance_fisico)} %`;

    //avance financiero
    //datosReporte.totales.porcentaje_avance_financiero = util.calcularPorcentaje(datosReporte.monto_desembolso_total, datosReporte.datos_proyecto.monto_adjudicacion);
    datosReporte.totales.porcentaje_avance_financiero = util.calcularPorcentaje(datosReporte.totales.monto_actual_ejecutado, datosReporte.datos_proyecto.monto_final);
    datosReporte.totales.porcentaje_avance_financiero = `${util.convertirNumeroAString(datosReporte.totales.porcentaje_avance_financiero)} %`;

    datosReporte.datos_proyecto.monto_adjudicacion = util.convertirNumeroAString(datosReporte.datos_proyecto.monto_adjudicacion);
    datosReporte.totales.monto_desembolso_fdi = util.convertirNumeroAString(datosReporte.totales.monto_desembolso_fdi);

    datosReporte.totales.porcentaje_desembolso_fdi = `${util.convertirNumeroAString(datosReporte.totales.porcentaje_desembolso_fdi)} %`;

    datosReporte.totales.monto_actual_ejecutado = util.convertirNumeroAString(datosReporte.totales.monto_actual_ejecutado);
    datosReporte.totales.monto_total_acumulado_a_la_fecha = `${util.convertirNumeroAString(datosReporte.totales.monto_total_acumulado_a_la_fecha)} Bs.`;

    datosReporte.totales.monto_total_por_ejecutar = `${util.convertirNumeroAString(datosReporte.totales.monto_total_por_ejecutar)} Bs.`;

    return datosReporte;
  }

  async function obtenerDatos(datosProyecto, idAdjudicacion, nroPlanilla) {
    let SUPERVISION = await app.dao.supervision.obtener(datosProyecto.id_proyecto, idAdjudicacion, nroPlanilla);
    if (!SUPERVISION) {
      throw new errors.NotFoundError(`No se encontró la planilla solicitada.`);
    }
    SUPERVISION = util.json(SUPERVISION);

    // Obtener montos de formulario de financiamiento
    let financiamiento = datosProyecto.formularios && datosProyecto.formularios.length > 0 ? datosProyecto.formularios[0] : {};
    let monto_contraparte = financiamiento && financiamiento.form_model ? financiamiento.form_model.monto_contraparte : 0;
    let monto_beneficiarios = financiamiento && financiamiento.form_model ? financiamiento.form_model.monto_beneficiarios : 0;
    let monto_otros = financiamiento && financiamiento.form_model ? financiamiento.form_model.monto_otros : 0;

    let ADJUDICACION = await app.dao.adjudicacion.obtenerPorId(idAdjudicacion);

    if (SUPERVISION.version_adjudicacion && SUPERVISION.version_adjudicacion < ADJUDICACION.version) {
      ADJUDICACION = await app.dao.adjudicacion_historico.obtenerPorId(SUPERVISION.fid_adjudicacion, SUPERVISION.version_adjudicacion);
    }
    ADJUDICACION.modificaciones = await app.dao.adjudicacion_historico.obtenerModificaciones(SUPERVISION.fid_adjudicacion, ADJUDICACION.version);
    ADJUDICACION.monto_inicial = util.convertirNumeroAString(ADJUDICACION.version == 1 ? ADJUDICACION.monto : (await app.dao.adjudicacion_historico.obtenerPorId(SUPERVISION.fid_adjudicacion,1)).monto);
    ADJUDICACION.monto = util.convertirNumeroAString(ADJUDICACION.monto);
    ADJUDICACION.adjudicacion_historico = _prepararListaHistoricosAdjudicacion(SUPERVISION);
    ADJUDICACION.modificacion = SUPERVISION.adjudicacion.tipo_modificacion || 'INICIAL';
    let modificaciones = await app.dao.adjudicacion_historico.obtenerModificaciones(SUPERVISION.fid_adjudicacion,ADJUDICACION.version);

    if (!ADJUDICACION.orden_proceder) ADJUDICACION.orden_proceder = SUPERVISION.proyecto.orden_proceder;
    let fechasRecepcionAdjudicacion = await app.dao.adjudicacion.calcularFechasRecepcion(ADJUDICACION, datosProyecto);
    if (fechasRecepcionAdjudicacion) {
      ADJUDICACION.fecha_recepcion_provisional = fechasRecepcionAdjudicacion.fecha_recepcion_provisional;
      ADJUDICACION.fecha_recepcion_definitiva = fechasRecepcionAdjudicacion.fecha_recepcion_definitiva;
    } else {
      ADJUDICACION.fecha_recepcion_provisional = SUPERVISION.proyecto.fecha_recepcion_provisional;
      ADJUDICACION.fecha_recepcion_definitiva = SUPERVISION.proyecto.fecha_recepcion_definitiva;
    }

    let datosReporte = {
      nro: SUPERVISION.nro_supervision,
      fecha_elaboracion_planilla: moment().format('DD/MM/YYYY'),
      periodo_planilla: {
        desde: moment(SUPERVISION.fecha_inicio).format('DD/MM/YYYY'),
        hasta: moment(SUPERVISION.fecha_fin).format('DD/MM/YYYY')
      },
      adjudicacion: ADJUDICACION,
      estado_supervision: SUPERVISION.estado_supervision,
      datos_proyecto: {
        nombre: SUPERVISION.proyecto.nombre,
        entidad_beneficiaria: SUPERVISION.proyecto.entidad_beneficiaria,
        // empresa: SUPERVISION.proyecto.empresa.nombre,
        supervisor: await obtenerNombreSupervisor(SUPERVISION.proyecto.equipo_tecnico),
        monto_contrato: util.convertirNumeroAString(SUPERVISION.proyecto.monto_contractual),
        monto_total: util.convertirNumeroAString(SUPERVISION.proyecto.monto_total),
        // monto_convenio: util.convertirNumeroAString(SUPERVISION.proyecto.monto_total_convenio),
        monto_fdi: util.convertirNumeroAString(SUPERVISION.proyecto.monto_fdi),
        monto_contraparte: util.convertirNumeroAString(monto_contraparte),
        monto_beneficiarios: util.convertirNumeroAString(monto_beneficiarios),
        monto_otros: util.convertirNumeroAString(monto_otros),
        monto_anticipo: util.convertirNumeroAString(SUPERVISION.proyecto.anticipo),
        incidencia_anticipo: util.convertirNumeroAString(SUPERVISION.proyecto.porcentaje_anticipo),
        // fecha_inicio_obra: SUPERVISION.proyecto.orden_proceder ? moment(SUPERVISION.proyecto.orden_proceder).format('DD/MM/YYYY') : 'N/E',
        fecha_inicio_obra: ADJUDICACION.orden_proceder ? moment(ADJUDICACION.orden_proceder).format('DD/MM/YYYY') : moment(SUPERVISION.proyecto.orden_proceder).format('DD/MM/YYYY'),
        plazo_ejecucion: SUPERVISION.proyecto.plazo_ejecucion,
        departamento: SUPERVISION.proyecto.dpa.departamento.nombre,
        provincia: SUPERVISION.proyecto.dpa.provincia.nombre,
        municipio: SUPERVISION.proyecto.dpa.municipio.nombre,
        ampliacion_plazo: ADJUDICACION.plazo_ampliacion ? ADJUDICACION.plazo_ampliacion.por_volumenes + ADJUDICACION.plazo_ampliacion.por_compensacion : 0,
        fecha_prevista_recepcion_provisional: moment(ADJUDICACION.fecha_recepcion_provisional).format('DD/MM/YYYY'),
        fecha_prevista_recepcion_definitiva: moment(ADJUDICACION.fecha_recepcion_definitiva).format('DD/MM/YYYY'),
        proyecto_historico: _prepararListaHistoricos(SUPERVISION),
        fecha_aprobacion: '',
        modificacion: SUPERVISION.proyecto.tipo_modificacion || 'INICIAL'
      },
      modulos: _prepararHistoricosItems(SUPERVISION,modificaciones),
      totales: {
        monto_desembolsos_acumulados: util.convertirNumeroAString(SUPERVISION.monto_desembolsos_acumulados || 0),
        monto_desembolso_actual: util.convertirNumeroAString(SUPERVISION.monto_desembolso_actual),
        total_desembolsado: util.convertirNumeroAString(SUPERVISION.monto_desembolso_total),
        monto_actual_ejecutado: `${util.convertirNumeroAString(SUPERVISION.monto_total_actual_ejecutado)} Bs.`,
        // porcentaje_total_ejecutado: `${util.convertirNumeroAString(SUPERVISION.porcentaje_avance_fis)} %`,
        porcentaje_total_ejecutado: `${util.convertirNumeroAString(SUPERVISION.avance_fisico_ponderado)} %`,
        monto_total_acumulado_a_la_fecha: `${util.convertirNumeroAString(SUPERVISION.monto_total_acumulado)} Bs.`,
        monto_total_por_ejecutar: `${util.convertirNumeroAString(SUPERVISION.monto_total_por_ejecutar)} Bs.`,
        porcentaje_avance_fisico: `${util.convertirNumeroAString(SUPERVISION.porcentaje_avance_fis)} %`,
        // porcentaje_avance_fisico: `${util.convertirNumeroAString(SUPERVISION.avance_fisico_ponderado)} %`,
        porcentaje_avance_financiero: `${util.convertirNumeroAString(SUPERVISION.porcentaje_avance_fin)} %`,
        avance_fisico_ponderado: `${util.convertirNumeroAString(SUPERVISION.avance_fisico_ponderado)} %`,
        maximo_esperado: SUPERVISION.maximo_esperado
      },
      datosSupervision: SUPERVISION,
      //boletas: await getDatosBoletas(SUPERVISION.proyecto.boletas),
      informe: SUPERVISION.informe,
      monto_desembolso_total: util.convertirNumeroAString(util.sumarPor(SUPERVISION.supervisiones_anteriores, (supervision) => {
        return supervision.desembolso;
      })),
      supervisiones_anteriores: await getDatosSupervisionesAnteriores(SUPERVISION.supervisiones_anteriores)
    };

    //verificar la fecha de la aprobacion del fiscal
    if (SUPERVISION.estado_supervision == 'EN_ARCHIVO_FDI') {
      let transicion = await app.dao.transicion.getUltimaTransicion(SUPERVISION.id_supervision,'supervision','EN_ARCHIVO_FDI');
      if (transicion != null) {
        transicion = util.json(transicion);
        if (transicion.hasOwnProperty("_fecha_creacion")) datosReporte.datos_proyecto.fecha_aprobacion = moment(transicion._fecha_creacion).format('DD/MM/YYYY');
      }
    }

    // Validar fecha hasta (no deberia ser menor o invalida)
    //Se  cambio la conversion del tipo de datos de fecha fin e inicio para la comparacion
    if (Date.parse(SUPERVISION.fecha_fin) < Date.parse(SUPERVISION.fecha_inicio) || datosReporte.periodo_planilla.hasta === 'Invalid date') {
      datosReporte.periodo_planilla.hasta = '-';
    }

    return datosReporte;
  }

  async function getDatosBoletas (boletas) {
    let filtroBoletas = [];
    for (let i = 0; i < boletas.length; i++) {
      boletas[i].tipo_boleta = await app.dao.parametrica.obtenerNombre(boletas[i].tipo_boleta);
      boletas[i].tipo_documento = boletas[i].tipo_documento === 'POLIZA' ? 'Póliza' : 'Boleta';
      boletas[i].fecha_fin_validez = moment(boletas[i].fecha_fin_validez).format('DD/MM/YYYY');
      if (boletas[i].estado === 'ACTIVO') {
        if (boletas[i].tipo_boleta.indexOf('anticipo') >= 0) {
          filtroBoletas.push(boletas[i]);
        } else {
          filtroBoletas.unshift(boletas[i]);
        }
      }
    }
    return filtroBoletas;
  }

  async function getDatosSupervisionesAnteriores (supervisionesAnteriores) {
    supervisionesAnteriores.map(supervision => {
      supervision.desembolso = util.convertirNumeroAString(supervision.desembolso);
      supervision.numero = util.getNumeroOrdinal(supervision.nro_supervision);
    });
    supervisionesAnteriores = _.orderBy(supervisionesAnteriores, 'nro_supervision', 'asc');
    return supervisionesAnteriores;
  }

  app.dao.planillaAvance.crear = crear;
  app.dao.planillaAvance.crearPorProyecto = crearPorProyecto;
};
