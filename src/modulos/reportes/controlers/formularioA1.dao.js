import util from '../../../lib/util';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.formularioA1 = {};

  async function crear (rutaFormularios, datosProyecto) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A1`);
    const datosFormulario = await obtenerDatos(datosProyecto);
    const rep = new Reporte('formulario_A1');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto) {
    const fechaConvenio = moment(datosProyecto.fecha_suscripcion_convenio);
    const montoBs = datosProyecto.monto_total;
    const datosFormularioA1 = {
      nro_invitacion: datosProyecto.nro_invitacion,
      nro_convenio: datosProyecto.nro_convenio,
      fecha_convenio: {
        dia: fechaConvenio.format('DD'),
        mes: fechaConvenio.format('MM'),
        anio: fechaConvenio.format('YYYY')
      },
      descripcion_proyecto: datosProyecto.nombre,
      monto_bs: {
        numeral: util.convertirNumeroAString(montoBs),
        literal: util.convertirNumeroALiteral(montoBs)
      },
      validez: datosProyecto.plazo_ejecucion
    };
    return datosFormularioA1;
  }

  app.dao.formularioA1.crear = crear;
};
