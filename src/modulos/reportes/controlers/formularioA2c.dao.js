import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.formularioA2c = {};

  async function crear (rutaFormularios, datosProyecto, nroAsociado) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A2c_${nroAsociado}`);
    const datosFormulario = await obtenerDatos(datosProyecto, nroAsociado);
    const rep = new Reporte('formulario_A2c');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto, nroAsociado) {
    if (!datosProyecto.documentacion_exp.datos_asociacion || !datosProyecto.documentacion_exp.datos_asociacion.asociados) {
      throw new errors.ValidationError(`No se encontró el reporte de asociados integrantes.`);
    }
    const datos = datosProyecto.documentacion_exp.datos_asociacion.asociados[nroAsociado-1];
    if (!datos) {
      throw new errors.ValidationError(`No se encontró el reporte del integrante. ${nroAsociado}`);
    }
    const fechaInscripcion = moment(datos.fecha_inscripcion, 'YYYY-MM-DD');
    let datosFormularioA2c = {
      datos_generales: {
        razon_social: datos.razon_social,
        nit: datos.nit,
        nro_matricula: datos.nro_matricula,
        fecha_inscripcion: {
          dia: fechaInscripcion.format('DD'),
          mes: fechaInscripcion.format('MM'),
          anio: fechaInscripcion.format('YYYY')
        }
      },
      datos_complementarios: {}
    };
    if(datos.representante_legal && datos.representante_legal.persona) {
      const fechaExpedicion = moment(datos.representante_legal.poder.fecha_expedicion, 'YYYY-MM-DD');
      datosFormularioA2c.datos_complementarios = {
        representante_legal: {
          primer_apellido: datos.representante_legal.persona.primer_apellido,
          segundo_apellido: datos.representante_legal.persona.segundo_apellido,
          nombres: datos.representante_legal.persona.nombres,
          ci: datos.representante_legal.persona.documento_identidad,
          poder: {
            nro_testimonio: datos.representante_legal.poder.nro_testimonio,
            lugar_emision: datos.representante_legal.poder.lugar_emision,
            fecha_expedicion: {
              dia: datos.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('DD'): '',
              mes: datos.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('MM'): '',
              anio: datos.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('YYYY'): ''
            }
          }
        }
      };
    } else {
      datosFormularioA2c.datos_complementarios = {
        representante_legal: {
          primer_apellido: '---',
          segundo_apellido: '---',
          nombres: '---',
          ci: '---',
          poder: {
            nro_testimonio: '---',
            lugar_emision: '---',
            fecha_expedicion: {
              dia: '---',
              mes: '---',
              anio: '---'
            }
          }
        }
      };
    }
    return datosFormularioA2c;
  }

  app.dao.formularioA2c.crear = crear;
};
