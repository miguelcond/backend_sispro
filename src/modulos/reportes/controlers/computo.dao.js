import Reporte from '../../../lib/reportes';
import util from '../../../lib/util';
import path from 'path';
import fs from 'fs-extra';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.computo = {};

  async function crear (rutaComputos, datosProyecto, nombreModulo, item, resumen, computos, fotografias, nroPlanilla) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`CM_${item.nro_item}_${nroPlanilla}`);
    const datosReporte = await obtenerDatos(datosProyecto, nombreModulo, item, resumen, computos, fotografias, nroPlanilla);
    const rep = new Reporte('computo_metrico');
    return rep.pdf(datosReporte, { orientation: 'landscape', output: `${rutaComputos}/${nombreArchivo}` });
  }

  function obtenerDatosComputo (computo) {
    return {
      descripcion: computo.descripcion,
      nro_veces: util.convertirNumeroAString(computo.cantidad),
      factor_forma: util.convertirNumeroAString(computo.factor_forma),
      largo: util.convertirNumeroAString(computo.largo),
      ancho: util.convertirNumeroAString(computo.ancho),
      alto: util.convertirNumeroAString(computo.alto),
      area: util.convertirNumeroAString(computo.area),
      cantidad_parcial: util.convertirNumeroAString(computo.cantidad_parcial)
      // largo: util.convertirNumeroAString(computo.largo || 1),
      // ancho: util.convertirNumeroAString(computo.ancho || 1),
      // alto: util.convertirNumeroAString(computo.alto || 1),
      // area: util.convertirNumeroAString(computo.area || 1),
      // cantidad_parcial: util.convertirNumeroAString(computo.cantidad_parcial || 1)
    };
  }

  async function obtenerDatos (datosProyecto, nombreModulo, item, resumen, computos, fotografias, nroPlanilla) {
    let subtitulos = [];
    let computo = computos[0];
    let sub = computo.subtitulo || 'DEFAULT';
    let COMPUTOS = [];

    for (let i = 0; i < computos.length; i++) {
      computo = computos[i];
      if (computo.subtitulo !== sub) {
        if(COMPUTOS.length>0){
          subtitulos.push({
            nombre: sub,
            mostrar: (sub !== 'DEFAULT'),
            computos: COMPUTOS
          });
          COMPUTOS = [];
        }
        sub = computo.subtitulo || 'DEFAULT';
      }
      COMPUTOS.push(obtenerDatosComputo(computo));
    }

    if (COMPUTOS.length>0) {
      sub = sub || 'DEFAULT';
      subtitulos.push({
        nombre: sub,
        mostrar: (sub !== 'DEFAULT'),
        computos: COMPUTOS
      });
    }

    const TOTAL = util.convertirNumeroAString(resumen.total_actual);
    const FOTOGRAFIAS = {
      fotos: []
    };
    for (let i in fotografias) {
      const FOTO = fotografias[i];
      let pathv1 = `${_path}/uploads/${FOTO.path}`;
      let pathv2 = `${_path}/uploads/proyectos/${datosProyecto.codigo}/fotos/${FOTO.path}`;
      // let PATH = `${_path}/public/imagen/Imagen_no_disponible.jpg`;
      let PATH = null;
      if(fs.existsSync(pathv1)) {
        PATH = pathv1;
      }
      if(fs.existsSync(pathv2)) {
        PATH = pathv2;
      }
      // const PATH = `file://${path.resolve(`${__dirname}`, `./../../../../uploads/${FOTO.path}`)}`;
      if (FOTO.nombre.startsWith('foto')) {
        const NRO_FOTO = FOTO.nombre.substr(4, FOTO.nombre.indexOf('_') - 4);
        const TITULO = `Foto N° ${NRO_FOTO}`;
        if(PATH) {
          PATH = `file://${PATH}`;
          let foto = FOTOGRAFIAS.fotos.find((e)=>{ return e.titulo===TITULO;});
          if (foto) {
            foto.path = PATH;
          } else {
            FOTOGRAFIAS.fotos.push({ path: PATH, titulo: TITULO });
          }
        }
      }
    }
    FOTOGRAFIAS.fotos = _.orderBy(FOTOGRAFIAS.fotos, ['titulo'], ['asc']);
    let datosReporte = {
      nombre_proyecto: datosProyecto.nombre,
      nro_planilla: nroPlanilla,
      item: {
        modulo: nombreModulo,
        nombre: item.nombre,
        nro_item: item.nro_item,
        cantidad: item.cantidad,
        unidad_medida: item.unidad.nombre.toUpperCase()
      },
      resumen: {
        cantidad_contractual: util.convertirNumeroAString(item.cantidad),
        cantidad_ejecutada: util.convertirNumeroAString(resumen.cant_ejecutada),
        cantidad_faltante: util.convertirNumeroAString(resumen.cant_faltante)
      },
      subtitulos: subtitulos,
      total: TOTAL,
      fotografias: FOTOGRAFIAS
    };
    return datosReporte;
  }

  app.dao.computo.crear = crear;
};
