import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.formularioA2b = {};

  async function crear (rutaFormularios, datosProyecto) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A2b`);
    const datosFormulario = await obtenerDatos(datosProyecto);
    const rep = new Reporte('formulario_A2b');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto) {
    const datos = datosProyecto.documentacion_exp;
    if (!datos) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    const tipo = datos.datos_empresa.tipo;
    const fechaInscripcion = moment(datos.datos_asociacion.fecha_inscripcion, 'YYYY-MM-DD');
    const fechaTestimonio = moment(datos.datos_asociacion.fecha_expedicion, 'YYYY-MM-DD');
    const fechaExpedicion = moment(datos.datos_empresa.representante_legal.poder.fecha_expedicion, 'YYYY-MM-DD');
    const datosFormularioA2b = {
      datos_asociacion: {
        nombre_asociacion: datos.datos_asociacion.nombre_asociacion,
        asociados: datos.datos_asociacion.asociados, // parsear solo datos necesarios
        nro_testimonio: datos.datos_asociacion.nro_testimonio,
        lugar_testimonio: datos.datos_asociacion.lugar_testimonio,
        fecha_inscripcion: {
          dia: fechaTestimonio.format('DD'),
          mes: fechaTestimonio.format('MM'),
          anio: fechaTestimonio.format('YYYY')
        }
      },
      datos_generales: { // datos de la empresa lider
        razon_social: datos.datos_empresa.razon_social,
        pais: datos.datos_empresa.pais,
        ciudad: datos.datos_empresa.ciudad,
        direccion: datos.datos_empresa.direccion,
        telefono: datos.datos_empresa.telefono,
      },
      datos_complementarios: {
        representante_legal: {
          primer_apellido: datos.datos_empresa.representante_legal.persona.primer_apellido,
          segundo_apellido: datos.datos_empresa.representante_legal.persona.segundo_apellido,
          nombres: datos.datos_empresa.representante_legal.persona.nombres,
          ci: datos.datos_empresa.representante_legal.persona.documento_identidad,
          lugar_expedicion: datos.datos_empresa.representante_legal.persona.lugar_expedicion_documento,
          poder: {
            nro_testimonio: datos.datos_empresa.representante_legal.poder.nro_testimonio,
            lugar_emision: datos.datos_empresa.representante_legal.poder.lugar_emision,
            fecha_expedicion: {
              dia: datos.datos_empresa.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('DD'): '',
              mes: datos.datos_empresa.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('MM'): '',
              anio: datos.datos_empresa.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('YYYY'): ''
            }
          }
        }
      },
      margen_preferencia: datos.datos_asociacion.margen_preferencia,
      informacion_contacto: {
        fax_check: datos.datos_empresa.notificacion_fax_check,
        fax: datos.datos_empresa.notificacion_fax,
        correo_check: datos.datos_empresa.notificacion_correo_check,
        correo: datos.datos_empresa.notificacion_correo
      }
    };

    return datosFormularioA2b;
  }

  app.dao.formularioA2b.crear = crear;
};
