import util from '../../../lib/util';
import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.formularioA4 = {};

  async function crear (rutaFormularios, datosProyecto) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A4`);
    const datosFormulario = await obtenerDatos(datosProyecto);
    const rep = new Reporte('formulario_A4');
    return rep.pdf(datosFormulario, { orientation: 'landscape', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto) {
    const datos = datosProyecto.documentacion_exp;
    if (!datos) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    let experienciasEspecificas = [];
    let cnt = 1;
    let totalBs = 0;
    let totalSus = 0;
    let mostrarSus = true;
    for (let i in datos.experiencias.especificas) {
      let exp = datos.experiencias.especificas[i];
      experienciasEspecificas.push({
        nro: cnt++,
        nombre_contratante: exp.nombre_contratante,
        direccion_contacto: exp.direccion_contacto,
        objeto_contratacion: exp.objeto_contratacion,
        ubicacion: exp.ubicacion,
        monto_final_contrato_bs: util.convertirNumeroAString(exp.monto_final_contrato_bs),
        monto_final_contrato_sus: util.convertirNumeroAString(exp.monto_final_contrato_sus),
        fecha_inicio_ejecucion: moment(exp.fecha_inicio_ejecucion, 'YYYY-MM-DD').format('DD/MM/YYYY'),
        fecha_fin_ejecucion: moment(exp.fecha_fin_ejecucion, 'YYYY-MM-DD').format('DD/MM/YYYY'),
        participacion: exp.participacion,
        socio: exp.socio,
        profesional_responsable: exp.profesional_responsable
      });
      totalBs += exp.monto_final_contrato_bs;
      if (exp.monto_final_contrato_sus) {
        totalSus += exp.monto_final_contrato_sus;
      } else {
        mostrarSus = false;
      }
    }
    let datosFormularioA4 = {
      empresa: datos.datos_empresa.razon_social,
      experiencias_especificas: experienciasEspecificas,
      totales: {
        total_facturado_bs: util.convertirNumeroAString(totalBs),
        total_facturado_sus: mostrarSus ? util.convertirNumeroAString(totalSus) : ''
      }
    };

    return datosFormularioA4;
  }

  app.dao.formularioA4.crear = crear;
};
