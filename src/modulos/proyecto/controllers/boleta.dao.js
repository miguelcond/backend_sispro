import errors from '../../../lib/errors';
import util from '../../../lib/util';
import seqOpt from '../../../lib/sequelize-options';
import shortid from 'shortid';
import Reporte from '../../../lib/reportes';
import moment from 'moment';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.boleta = {};
  const models = app.src.db.models;
  const output = {
    model: 'boleta',
    fields: {
      id_boleta: ['int', 'pk'],
      tipo: ['str', 'fk'],
      numero: ['str', 'fk'],
      monto: ['str', 'fk'],
      fecha_inicio_validez: ['date'],
      fecha_fin_validez: ['date'],
      doc_boleta: ['str'],
      estado: ['enum'],
      fid_proyecto: ['int', 'fk']
    }
  };

  async function crear (datos) {
    return models.boleta.create(datos);
  }

  async function crearPorLote (proyecto, boletas, idUsuario) {
    const MONTO_CCONTRATO = util.valorPorcentual(proyecto.monto_total, 7);
    await app.dao.boleta.getBoletasConFormato(proyecto.id_proyecto, boletas, MONTO_CCONTRATO, 'TB_CUMPLIMIENTO_CONTRATO', idUsuario);
    await app.dao.boleta.getBoletasConFormato(proyecto.id_proyecto, boletas, proyecto.anticipo, 'TB_CORRECTA_INVERSION', idUsuario);
    return models.boleta.bulkCreate(boletas);
  }

  async function getNumero (idProyecto) {
    const NRO_BOLETAS = await models.boleta.count({
      where: {
        fid_proyecto: idProyecto,
        estado: 'ACTIVO'
      }
    });
    return NRO_BOLETAS + 1;
  }

  async function modificar (id_boleta, datos, t) {
    return models.boleta.update(datos, {
      where: {
        id_boleta
      },
      transaction: t
    });
  }

  async function adicionar (datos, t) {
    return models.boleta.create(datos, {transaction: t});
  }

  function getBoletasConFormato (idProyecto, boletas, montoEsperado, tipo, idUsuario) {
    const BOLETAS = getBoletaPorTipo(boletas, tipo);
    let total = 0;
    for (let i = 0; i < BOLETAS.length; i++) {
      BOLETAS[i].fid_proyecto = idProyecto;
      BOLETAS[i]._usuario_creacion = idUsuario;
      BOLETAS[i].fecha_inicio_validez = moment(BOLETAS[i].fecha_inicio_validez, 'YYYY-MM-DD').toDate();
      BOLETAS[i].fecha_fin_validez = moment(BOLETAS[i].fecha_fin_validez, 'YYYY-MM-DD').toDate();
      total = util.sumar([total, BOLETAS[i].monto]);
    }
    if (total < montoEsperado) {
      throw new errors.ValidationError(`La suma de los montos de las boletas de ${tipo === 'TB_CUMPLIMIENTO_CONTRATO' ? 'Cumplimiento de contrato' : 'Anticipo'} debe ser mayor o igual al ${tipo === 'TB_CUMPLIMIENTO_CONTRATO' ? '7% del monto del contrato' : 'monto del anticipo solicitado'}.`);
    }
    return BOLETAS;
  }

  function getBoletaPorTipo (boletas, tipo) {
    return _.filter(boletas, boleta => {
      return boleta.tipo_boleta === tipo;
    });
  }

  app.dao.boleta.crear = crear;
  app.dao.boleta.modificar = modificar;
  app.dao.boleta.adicionar = adicionar;
  app.dao.boleta.crearPorLote = crearPorLote;
  app.dao.boleta.getNumero = getNumero;
  app.dao.boleta.getBoletaPorTipo = getBoletaPorTipo;
  app.dao.boleta.getBoletasConFormato = getBoletasConFormato;
};
