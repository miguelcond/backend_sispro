import util from '../../../lib/util';

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  const guardar = async (idProyecto, version) => {
    let items = util.json(await app.dao.item.obtenerDatosItems(idProyecto)).rows;
    items.map(item => {
      item.version = version;
    });
    await models.item_historico.bulkCreate(items);
  };

  const guardarAdjudicacion = async (idProyecto, idAdjudicacion, version) => {
    let items = util.json(await app.dao.item.obtenerDatosItemsAdjudicacion(idProyecto,idAdjudicacion)).rows;
    items.map(item => {
      item.version = version;
    });
    await models.item_historico.bulkCreate(items);
  };

  const cancelarModificacion = async (idProyecto, version) => {
    await sequelize.query(`UPDATE item i
      SET cantidad = ih.cantidad
      FROM item_historico ih, modulo_historico mh
      WHERE i.id_item = ih.id_item AND ih.fid_modulo = mh.id_modulo AND ih.version = mh.version AND mh.fid_proyecto = $1 AND ih.version = $2`,
      { bind: [idProyecto, version], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM item i
      WHERE i.fid_modulo in (SELECT m.id_modulo FROM modulo m
        WHERE m.fid_proyecto = $1)
      AND i.id_item NOT IN (SELECT fid_item FROM computo_metrico )
      AND i.id_item NOT IN (SELECT ih.id_item FROM item_historico ih, modulo_historico mh
        WHERE ih.fid_modulo = mh.id_modulo
        AND mh.fid_proyecto = $1 and ih.version = $2 GROUP BY ih.id_item) AND estado <> 'ELIMINADO';`,
        { bind: [idProyecto, version], type: sequelize.QueryTypes.UPDATE }
      );
    await sequelize.query(`DELETE FROM item_historico ih
      WHERE ih.version = $1
      AND ih.id_item IN (SELECT ih.id_item FROM item_historico ih, modulo_historico mh
      WHERE ih.fid_modulo = mh.id_modulo
      AND mh.fid_proyecto = $2)`,
      { bind: [version, idProyecto], type: sequelize.QueryTypes.UPDATE }
    );
  };

  const cancelarModificacionAdjudicacion = async (idProyecto, idAdjudicacion, version) => {
    await sequelize.query(`UPDATE item i
      SET cantidad = ih.cantidad
      FROM item_historico ih, modulo_historico mh
      WHERE i.id_item = ih.id_item AND ih.fid_modulo = mh.id_modulo AND ih.version = mh.version AND mh.fid_proyecto = $1 AND fid_adjudicacion = $2 AND ih.version = $3`,
      { bind: [idProyecto, idAdjudicacion, version], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM item i
      WHERE i.fid_modulo in (SELECT m.id_modulo FROM modulo m
        WHERE m.fid_proyecto = $1 AND fid_adjudicacion = $2)
      AND i.id_item NOT IN (SELECT fid_item FROM computo_metrico )
      AND i.id_item NOT IN (SELECT ih.id_item FROM item_historico ih, modulo_historico mh
        WHERE ih.fid_modulo = mh.id_modulo
        AND mh.fid_proyecto = $1 AND fid_adjudicacion = $2 and ih.version = $3 GROUP BY ih.id_item) AND estado <> 'ELIMINADO';`,
        { bind: [idProyecto, idAdjudicacion, version], type: sequelize.QueryTypes.UPDATE }
      );
    await sequelize.query(`DELETE FROM item_historico ih
      WHERE ih.version = $3
      AND ih.id_item IN (SELECT ih.id_item FROM item_historico ih, modulo_historico mh
      WHERE ih.fid_modulo = mh.id_modulo
      AND mh.fid_proyecto = $1 AND fid_adjudicacion = $2)`,
      { bind: [idProyecto, idAdjudicacion, version], type: sequelize.QueryTypes.UPDATE }
    );
  };

  _app.dao.item_historico = {
    guardar,
    guardarAdjudicacion,
    cancelarModificacion,
    cancelarModificacionAdjudicacion
  };
};
