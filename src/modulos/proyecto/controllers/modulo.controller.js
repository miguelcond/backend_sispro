const util = require('../../../lib/util');
const errors = require('../../../lib/errors');
const _ = require('lodash');

module.exports = (app) => {
  const _app = app;
  _app.controller.modulo = {};
  const moduloController = _app.controller.modulo;
  const moduloModel = app.src.db.models.modulo;
  const itemModel = app.src.db.models.item;
  const sequelize = app.src.db.sequelize;

  async function get (req, res, next) {
    try {
      const modulos = await moduloModel.moduloIncluye(app.src.lib.util.formarConsulta(req.query, moduloModel, app.src.db.models));
      res.json({
        finalizado: true,
        mensaje: 'Módulos recuperados correctamente.',
        datos: modulos
      });
    } catch (e) {
      next(e);
    }
  }

  async function getProyecto (req, res, next) {
    let consulta = app.src.lib.util.formarConsulta(req.query, moduloModel, app.src.db.models);
    if (!consulta.condicionModulo) {
      consulta.condicionModulo = {};
    }
    try {
      await app.dao.proyecto.getPorId(req.params.id);
      consulta.condicionModulo.fid_proyecto = req.params.id;
      const result = await moduloModel.moduloIncluye(consulta);
      res.json({
        finalizado: true,
        mensaje: 'Módulos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  }

  async function getProyectoAdjudicacion (req, res, next) {
    let consulta = app.src.lib.util.formarConsulta(req.query, moduloModel, app.src.db.models);
    if (!consulta.condicionModulo) {
      consulta.condicionModulo = {};
    }
    try {
      await app.dao.proyecto.getPorId(req.params.id);
      consulta.condicionModulo.fid_proyecto = req.params.id;
      consulta.condicionModulo.fid_adjudicacion = req.params.id_adjudicacion;
      const result = await moduloModel.moduloConsulta(consulta);
      res.json({
        finalizado: true,
        mensaje: 'Módulos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  }

  async function getId (req, res, next) {
    try {
      const modulo = await app.dao.modulo.obtenerModuloId(req.params.id);
      if (!modulo) {
        throw new errors.NotFoundError(`No exite el módulo solicitado.`);
      }
      res.json({
        finalizado: true,
        mensaje: 'Módulo recuperado correctamente.',
        datos: modulo
      });
    } catch (e) {
      next(e);
    }
  }

  moduloController.post = (req, res, next) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const modulo = req.body;
    sequelize.transaction().then((t) => {
      _app.dao.proyecto.obtenerEstado(modulo.fid_proyecto, t)
      .then((result) => {
        if (!result.estado_actual.atributos_detalle || (result.estado_actual.atributos_detalle.modulo.acciones.indexOf('post') === -1)) {
          throw new errors.ValidationError(`No puede modificar un proyecto en estado "${result.estado_actual.nombre}".`);
        } else {
          let moduloRes = {};
          util.asignarValorAtributos(moduloRes, modulo, result.estado_actual.atributos_detalle.modulo.post);
          return moduloRes;
        }
      }).then((result) => {
        result._usuario_creacion = idUsuario;
        return moduloModel.create(result, {
          transaction: t
        });
      }).then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Módulo guardado correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        next(error);
      });
    });
  };

  moduloController.put = (req, res, next) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const modulo = req.body;
    sequelize.transaction().then((t) => {
      _app.dao.proyecto.obtenerEstado(modulo.fid_proyecto, t)
      .then((result) => {
        if (!result.estado_actual.atributos_detalle || (result.estado_actual.atributos_detalle.modulo.acciones.indexOf('put') === -1)) {
          throw new errors.ValidationError(`No puede modificar un proyecto en estado "${result.estado_actual.nombre}".`);
        } else {
          return app.dao.modulo.actualizar(req.params.id, modulo, result.estado_actual.atributos_detalle, idUsuario, t);
        }
      }).then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Módulo guardado correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        next(error);
      });
    });
  };

  const _eliminarItems = async (idProyecto, items) => {
    const ITEMS = _.map(items, 'id_item');
    await app.dao.item.eliminar(ITEMS);
    for (var i = 0; i < ITEMS.length; i++) {
      await app.dao.proyecto.eliminarArchivo(idProyecto, `ET_${ITEMS[i]}`);
    }
  };

  const eliminar = async (req, res, next) => {
    const ID_MODULO = req.params.id;
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const MODULO = await app.dao.modulo.obtenerModuloId(ID_MODULO);
        let proyecto = util.json(await app.dao.modulo.obtenerEstadoProyecto(MODULO.id_modulo)).proyecto;
        if (!proyecto.estado_actual.atributos_detalle || (proyecto.estado_actual.atributos_detalle.modulo.acciones.indexOf('put') === -1)) {
          throw new errors.ValidationError(`No puede modificar un proyecto en estado "${proyecto.estado_actual.nombre}".`);
        }
        const ITEMS = await app.dao.item.obtenerDeModulo(MODULO.id_modulo);
        await _eliminarItems(proyecto.id_proyecto, ITEMS);
        await app.dao.item.actualizarNro(proyecto.id_proyecto);
        await app.dao.modulo.eliminar(MODULO.id_modulo);
        res.json({
          finalizado: true,
          mensaje: 'Módulo eliminado correctamente.',
          datos: null
        });
      } catch (error) {
        _t.rollback();
        next(error);
      }
    });
  };

  moduloController.get = get;
  moduloController.getProyecto = getProyecto;
  moduloController.getProyectoAdjudicacion = getProyectoAdjudicacion;
  moduloController.getId = getId;
  moduloController.eliminar = eliminar;
};
