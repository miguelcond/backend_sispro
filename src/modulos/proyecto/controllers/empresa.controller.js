import fundempresa from '../../../services/fundempresa/fundempresa';

module.exports = (app) => {
  const _app = app;
  _app.controller.empresa = {};
  const empresaController = _app.controller.empresa;

  empresaController.get = (req, res) => {
    fundempresa.obtenerEmpresa(req.params.matricula).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Empresa recuperada correctamente.',
        datos: result.SrvMatriculaListSucResult.MatriculaResultSuc
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };

  empresaController.getMatriculas = (req, res) => {
    fundempresa.obtenerMatriculas(req.params.nit).then((result) => {
      if (result.SrvMatriculaConsultaNitResult.MatriculasResult[0].CtrResult !== 'D-EXIST') {
        throw new Error('No se encontraron matrículas para el nit indicado en fundempresa.');
      }
      res.json({
        finalizado: true,
        mensaje: 'Matriculas recuperadas correctamente.',
        datos: result.SrvMatriculaConsultaNitResult.MatriculasResult
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };
};
