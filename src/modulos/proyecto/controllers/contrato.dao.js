module.exports = (app) => {
  const _app = app;
  _app.dao.contrato = {};
  const contratoModel = app.src.db.models.contrato;
  const models = app.src.db.models;

  async function crearRecuperar (contrato, idUsuario, t) {
    let contratoRes = await contratoModel.findOne({
      where: {
        fid_proyecto: contrato.fid_proyecto,
        fid_empresa: contrato.fid_empresa
      },
      transaction: t
    });
    if (contratoRes) {
      return contratoRes;
    } else {
      contrato._usuario_creacion = idUsuario;
      return contratoModel.create(contrato, {
        transaciton: t
      });
    }
  };

  async function getEmpresa (idProyecto) {
    const CONTRATO = await models.contrato.findOne({
      where: {
        fid_proyecto: idProyecto,
        estado: 'ACTIVO'
      },
      include: {
        model: models.empresa,
        as: 'empresa'
      },
      order: [['id_contrato', 'DESC']]
    });
    return CONTRATO.empresa;
  }

  _app.dao.contrato.crearRecuperar = crearRecuperar;
  _app.dao.contrato.getEmpresa = getEmpresa;
};
