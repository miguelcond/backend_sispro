import errors from './../../../lib/errors';
import _ from 'lodash';
import util from '../../../lib/util';

module.exports = (app) => {
  const _app = app;
  _app.controller.computoMetrico = {};
  const computoMetricoController = _app.controller.computoMetrico;
  const computoMetricoModel = app.src.db.models.computo_metrico;
  const sequelize = app.src.db.sequelize;
  const models = app.src.db.models;

  async function get (req, res, next) {
    try {
      const idItem = req.query.item;
      const idSupervision = req.query.supervision;
      let fotos, sumaTotalSupervisiones;
      // TODO: falta validar que los datos los obtenga solo el dueño de la informacion ademas del tecnico o supervisor
      const xfotos = await _app.dao.fotoSupervision.obtenetFotos(idSupervision, idItem);
      fotos = xfotos.map(foto => {
        return {
          id_foto_supervision: foto.id_foto_supervision,
          codigo: foto.path_fotografia.substring(0, foto.path_fotografia.indexOf('_')),
          nombre: foto.path_fotografia.substring(foto.path_fotografia.indexOf('_') + 1)
        };
      });
      const sum = await computoMetricoModel.getSumCantidadParcial(idItem, 'lt', idSupervision);
      sumaTotalSupervisiones = sum || 0;
      const COMPUTOS = await _app.dao.computoMetrico.getPorItemSupervision(idItem, idSupervision);
      res.json({
        finalizado: true,
        mensaje: 'Cómputos métricos obtenidos correctamente',
        datos: {
          cantidad_anterior: sumaTotalSupervisiones,
          computos: COMPUTOS,
          fotos: fotos
        }
      });
    } catch (error) {
      next(error);
    }
  }

  async function post (req, res, next) {
    const idSupervision = req.body.fid_supervision;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const computo = req.body;
    sequelize.transaction().then((t) => {
      _app.dao.computoMetrico.crearRecuperar(computo, idSupervision, idUsuario, t)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Cómputo métrico registrado correctamente',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        next(error);
      });
    });
  }

  async function put (req, res, next) {
    const idSupervision = req.body.fid_supervision;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const computo = req.body;
    computo.id_computo_metrico = req.params.id;
    return sequelize.transaction(async (t) => {
      return _app.dao.computoMetrico.crearRecuperar(computo, idSupervision, idUsuario, t);
    }).then(result => {
      res.json({
        finalizado: true,
        mensaje: 'Cómputo métrico actualizado correctamente',
        datos: result
      });
    }).catch(error => {
      next(error);
    });
  }

  async function actualizarComputoMetricoItem (req, res, next) {
    let computos = req.body.computos;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    return sequelize.transaction(async (t) => {
      computos = await validarDatosComputos(computos);
      let promesas = [];
      computos.forEach(computo => {
        promesas.push(_app.dao.computoMetrico.crearRecuperar(computo, computo.fid_supervision, ID_USUARIO, t, false));
      });
      return Promise.all(promesas);
    }).then((result) => {
      res.json({
        finalizado: true,
        mensaje: result.length > 1 ? 'Cómputos métricos actualizados correctamente.' : 'Cómputo métrico actualizado correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  }

  async function validarDatosComputos (lista) {
    if (lista.length === 0) {
      throw new errors.ValidationError(`No tiene cómputos métricos`);
    }
    const ID_SUPERVISIONES = _.uniq(_.map(lista, 'fid_supervision'));
    for (var i = 0; i < ID_SUPERVISIONES.length; i++) {
      if (!await _app.dao.supervision.existe(ID_SUPERVISIONES[i])) {
        throw new errors.ValidationError(`La supervisión no existe`);
      }
    }
    const ITEM = await app.dao.item.obtenerPorId(lista[0].fid_item);
    let sumCantidadEjecutada = 0;
    for (let i = 0; i < lista.length; i++) {
      await _app.dao.computoMetrico.calcularDatosComputos(lista[i], ITEM);
      sumCantidadEjecutada = util.sumar([sumCantidadEjecutada, lista[i].cantidad_parcial]);
    }
    if (sumCantidadEjecutada > ITEM.cantidad) {
      throw new errors.ValidationError(`**La cantidad ejecutada sobrepasa a la cantidad contractual.`);
    }
    return lista;
  }

  async function eliminarComputo (req, res, next) {
    const idComputo = req.params.id;
    const idUsuario = req.body.audit_usuario.id_usuario;
    return sequelize.transaction(async (t) => {
      return _app.dao.computoMetrico.eliminar(idComputo, idUsuario, t);
    }).then(result => {
      res.json({
        finalizado: true,
        mensaje: 'Cómputo métrico eliminado correctamente',
        datos: result
      });
    }).catch(error => {
      next(error);
    });
  }

  async function deleteAll (req, res, next) {
    const idSupervision = req.params.id;
    const idItem = req.params.idItem;
    const idUsuario = req.body.audit_usuario.id_usuario;
    return sequelize.transaction(async (t) => {
      await _app.dao.computoMetrico.eliminarTodos(idSupervision, idItem, idUsuario, t)
      .then(() => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Cómputos métricos eliminados correctamente',
          datos: null
        });
      }).catch(error => {
        t.rollback();
        next(error);
      });
    });
  }

  async function getSupervision(req, res, next){
    const id_supervision = req.params.id;
    sequelize.transaction().then((t) => {
      _app.dao.computoMetrico.getSupervision(id_supervision)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Computos Metricos obtenidos correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  }

  computoMetricoController.get = get;
  computoMetricoController.post = post;
  computoMetricoController.put = put;
  computoMetricoController.actualizarComputoMetricoItem = actualizarComputoMetricoItem;
  computoMetricoController.delete = eliminarComputo;
  computoMetricoController.deleteAll = deleteAll;
  computoMetricoController.getSupervision = getSupervision;
};
