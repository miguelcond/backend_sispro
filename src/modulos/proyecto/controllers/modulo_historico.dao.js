import util from '../../../lib/util';
const errors = require('../../../lib/errors');

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  const obtener = async (idProyecto, idAdjudicacion, version) => {
    let query = app.dao.modulo.filtrosPorProyecto(idProyecto,idAdjudicacion);
    query.include.model = models.item_historico;
    // const SUPERVISION_ANTERIOR = await app.dao.supervision.obtenerPorVersion(idProyecto, version - 1);
    const SUPERVISION_ANTERIOR = await app.dao.supervision.obtenerPorVersionAdjudicacion(idAdjudicacion, version - 1);
    query.include.attributes.push([sequelize.literal(`(
      SELECT COALESCE(SUM(cm.cantidad_parcial), 0)
      FROM computo_metrico cm
      WHERE cm.fid_item = "items".id_item and cm.fid_supervision <= ${SUPERVISION_ANTERIOR ? SUPERVISION_ANTERIOR.id_supervision : 0}
        AND cm.estado = 'ACTIVO'
    )`), 'avance_actual']);
    query.include.include.push({
      attributes: ['cantidad'],
      model: models.item_historico,
      as: 'version_anterior',
      where: {
        version: version - 1
      },
      required: false
    });
    query.order = [[{model: models.item_historico, as: 'items'}, 'nro_item']];
    if (version) {
      query.where.version = version;
      query.include.where.version = version;
    }
    return models.modulo_historico.findAll(query);
  };

  const guardar = async (idProyecto, version) => {
    let modulos = util.json(await app.dao.modulo.obtenerModulosProyecto(idProyecto));
    modulos.map(modulo => {
      modulo.version = version;
    });
    await models.modulo_historico.bulkCreate(modulos);
    await app.dao.item_historico.guardar(idProyecto, version);
  };

  const guardarAdjudicacion = async (idProyecto, idAdjudicacion, version) => {
    let modulos = util.json(await app.dao.modulo.obtenerModulosAdjudicacionProyecto(idProyecto,idAdjudicacion));
    modulos.map(modulo => {
      modulo.version = version;
    });
    await models.modulo_historico.bulkCreate(modulos);
    await app.dao.item_historico.guardarAdjudicacion(idProyecto, idAdjudicacion, version);
  };

  const cancelarModificacion = async (idProyecto, version) => {
    await app.dao.item_historico.cancelarModificacion(idProyecto, version);
    await sequelize.query(`UPDATE modulo m SET nombre = mh.nombre
      FROM modulo_historico mh
      WHERE m.id_modulo = mh.id_modulo AND mh.fid_proyecto = $1 AND mh.version = $2;`,
       { bind: [idProyecto, version], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM modulo
      WHERE id_modulo NOT IN (SELECT id_modulo FROM modulo_historico WHERE fid_proyecto = $1 and version = $2)
      AND id_modulo NOT IN (SELECT fid_modulo FROM item)
      AND fid_proyecto = $1;`,
      { bind: [idProyecto, version], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM modulo_historico
      WHERE fid_proyecto = $1 AND version = $2;`,
      { bind: [idProyecto, version], type: sequelize.QueryTypes.UPDATE }
    );
  };

  const cancelarModificacionAdjudicacion = async (idProyecto,idAdjudicacion, version) => {
    await app.dao.item_historico.cancelarModificacionAdjudicacion(idProyecto, idAdjudicacion, version);
    await sequelize.query(`UPDATE modulo m SET nombre = mh.nombre
      FROM modulo_historico mh
      WHERE m.id_modulo = mh.id_modulo AND mh.fid_proyecto = $1 AND mh.fid_adjudicacion = $2 AND mh.version = $3;`,
       { bind: [idProyecto, idAdjudicacion, version], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM modulo
      WHERE id_modulo NOT IN (SELECT id_modulo FROM modulo_historico WHERE fid_proyecto = $1 and fid_adjudicacion = $2 and version = $3)
      AND id_modulo NOT IN (SELECT fid_modulo FROM item)
      AND fid_proyecto = $1 AND fid_adjudicacion = $2;`,
      { bind: [idProyecto, idAdjudicacion, version], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM modulo_historico
      WHERE fid_proyecto = $1 AND fid_adjudicacion = $2 AND version = $3;`,
      { bind: [idProyecto, idAdjudicacion, version], type: sequelize.QueryTypes.UPDATE }
    );
  };

  /**
   * Obtiene los módulos de la última versión de un proyecto
   */
  const obtenerUltimaVersion = async (idProyecto) => {
    let consulta = {
      where: {
        fid_proyecto: idProyecto,
        version: {
          $eq: sequelize.literal(`(SELECT version from proyecto WHERE id_proyecto = ${idProyecto}) - 1`)
        }
      },
      attributes: ['id_modulo']
    };
    return models.modulo_historico.findAll(consulta);
  };

  const obtenerDatosProyecto = async (idProyecto, idSupervision, idAdjudicacion, conHistoricos = false, version) => {
    let consulta = app.dao.modulo.filtrosPorProyecto(idProyecto,idAdjudicacion);
    consulta.include.model = models.item_historico;
    consulta.include.where.version = version;
    consulta.order = [[{model: models.item_historico, as: 'items'}, 'nro_item']];
    consulta.where.version = version;
    // consulta.include.attributes.push([sequelize.fn('COALESCE', sequelize.literal(`(SELECT SUM(cantidad_parcial) FROM computo_metrico WHERE fid_item = "items"."id_item" AND estado <> 'ELIMINADO' AND fid_supervision < ${idSupervision})::Decimal(12, 2)::float`), 0), 'cantidad_anterior_fis']);
    // consulta.include.attributes.push([sequelize.fn('COALESCE', sequelize.literal(`(SELECT SUM(cantidad_parcial) FROM computo_metrico WHERE fid_item = "items"."id_item" AND estado <> 'ELIMINADO' AND fid_supervision = ${idSupervision})::Decimal(12, 2)::float`), 0), 'cantidad_actual_fis']);
    consulta.include.attributes.push([sequelize.literal(`(CASE WHEN(SELECT cantidad FROM item_historico
      WHERE id_item="items"."id_item" AND version = ${version - 1}) = "items"."cantidad" THEN false ELSE true END)`), 'modificado']);
    if (conHistoricos) {
      consulta.include.include.push({
        model: models.item_historico,
        as: 'historicos',
        where: {
          version: {
            $lt: version
          }
        },
        required: false,
        attributes: ['version', 'cantidad', 'precio_unitario',
          [sequelize.literal(`("items->historicos"."cantidad" * "items->historicos"."precio_unitario")::Decimal(12, 2)::float`), 'total'],
          [sequelize.literal(`(CASE WHEN "items->historicos".version = 1 OR (SELECT ih.cantidad FROM item_historico ih WHERE ih.id_item="items->historicos"."id_item" AND ih.version="items->historicos".version - 1) = "items->historicos"."cantidad" THEN false ELSE true END)`), 'modificado'],
          [sequelize.literal(`(CASE WHEN "items->historicos".version = 1 THEN 'INICIAL' ELSE (SELECT tipo_modificacion FROM proyecto_historico WHERE id_proyecto = ${idProyecto} AND version = "items->historicos".version) END)`), 'modificacion']
        ]
      });
      consulta.order.push([sequelize.literal('"items->historicos".version'), 'ASC']);
    }
    let modulos = await models.modulo_historico.findAll(consulta);
    if (!modulos) {
      throw new errors.ValidationError(`No se encontró los módulos para el proyecto solicitado`);
    }
    modulos = util.json(modulos);
    modulos.map(modulo => {
      modulo.items.map(item => {
        item.item_historico = item.historicos;
        delete item.historicos;
      });
    });
    return modulos;
  };

  _app.dao.modulo_historico = {
    obtener,
    guardar,
    guardarAdjudicacion,
    cancelarModificacion,
    cancelarModificacionAdjudicacion,
    obtenerUltimaVersion,
    obtenerDatosProyecto
  };
};
