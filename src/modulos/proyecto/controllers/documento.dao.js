import fs from 'fs-extra';
import fileType from 'file-type';
import shortid from 'shortid';

module.exports = (app) => {
  const _app = app;
  _app.dao.documento = {};
  const documentoModel = app.src.db.models.documento;

  async function crearRecuperar (documento, idProyecto, idUsuario, t) {
    let documentos = [];
    for (let i = 0; i < documento.length; i++) {
      let decoded;
      let tipo = null;
      const partes = documento[i].base64.split('base64,');
      if (partes.length === 2) {
        decoded = Buffer.from(partes[1], 'base64');
        tipo = partes[0];
      } else {
        decoded = Buffer.from(partes[0], 'base64');
        let type = fileType(decoded);
        if (type) {
          tipo = type.mime;
        }
      }

      const dir = `${_path}/uploads`;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }

      documento[i].codigo = shortid.generate();
      fs.writeFileSync(`${dir}/${documento[i].codigo}-${documento[i].nombre}`, decoded);

      documentos.push({
        codigo: documento[i].codigo,
        fid_proyecto: idProyecto,
        path_documento: documento[i].nombre,
        tipo_documento: tipo,
        _usuario_creacion: idUsuario
      });
    }

    return documentoModel.bulkCreate(documentos, {
      transaction: t
    });
  };

  _app.dao.documento.crearRecuperar = crearRecuperar;
};
