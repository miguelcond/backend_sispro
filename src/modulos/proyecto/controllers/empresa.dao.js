import empresas from '../../../services/empresas/empresas';
import errors from './../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.empresa = {};
  const empresaModel = app.src.db.models.empresa;

  async function obtenerEmpresa (matricula) {
    let result = await empresaModel.findOne({
      where: { matricula },
    });
    return result;
  }

  async function crearRecuperar (empresa, idUsuario, t) {
    let result = await empresaModel.findOne({
      where: { matricula: empresa.matricula },
      transaction: t
    });
    if (result) {
      return result;
    } else {
      empresa = await empresaModel.create({
        matricula: empresa.matricula,
        nit: empresa.nit,
        nombre: empresa.nombre,
        tipo: empresa.tipo,
        otro: empresa.otro,
        pais: empresa.pais,
        ciudad: empresa.ciudad,
        direccion: empresa.direccion,
        telefono: empresa.telefono,
        _usuario_creacion: idUsuario
      }, {
        transaction: t
      });
      return empresa;
    }
  };

  async function obtenerDocumentacionExp (matricula, codigo) {
    if (codigo) {
      let res = await empresas.obtenerDocumentacionExp(codigo, matricula);
      if (res.finalizado === true) {
        if (res.datos.rows.length === 0) {
          throw new errors.NotFoundError(`No se encontró el formulario solicitado`);
        }
        return res.datos.rows[0].contenido;
      } else {
        if (res.codigo === 404) {
          throw new errors.NotFoundError(res.mensaje);
        }
        throw new Error(`Error al obtener los datos desde el sistema EMPRESAS.`);
      }
    } else {
      return null;
    }
  }

  function contrato (nroInvitacion, documentacionExp) {
    if (nroInvitacion && documentacionExp) {
      return {
        nro_invitacion: nroInvitacion,
        nombre_representante_legal: documentacionExp.datos_empresa.representante_legal.persona.nombre,
        ci_representante_legal: documentacionExp.datos_empresa.representante_legal.persona.documento_identidad,
        numero_testimonio: documentacionExp.datos_empresa.representante_legal.poder.nro_testimonio,
        lugar_emision: documentacionExp.datos_empresa.representante_legal.poder.lugar_emision,
        fecha_expedicion: documentacionExp.datos_empresa.representante_legal.poder.fecha_expedicion,
        fax: documentacionExp.datos_empresa.fax,
        correo: documentacionExp.datos_empresa.correo,
        empresa: {
          matricula: documentacionExp.datos_empresa.nro_matricula,
          nit: documentacionExp.datos_empresa.nit,
          nombre: documentacionExp.datos_empresa.razon_social,
          tipo: documentacionExp.datos_empresa.tipo,
          pais: documentacionExp.datos_empresa.pais,
          ciudad: documentacionExp.datos_empresa.ciudad,
          direccion: documentacionExp.datos_empresa.direccion,
          telefono: documentacionExp.datos_empresa.telefono
        }
      };
    } else {
      return null;
    }
  }

  _app.dao.empresa.obtenerEmpresa = obtenerEmpresa;
  _app.dao.empresa.crearRecuperar = crearRecuperar;
  _app.dao.empresa.obtenerDocumentacionExp = obtenerDocumentacionExp;
  _app.dao.empresa.contrato = contrato;
};
