const validate = require('express-validation');
const paramValidation = require('./modulo.validation');
const sequelizeFormly = require('sequelize-formly');

module.exports = (app) => {
  /**
   * @apiVersion 1.0.0
   * @apiGroup Modulos
   * @apiName Get modulos
   * @api {get} /modulos Obtiene los modulos
   *
   * @apiDescription Get modulos, Obtiene los modulos y opcionalmente se puede indicar el estado
   *
   * @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre los modulos
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Modulos recuperados correctamente.",
      "datos": {
        "count": 3,
        "rows": [
          {
            "id_modulo": 1,
            "fid_proyecto": 2,
            "nombre": "M03 - OBRAS ARQUITECTÓNICAS",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:09:41.564Z",
            "_fecha_modificacion": "2017-09-06T22:09:41.564Z"
          },
          {
            "id_modulo": 2,
            "fid_proyecto": 2,
            "nombre": "M02 - OBRAS ESTRUCTURALES",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:09:58.872Z",
            "_fecha_modificacion": "2017-09-06T22:09:58.872Z"
          },
          {
            "id_modulo": 3,
            "fid_proyecto": 2,
            "nombre": "M03 - OBRAS ARQUITECTONICAS",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:10:09.086Z",
            "_fecha_modificacion": "2017-09-06T22:10:09.086Z"
          }
        ]
      }
    }
  */
  app.api.get('/modulos', validate(paramValidation.get), app.controller.modulo.get);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Modulos
   * @apiName Get Proyecto
   * @api {get} /api/v1/modulos/proyectos/:id Obtiene los modulos de un proyecto
   *
   * @apiParam id {number} Identificador del proyecto
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Módulos recuperados correctamente.",
      "datos": {
        "count": 5,
        "rows": [
          {
            "estado": "ACTIVO",
            "fid_proyecto": 1,
            "id_modulo": 1,
            "items": [
              {
                "analisis_precios": null,
                "avance_total": null,
                "cantidad": 1,
                "especificaciones": null,
                "estado": "ACTIVO",
                "fid_modulo": 1,
                "id_item": 1,
                "nombre": "TALLERES DE ARRANQUE Y PROFUNDIZACION DEL PROYECTO",
                "nro_item": 1,
                "precio_unitario": 1200,
                "subtotal": 1200,
                "tiempo_ejecucion": null,
                "unidad_medida": "UN_GLOBAL",
                "_fecha_creacion": "2018-07-10T19:02:31.977Z",
                "_fecha_modificacion": "2018-07-10T19:02:31.977Z",
                "_usuario_creacion": 37,
                "_usuario_modificacion": null
              },
              {
                "analisis_precios": null,
                "avance_total": null,
                "cantidad": 1,
                "especificaciones": null,
                "estado": "ACTIVO",
                "fid_modulo": 1,
                "id_item": 2,
                "nombre": "TALLERES DE CAPACITACION EN INNOVACION PRODUCTIVA FRUTICOLA",
                "nro_item": 2,
                "precio_unitario": 1000,
                "subtotal": 1000,
                "tiempo_ejecucion": null,
                "unidad_medida": "UN_PIEZA",
                "_fecha_creacion": "2018-07-10T19:02:31.977Z",
                "_fecha_modificacion": "2018-07-10T19:02:31.977Z",
                "_usuario_creacion": 37,
                "_usuario_modificacion": null
              }
            ],
            "nombre": "COMPONENTE 1: CAPACITACION Y ASISTENCIA TECNICA ESPECIALIZADA|RESULTADO 1.1: TALLERES DE CAPACITACION",
            "show": true,
            "subtotal": 2200,
            "_fecha_creacion": "2018-07-10T19:02:31.977Z",
            "_fecha_modificacion": "2018-07-10T19:02:31.977Z",
            "_usuario_creacion": 37,
            "_usuario_modificacion": null
          },
          {
            "estado": "ACTIVO",
            "fid_proyecto": 1,
            "id_modulo": 6,
            "items": [
              {
                "analisis_precios": null,
                "avance_total": null,
                "cantidad": 29.86,
                "especificaciones": null,
                "estado": "ACTIVO",
                "fid_modulo": 6,
                "id_item": 7,
                "nombre": "REPLANTEO Y CONTROL TUBERÍA",
                "nro_item": 3,
                "precio_unitario": 5000,
                "subtotal": 149300,
                "tiempo_ejecucion": null,
                "unidad_medida": "UN_METRO_CUB",
                "_fecha_creacion": "2018-07-10T19:02:31.977Z"
                "_fecha_modificacion": "2018-07-10T19:02:31.977Z"
                "_usuario_creacion": 37,
                "_usuario_modificacion": null
              },
              {
                "analisis_precios": null,
                "avance_total": null,
                "cantidad": 382.84,
                "especificaciones": null,
                "estado": "ACTIVO",
                "fid_modulo": 6,
                "id_item": 8,
                "nombre": "EXCAVACIÓN SUELO COMÚN (0­2 M)",
                "nro_item": 4,
                "precio_unitario": 450,
                "subtotal": 172278,
                "tiempo_ejecucion": null,
                "unidad_medida": "UN_METRO_CUB",
                "_fecha_creacion": "2018-07-10T19:02:31.977Z",
                "_fecha_modificacion": "2018-07-10T19:02:31.977Z",
                "_usuario_creacion": 37,
                "_usuario_modificacion": null
              },
              {
                "analisis_precios": null,
                "avance_total": null,
                "cantidad": 74.02,
                "especificaciones": null,
                "estado": "ACTIVO",
                "fid_modulo": 6,
                "id_item": 9,
                "nombre": "PROV. Y TEND. TUBERÍA HDPE 50mm D=1,5"",
                "nro_item": 5,
                "precio_unitario": 1220,
                "subtotal": 90304.4,
                "tiempo_ejecucion": null,
                "unidad_medida": "UN_METRO_CUB",
                "_fecha_creacion": "2018-07-10T19:02:31.977Z",
                "_fecha_modificacion": "2018-07-10T19:02:31.977Z",
                "_usuario_creacion": 37,
                "_usuario_modificacion": null
              }
            ],
            "nombre": "COMPONENTE 3: RIEGO TECNIFICADO POR GOTEO EN PARCELA|RESULTADO 3.2: RED APROXIMACION A LA PARCELA DESDE EL HIDRANTE",
            "show": true,
            "subtotal": 411882.4,
            "_fecha_creacion": "2018-07-10T19:02:31.977Z",
            "_fecha_modificacion": "2018-07-10T19:02:31.977Z",
            "_usuario_creacion": 37,
            "_usuario_modificacion": null
          }
        ]
      }
    }
   */
  app.api.get('/modulos/proyectos/:id', validate(paramValidation.getId), app.controller.modulo.getProyecto);
  app.api.get('/modulos/proyectos/:id/:id_adjudicacion', app.controller.modulo.getProyectoAdjudicacion);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Modulos
   * @apiName Get modulo
   * @api {get} /api/v1/modulos/:id Obtiene el modulo
   *
   * @apiParam id {string} Identificador del modulo
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Modulo recuperado correctamente.",
      "datos": {
        "id_modulo": 1,
        "fid_proyecto": 2,
        "nombre": "M03 - OBRAS ARQUITECTÓNICAS",
        "estado": "ACTIVO",
        "_usuario_creacion": 1,
        "_usuario_modificacion": null,
        "_fecha_creacion": "2017-09-06T22:09:41.564Z",
        "_fecha_modificacion": "2017-09-06T22:09:41.564Z"
      }
    }
   */
  app.api.get('/modulos/:id', validate(paramValidation.getId), app.controller.modulo.getId);
  app.api.options('/modulos', sequelizeFormly.formly(app.src.db.models.modulo, app.src.db.models));

  /**
   * @apiVersion 1.0.0
   * @apiGroup Modulos
   * @apiName Post modulos
   * @api {post} /api/v1/modulos Crear modulo
   *
   * @apiDescription Post para modulo
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "fid_adjudicacion": 5,
      "fid_proyecto": 2,
      "nombre": "M01 - OBRAS COMPLEMENTARIAS|RESULTADO"
    }
   */
  app.api.post('/modulos', validate(paramValidation.create), app.controller.modulo.post);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Modulos
   * @apiName Put modulos
   * @api {put} /api/v1/modulos/:id Modificar modulo
   *
   * @apiDescription Put para modulo
   *
   * @apiParam (id) {numeric} Identificador del modulo a modificar
   *
   * @apiParamExample {json} Ejemplo para enviar:
   * HTTP/1.1 200 OK
   *{
      "fid_proyecto": 2,
      "nombre": "M01 - OBRAS COMPLEMENTARIAS",
      "items": [
        {
          "id_item": 1,
          "nombre": "INSTALACION DE FAENAS",
          "unidad_medida": "UN_GLOBAL",
          "cantidad": 1,
          "precio_unitario": 67102.03,
          "tiempo_ejecucion": 24
        }, {
          "nombre": "REPLANTEO Y TRAZADO",
          "unidad_medida": "UN_METRO_CUAD",
          "cantidad": 3125,
          "precio_unitario": 3.72,
          "tiempo_ejecucion": 40
        }
      ]
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No se encontró el modulo.",
      "datos": null
    }
   */
  app.api.put('/modulos/:id', validate(paramValidation.update), app.controller.modulo.put);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Modulos
   * @apiName Delete modulos
   * @api {delete} /api/v1/modulos/:id Borrar modulo
   *
   * @apiDescription Delete para modulo
   *
   * @apiParam (Parámetro) {numeric} id Identificador del modulo a eliminar
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Modulo eliminado correctamente.",
      "datos": {
        "id_modulo": 1,
        "fid_proyecto": 2,
        "nombre": "M01 - OBRAS COMPLEMENTARIAS",
        "estado": "ELIMINADO",
        "_usuario_creacion": 4,
        "_usuario_modificacion": 4,
        "_fecha_creacion": "2017-10-16T18:32:58.848Z",
        "_fecha_modificacion": "2017-10-16T19:18:41.230Z"
      }
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No se encontró el modulo.",
      "datos": null
    }
   */
  app.api.delete('/modulos/:id', validate(paramValidation.getId), app.controller.modulo.eliminar);
};
