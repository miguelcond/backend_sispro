const validate = require('express-validation');
const paramValidation = require('./supervision.validation');
const sequelizeFormly = require('sequelize-formly');

module.exports = (app) => {
  /**
    @apiVersion 1.0.0
    @apiGroup supervision supervisiones
    @apiName Get supervisiones
    @api {get} /supervisiones Obtiene las supervisiones

    @apiDescription Get supervisiones, Obtiene las supervisiones y opcionalmente se puede indicar el estado

    @apiParam (Parametros) {Numero} fid_proyecto Es el id del proyecto del cual se quiere obtener las supervisiones
    @apiParam (Parametros) {Texto} origen Indica desde donde se pide la consulta, hara variar la respuesta en el campo "computo_metrico". Los valores son [bandeja, aplicacion]

    @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
    @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
    @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre las supervisiones

    @apiSuccessExample {json} Respuesta:
    HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Supervisiones recuperadas correctamente.",
      "datos": {
        "count": 2,
        "rows": [
          {
            "id_supervision": 2,
            "fid_proyecto": 2,
            "nro_supervision": 1,
            "fecha_inicio": null,
            "fecha_fin": null,
            "cantidad_avance": 40,
            "costo_avance": null,
            "estado_supervision": "INICIADO",
            "uuid": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "fecha_creacion": null,
            "_usuario_modificacion": null,
            "fecha_modificacion": null,
            "_fecha_creacion": "2017-09-26T21:10:20.327Z",
            "_fecha_modificacion": "2017-09-26T21:10:20.374Z",
            "computo_metrico": [
              {
                "id_computo_metrico": 4,
                "fid_item": 1,
                "fid_supervision": 2,
                "descripcion": "ALA NOR ESTE",
                "cantidad": null,
                "largo": 10,
                "ancho": 2,
                "alto": null,
                "factor_forma": 1,
                "area": 20,
                "volumen": null,
                "cantidad_parcial": 20,
                "uuid": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1,
                "fecha_creacion": null,
                "_usuario_modificacion": null,
                "fecha_modificacion": null,
                "_fecha_creacion": "2017-09-26T21:10:20.362Z",
                "_fecha_modificacion": "2017-09-26T21:10:20.362Z"
              },
              {
                "id_computo_metrico": 3,
                "fid_item": 1,
                "fid_supervision": 2,
                "descripcion": "ALA NOR OESTE",
                "cantidad": null,
                "largo": 10,
                "ancho": 2,
                "alto": null,
                "factor_forma": 1,
                "area": 20,
                "volumen": null,
                "cantidad_parcial": 20,
                "uuid": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1,
                "fecha_creacion": null,
                "_usuario_modificacion": null,
                "fecha_modificacion": null,
                "_fecha_creacion": "2017-09-26T21:10:20.350Z",
                "_fecha_modificacion": "2017-09-26T21:10:20.350Z"
              }
            ]
          }
        ]
      }
    }
  */
  app.api.get('/supervisiones', validate(paramValidation.get), app.controller.supervision.get);  
  app.api.options('/supervisiones', sequelizeFormly.formly(app.src.db.models.supervision, app.src.db.models));

  /**
   * @apiVersion 1.0.0
   * @apiGroup supervision supervisiones
   * @apiName Get supervisión específica por id
   * @api {get} /api/v1/supervisiones/:id Obtiene la supervisión por id
   *
   * @apiParam id {string} Identificador de la supervision
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   {
      "finalizado": true,
      "mensaje": "Supervisión recuperada correctamente.",
      "datos": {
        "id_supervision": 1,
        "fid_proyecto": 2,
        "nro_supervision": 1,
        "fecha_inicio": null,
        "fecha_fin": null,
        "estado_supervision": "INICIADO",
        "uuid": null,
        "estado": "ACTIVO",
        "_usuario_creacion": 494937017,
        "fecha_creacion": null,
        "_usuario_modificacion": null,
        "fecha_modificacion": null,
        "_fecha_creacion": "2017-11-13T14:24:29.998Z",
        "_fecha_modificacion": "2017-11-13T14:24:30.008Z",
        "monto_desembolso_actual": 19602586,
        "monto_desembolso_acumulado": 0,
        "monto_desembolso_total": 19602586,
        "modulos": [
          {
            "id_modulo": 1,
            "fid_proyecto": 2,
            "nombre": "M01 - OBRAS COMPLEMENTARIAS",
            "estado": "ACTIVO",
            "_usuario_creacion": "4",
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-11-13T14:21:11.995Z",
            "_fecha_modificacion": "2017-11-13T14:21:11.995Z",
            "items": [
              {
                "id_item": 1,
                "fid_modulo": 1,
                "nro_item": 1,
                "nombre": "INSTALACION DE FAENAS",
                "unidad_medida": "UN_GLOBAL",
                "cantidad": 1,
                "precio_unitario": 10000,
                "tiempo_ejecucion": null,
                "avance_total": null,
                "especificaciones": null,
                "estado": "ACTIVO",
                "_usuario_creacion": "4",
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-11-13T14:21:11.995Z",
                "_fecha_modificacion": "2017-11-13T14:21:11.995Z",
                "cantidad_anterior_fis": 0,
                "total": 10000,
                "cantidad_actual_fis": 0,
                "total_acumulado_fis": 0,
                "falta_ejecutar_fis": 1,
                "cantidad_anterior_fin": 0,
                "cantidad_actual_fin": 0,
                "total_acumulado_fin": 0,
                "falta_ejecutar_fin": 10000,
                "ejecutado_porcentaje_fis": 0,
                "ejecutar_porcentaje_fis": 100,
                "ejecutado_porcentaje_fin": 0,
                "ejecutar_porcentaje_fin": 100
              },
              {
                "id_item": 2,
                "fid_modulo": 1,
                "nro_item": 2,
                "nombre": "LETRERO EN OBRA (LONA PVC)",
                "unidad_medida": "UN_PIEZA",
                "cantidad": 1,
                "precio_unitario": 2500,
                "tiempo_ejecucion": null,
                "avance_total": null,
                "especificaciones": null,
                "estado": "ACTIVO",
                "_usuario_creacion": "4",
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-11-13T14:21:11.995Z",
                "_fecha_modificacion": "2017-11-13T14:21:11.995Z",
                "cantidad_anterior_fis": 0,
                "total": 2500,
                "cantidad_actual_fis": 0,
                "total_acumulado_fis": 0,
                "falta_ejecutar_fis": 1,
                "cantidad_anterior_fin": 0,
                "cantidad_actual_fin": 0,
                "total_acumulado_fin": 0,
                "falta_ejecutar_fin": 2500,
                "ejecutado_porcentaje_fis": 0,
                "ejecutar_porcentaje_fis": 100,
                "ejecutado_porcentaje_fin": 0,
                "ejecutar_porcentaje_fin": 100
              },
            ]
          },
          {
            "id_modulo": 2,
            "fid_proyecto": 2,
            "nombre": "M02 - OBRAS ESTRUCTURALES",
            "estado": "ACTIVO",
            "_usuario_creacion": "4",
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-11-13T14:21:11.995Z",
            "_fecha_modificacion": "2017-11-13T14:21:11.995Z",
            "items": [
              ...
            ]
          },
          {
            "id_modulo": 3,
            "fid_proyecto": 2,
            "nombre": "M03 - OBRAS ARQUITECTONICAS",
            "estado": "ACTIVO",
            "_usuario_creacion": "4",
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-11-13T14:21:11.995Z",
            "_fecha_modificacion": "2017-11-13T14:21:11.995Z",
            "items": [
              ...
            ]
          }
        ]
      }
    }
   */
  app.api.get('/supervisiones/:id', validate(paramValidation.getId), app.controller.supervision.getId);

  /**
   * @apiVersion 1.0.0
   * @apiGroup supervision supervisiones
   * @apiName Post supervisiones
   * @api {post} /api/v1/supervisiones Crear supervision
   *
   * @apiDescription Post para supervision
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "fid_proyecto": 2,
    * GLOBAL
      "computo_metrico": [
        {
          "fid_item": 1,
          "descripcion": "ALA NOR OESTE",
          "cantidad": 1
        }
      ]
    * PIEZA
      "computo_metrico": [
        {
          "fid_item": 2,
          "descripcion": "piso 1",
          "cantidad": 1
        },
        {
          "fid_item": 2,
          "descripcion": "piso 2",
          "cantidad": 4
        }
      ]
    * ML
      "computo_metrico": [
        {
          "fid_item": 10,
          "descripcion": "SALA NOR OESTE",
          "largo": 10,
          "cantidad": 12
        },
        {
          "fid_item": 10,
          "descripcion": "SALA SUR",
          "largo": 1,
          "cantidad": 5
        }
      ]
    * M2
      "computo_metrico": [
        {
          "fid_item": 3,
          "descripcion": "curva norte",
          "largo": 7,
          "alto": 4,
          "cantidad": 3,
          "factor_forma": 1
        },
        {
          "fid_item": 3,
          "descripcion": "curva sur",
          "largo": 1,
          "alto": 1,
          "cantidad": 12,
          "factor_forma": 16.99
        }
      ]
    * M3
      "computo_metrico": [
        {
          "fid_item": 4,
          "descripcion": "piso 13",
          "largo": 1,
          "ancho": 1,
          "alto": 1,
          "cantidad": 2,
          "factor_forma": 20
        },
        {
          "fid_item": 4,
          "descripcion": "piso 15",
          "largo": 12,
          "ancho": 16,
          "alto": 21,
          "cantidad": 7,
          "factor_forma": 1
        }
      ]
    }
   */
  app.api.post('/supervisiones', validate(paramValidation.create), app.controller.supervision.post);
  app.api.post('/supervisiones/adjudicacion/:id_adjudicacion', validate(paramValidation.create), app.controller.supervision.post);

  /**
   * @apiVersion 1.0.0
   * @apiGroup supervision supervisiones
   * @apiName Put superviciones
   * @api {put} /api/v1/supervisiones/:id Modificar supervision
   *
   * @apiDescription Put para supervision
   *
   * @apiParam (id) {numeric} Identificador de la supervision a modificar
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "fid_proyecto": 2,
      "id_supervision": 1,
      "computo_metrico": [
        {
          "id_computo_metrico": 93,
          "descripcion": "actualizando desde supervisiones",
          "cantidad": 1,
          "largo": 1,
          "ancho": 1,
          "alto": 1,
          "factor_forma": -9.8
        }
      ]
    }
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Avance actualizado correctamente.",
      "datos": {
        "id_supervision": 1,
        "fid_proyecto": 2,
        "nro_supervision": 1,
        "fecha_inicio": null,
        "fecha_fin": null,
        "estado_supervision": "INICIADO",
        "uuid": null,
        "estado": "ACTIVO",
        "_usuario_creacion": 1022579028,
        "fecha_creacion": null,
        "_usuario_modificacion": null,
        "fecha_modificacion": null,
        "_fecha_creacion": "2017-11-09T22:47:27.576Z",
        "_fecha_modificacion": "2017-11-10T13:10:01.685Z",
        "computo_metrico": [
            {
                "id_computo_metrico": 93,
                "fid_item": 4,
                "fid_supervision": 1,
                "descripcion": "actualizando desde supervisiones",
                "cantidad": 1,
                "largo": 1,
                "ancho": 1,
                "alto": 1,
                "factor_forma": -9.8,
                "area": null,
                "volumen": -9.8,
                "cantidad_parcial": -9.8,
                "subtitulo": null,
                "uuid": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1022579028,
                "fecha_creacion": null,
                "_usuario_modificacion": 1022579028,
                "fecha_modificacion": null,
                "_fecha_creacion": "2017-11-09T22:47:27.587Z",
                "_fecha_modificacion": "2017-11-10T13:11:14.994Z"
              }
          ]
      }
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No se encontró la supervision.",
      "datos": null
    }
   */
  app.api.put('/supervisiones/:id', validate(paramValidation.update), app.controller.supervision.put);

  app.api.put('/supervisiones/anticipos/:id', validate(paramValidation.update), app.controller.supervision.actualizarAnticipo)
};
