const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getId: {
    params: {
      id: Joi.number().required()
    }
  },
  get: {
    params: { }
  },
  create: {
    body: {
      fid_proyecto: Joi.number().required(),
      nombre: Joi.string().required()
    }
  },
  update: {
    params: {
      id: Joi.number().required()
    },
    body: {
      fid_proyecto: Joi.number().required(),
      nombre: Joi.string().required(),
      items: Joi.array().items({
        id_item: Joi.number().optional(),
        nombre: Joi.string().optional(),
        unidad_medida: Joi.string().optional(),
        cantidad: Joi.number().optional(),
        precio_unitario: Joi.number().allow(null).allow('').optional(),
        tiempo_ejecucion: Joi.number().allow(null).allow('').optional()
      })
    }
  }
};
