const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getAdmin: {
    query: {
      limit: Joi.number(),
      page: Joi.number(),
    }
  },
  getAdminUsuario: {
    params: {
      codigo: Joi.number().required(),
    }
  },
  putAdminUsuario: {
    params: {
      codigo: Joi.number().required(),
    },
    body: {
      nro_usuario: Joi.number().required(),
      nro_rol: Joi.number().required(),
      nuevo_nro_usuario: Joi.number().required(),
      nuevo_nro_rol: Joi.number().required(),
    },
  },
  getId: {
    params: {
      id: Joi.number().required()
    },
    query: {
      rol: Joi.number(),
      roles: Joi.array().items(Joi.number())
    }
  },
  get: {
    params: { }
  },
  getCodigo: {
    params: {
      codigo: Joi.string().required()
    }
  },
  create: {
    body: {
      tipo: Joi.string().required(),
      codigo: Joi.string().allow('').optional(),
      nombre: Joi.string().required(),
      nro_convenio: Joi.string().required(),
      monto_fdi: Joi.number().required(),
      monto_contraparte: Joi.number().required(),
      plazo_ejecucion_convenio: Joi.number().required(),
      entidad_beneficiaria: Joi.string().allow('').optional(),
      fecha_suscripcion_convenio: Joi.date().format('YYYY-MM-DD').required(),
      doc_convenio: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      doc_convenio_extendido: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      doc_resmin_cife: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      doc_ev_externa: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      fcod_municipio: Joi.string().required()
    }
  },
  update: {
    query: {
      rol: Joi.number()
    },
    params: {
      id: Joi.number().required()
    },
    body: {
      tipo: Joi.string().allow('').optional(),
      codigo: Joi.string().allow('').optional(),
      nombre: Joi.string().allow('').optional(),
      nro_convenio: Joi.string().allow('').optional(),
      monto_total: Joi.number().allow('').optional(),
      plazo_ejecucion: Joi.number().allow('').optional(),
      entidad_beneficiaria: Joi.string().allow('').optional(),
      fecha_suscripcion_convenio: Joi.date().format('YYYY-MM-DD').allow('').optional(),
      doc_convenio: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      doc_convenio_extendido: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      doc_resmin_cife: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      doc_ev_externa: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      fcod_municipio: Joi.string().allow('').optional(),
      desembolso: Joi.number().allow('').optional(),
      porcentaje_desembolso: Joi.number().allow('').optional(),
      fecha_desembolso: Joi.date().format('YYYY-MM-DD').allow('').optional(),
      doc_cert_presupuestaria: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      anticipo: Joi.number().allow('').optional(),
      porcentaje_anticipo: Joi.number().allow('').optional(),
      sector: Joi.string().allow('').optional(),
      autoridad_beneficiaria: {
        tipo_documento: Joi.string().optional(),
        numero_documento: Joi.string().optional(),
        fecha_nacimiento: Joi.date().format('YYYY-MM-DD').optional(),
        nombres: Joi.string().allow('').optional(),
        primer_apellido: Joi.string().allow('').optional(),
        segundo_apellido: Joi.string().allow('').optional(),
        telefono: Joi.string().optional(),
        correo: Joi.string().allow(null).allow('').optional()
      },
      cargo_autoridad_beneficiaria: Joi.string().allow('').optional(),
      fax_autoridad_beneficiaria: Joi.string().max(60).allow('').optional(),
      doc_base_contratacion: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      nro_resmin_cife: Joi.string().allow(null).allow('').optional(),
      orden_proceder: Joi.date().format('YYYY-MM-DD').allow(null).allow('').optional(),
      coordenadas: Joi.object({
        type: Joi.string().required(),
        coordinates: Joi.array().required()
      }),
      contrato: {
        nro_invitacion: Joi.string().required(),
        nro_cuce: Joi.string().required(),
        nombre_representante_legal: Joi.string().required(),
        empresa: {
          matricula: Joi.string().required(),
          nit: Joi.string().required(),
          nombre: Joi.string().required(),
          tipo: Joi.string().allow('').optional(),
          pais: Joi.string().allow('').optional(),
          ciudad: Joi.string().allow('').optional(),
          direccion: Joi.string().allow().optional(),
          telefono: Joi.number().allow('').optional()
        }
      },
      equipo_tecnico: Joi.array().items({
        cargo: Joi.string().required(),
        persona: {
          tipo_documento: Joi.string().required(),
          numero_documento: Joi.string().required(),
          fecha_nacimiento: Joi.date().format('YYYY-MM-DD').required(),
          nombres: Joi.string().allow('').optional(),
          primer_apellido: Joi.string().allow('').optional(),
          segundo_apellido: Joi.string().allow('').optional(),
          telefono: Joi.string().required(),
          correo: Joi.string().allow('').optional()
        }
      }),
      fecha_modificacion: Joi.date().format('YYYY-MM-DD').allow('').optional()
    }
  },
  empresasParalizar: {
    query: {
      rol: Joi.number().min(1),
    },
    params: {
      id: Joi.number().required(),
    },
    body: {
      estado: Joi.string(),
      rol: Joi.number().min(1),
    }
  },
  empresasAvance: {
    query: {
      rol: Joi.number().min(1),
    },
    params: {
      id: Joi.number().required(),
    },
    body: {
      estado: Joi.string(),
      rol: Joi.number().min(1),
    }
  },

  // FDI
  regCreate: {
    body: {
      codigo: Joi.string().allow('').optional(),
      nombre: Joi.string().required(),
      nro_convenio: Joi.string().required(),
      entidad_beneficiaria: Joi.string().allow('').optional(),
      fecha_suscripcion_convenio: Joi.date().format('YYYY-MM-DD').required(),
      fcod_municipio: Joi.string().required()
    }
  },
};
