import BaseJoi from '../../../lib/joi';
import Extension from 'joi-date-extensions';
const Joi = BaseJoi.extend(Extension);

module.exports = {
  crear: {
    params: {
      id_proyecto: Joi.number().required()
    },
    body: {
      tipo: Joi.string().max(50).required(),
      numero: Joi.string().max(150).required(),
      monto: Joi.number().required(),
      fecha_inicio_validez: Joi.date().format('YYYY-MM-DD').required(),
      fecha_fin_validez: Joi.date().format('YYYY-MM-DD').required(),
      doc_boleta: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      })
    }
  },
  adicionar: {
    params: {
      id_proyecto: Joi.number().required()
    },
    body: {
      boletas: Joi.array().items({
        tipo_boleta: Joi.string().max(50).required(),
        tipo_documento: Joi.string().max(15).required(),
        numero: Joi.string().max(150).required(),
        monto: Joi.number().required(),
        fecha_inicio_validez: Joi.date().format('YYYY-MM-DD').required(),
        fecha_fin_validez: Joi.date().format('YYYY-MM-DD').required()
      })
    }
  },
  modificar: {
    params: {
      id_proyecto: Joi.number().required()
    },
    body: {
      estado: Joi.string().max(10)
    }
  }
};
