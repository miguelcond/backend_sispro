import sequelizeFormly from 'sequelize-formly';

module.exports = (app) => {
  /**
   * @apiVersion 1.0.0
   * @apiGroup Empresas
   * @apiName Get empresa
   * @api {get} /empresas/:matricula Obtiene la empresa
   *
   * @apiDescription Get empresas, Obtiene la empresa
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Empresa recuperada correctamente.",
      "datos": [
        {
          "CtrResult": "D-EXIST",
          "IdMatricula": "00101952",
          "RazonSocial": "MR. CAFE",
          "FechaInscripcion": "2002-11-20",
          "FechaUltRenovacion": "2013-07-09",
          "UltGestionRenovada": "2012",
          "Nit": "03275482018",
          "CtrEstado": "0",
          "MatriculaDatosSucList1": {
            "MatriculaDatosSuc": [
              {
                "IdSuc": 1,
                "Sucursal": "SUCURSAL AEROPUERTO VIRU VIRU",
                "Municipio": "SANTA CRUZ",
                "Direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
                "Zona": "NORTE",
                "Telefono": "72159590",
                "Representante": "REED RHONDA MARIE"
              }
            ]
          }
        }
      ]
    }
   */
  app.api.get('/empresas/:matricula', app.controller.empresa.get);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Empresas
   * @apiName Get empresas
   * @api {get} /empresas/:nit/matriculas Obtiene las matrículas del nit
   *
   * @apiDescription Get modulos, Obtiene las matrículas del nit
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Matriculas recuperadas correctamente.",
      "datos": [
        {
          "CtrResult": "D-EXIST",
          "IdMatricula": "00101952",
          "Nit": "03275482018",
          "RazonSocial": "MR. CAFE",
          "CtrEstado": "0"
        },
        {
          "CtrResult": "D-EXIST",
          "IdMatricula": "00161835",
          "Nit": "03275482018",
          "RazonSocial": "CHIARA COFFE EVENTOS Y CATERING",
          "CtrEstado": "0"
        }
      ]
    }
   */
  app.api.get('/empresas/:nit/matriculas', app.controller.empresa.getMatriculas);
  app.api.options('/empresas', sequelizeFormly.formly(app.src.db.models.empresa, app.src.db.models));
};
