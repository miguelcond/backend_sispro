import validate from 'express-validation';
import paramValidation from './equipo_tecnico.validation';

module.exports = (app) => {
/**
 * @apiDefine headerAuthorization
 *
 * @apiHeader {String} token Bearer <code>token de autorizacion</code>
 */

  /**
  * @api {get} /proyectos/:id_proyecto/equipos_tecnicos Listar Equipo Tecnico
  * @apiName Listar Equipo Tecnico
  * @apiGroup Equipo Tecnico
  * @apiVersion 0.1.0
  * @apiDescription Devuelve una lista de registros <code>equipo_tecnico</code> de un determinado proyecto.
  * @apiParam (Params) {Number} [id_proyecto] Identificador único del registro <code>proyecto</code>
  * @apiSuccessExample Respuesta exitosa:
  * HTTP/1.1 200 OK
  * {
      "finalizado": true,
      "mensaje": "Se obtuvo la lista exitosamente.",
      "datos": {
        "count": 2,
        "rows": [
          {
            "cargo": "CG_ENCARGADO",
            "cargo_usuario": {
              "codigo": "CG_ENCARGADO",
              "nombre": "Jefe de la Unidad de Revisión",
              "tipo": "CARGO"
            },
            "doc_designacion": null,
            "equipo": {
              "codigo": "TE_INSTITUCION",
              "nombre": "INSTITUCION",
              "tipo": "TIPO_EQUIPO"
            },
            "estado": "ACTIVO",
            "fid_proyecto": 1,
            "fid_usuario": 31,
            "id_equipo_tecnico": 1,
            "tipo_equipo": "TE_INSTITUCION",
            "usuario": {
              "fid_persona": 31,
              "id_usuario": 31,
              "persona": {
                "complemento": "00",
                "complemento_visible": false,
                "correo": "email.31@example.com",
                "direccion": null,
                "estado": "ACTIVO",
                "fecha_nacimiento": "1990-03-02T04:00:00.000Z",
                "id_persona": 31,
                "lugar_nacimiento_pais": null,
                "nombres": "JEFE REVISION",
                "numero_documento": "000031",
                "primer_apellido": null,
                "segundo_apellido": null,
                "telefono": null,
                "tipo_documento": "TD_CI"
              },
              "usuario": "revisionfdi"
            },
            "_fecha_creacion": "2018-07-11T13:01:48.476Z",
            "_fecha_modificacion": "2018-07-11T13:01:48.476Z"
          },
          {
            "cargo": "CG_TECNICO",
            "cargo_usuario": {
              "codigo": "CG_TECNICO",
              "nombre": "Técnico Responsable",
              "tipo": "CARGO"
            },
            "doc_designacion": null,
            "equipo": {
              "codigo": "TE_INSTITUCION",
              "nombre": "INSTITUCION",
              "tipo": "TIPO_EQUIPO"
            },
            "estado": "ACTIVO",
            "fid_proyecto": 1,
            "fid_usuario": 32,
            "id_equipo_tecnico": 2,
            "tipo_equipo": "TE_INSTITUCION",
            "usuario": {
              "fid_persona": 32,
              "id_usuario": 32,
              "persona": {
                "complemento": "00",
                "complemento_visible": false,
                "correo": "email.32@example.com",
                "direccion": null,
                "estado": "ACTIVO",
                "fecha_nacimiento": "1990-03-03T04:00:00.000Z",
                "id_persona": 32,
                "lugar_nacimiento_pais": null,
                "nombres": "TECNICO FDI",
                "numero_documento": "000032",
                "primer_apellido": null,
                "segundo_apellido": null,
                "telefono": null,
                "tipo_documento": "TD_CI"
              },
              "usuario": "tecnicofdi"
            },
            "_fecha_creacion": "2018-07-11T13:01:48.476Z",
            "_fecha_modificacion": "2018-07-11T13:01:48.476Z"
          }
        ]
      }
    }
  */
  app.api.get('/proyectos/:id_proyecto/equipos_tecnicos', validate(paramValidation.listar), app.controller.equipo_tecnico.listar);
  app.api.get('/proyectos/:id_proyecto/equipos_tecnicos/adjudicaciones/:id_adjudicacion', validate(paramValidation.listar), app.controller.equipo_tecnico.listar);

  /**
  * @api {get} /api/v1/proyectos/:id_proyecto/equipos_tecnicos/:id Obtener datos de miembro de Equipo Tecnico
  * @apiName Obtener Equipo Tecnico
  * @apiGroup Equipo Tecnico
  * @apiVersion 0.1.0
  * @apiDescription Devuelve un registro de persona de <code>equipo_tecnico</code>.
  * @apiUse headerAuthorization
  *
  * @apiParam (Params) {Number} [id_proyecto] Identificador único del registro <code>proyecto</code>
  * @apiParam (Params) {Number} [id] Identificador único del registro <code>equipo_tecnico</code>
  *
  * @apiSuccessExample Respuesta exitosa:
  * HTTP/1.1 200 OK
  * {
      "finalizado": true,
      "mensaje": "Se obtuvo los datos exitosamente.",
      "datos": {
        "cargo": "CG_ENCARGADO",
        "cargo_usuario": {
          "codigo": "CG_ENCARGADO",
          "nombre": "Jefe de la Unidad de Revisión",
          "tipo": "CARGO"
        },
        "doc_designacion": null,
        "equipo": {
          "codigo": "TE_INSTITUCION",
          "nombre": "INSTITUCION",
          "tipo": "TIPO_EQUIPO"
        },
        "estado": "ACTIVO",
        "fid_proyecto": 1,
        "fid_usuario": 31,
        "id_equipo_tecnico": 1,
        "tipo_equipo": "TE_INSTITUCION",
        "usuario": {
          "fid_persona": 31,
          "id_usuario": 31,
          "persona": {
            "complemento": "00",
            "complemento_visible": false,
            "correo": "email.31@example.com",
            "direccion": null,
            "estado": "ACTIVO",
            "fecha_nacimiento": "1990-03-02T04:00:00.000Z"
            "id_persona": 31,
            "lugar_nacimiento_pais": null,
            "nombres": "JEFE REVISION",
            "numero_documento": "000031",
            "primer_apellido": null,
            "segundo_apellido": null,
            "telefono": null,
            "tipo_documento": "TD_CI"
          },
          "usuario": "revisionfdi"
        },
        "_fecha_creacion": "2018-07-11T13:01:48.476Z",
        "_fecha_modificacion": "2018-07-11T13:01:48.476Z"
      }
    }
  */
  app.api.get('/proyectos/:id_proyecto/equipos_tecnicos/supervisor', app.controller.equipo_tecnico.obtenerPorSupervisor);
  app.api.get('/proyectos/:id_proyecto/equipos_tecnicos/:id', validate(paramValidation.obtener), app.controller.equipo_tecnico.obtener);

  /**
  * @api {post} /proyectos/:id_proyecto/equipos_tecnicos Crear Equipo Tecnico
  * @apiName Crear Equipo Tecnico
  * @apiGroup Equipo Tecnico
  * @apiVersion 0.1.0
  * @apiDescription Crea un nuevo registro <code>equipo_tecnico</code>
  * Si es una persona BOLIVIANA, verifica los datos con SEGIP. Si es una persona EXTRANJERA, no verifica los datos.
  *
  * @apiParam (Params) {Number} id_proyecto ID Identificador único del registro <code>proyecto</code>
  * @apiParam (Body) {String} cargo Cargo de la persona: CG_SUPERVISOR, CG_FISCAL, CG_TECNICO, CG_ENCARGADO.
  * @apiParam (Body) {String} [correo] correo.
  * @apiParam (Body) {Object} doc_designacion Documento de designación.
  * @apiParam (Body) {String} doc_designacion.base64 Archivo en formato base64.
  * @apiParam (Body) {String} doc_designacion.nombre Nombre del archivo.
  * @apiParam (Body) {String} doc_designacion.path Nombre del archivo.
  * @apiParam (Body) {String} doc_designacion.size Tamaño del archivo.
  * @apiParam (Body) {String} doc_designacion.tipo Tipo de archivo.
  * @apiParam (Body) {String} [estado] Estado del registro.
  * @apiParam (Body) {String} fecha_nacimiento Fecha de nacimiento.
  * @apiParam (Body) {Number} id_persona Identificador de registro <code>persona</code>
  * @apiParam (Body) {String} [nombres] Nombres (Sólo en caso de extranjero).
  * @apiParam (Body) {String} numero_documento CI de la persona.
  * @apiParam (Body) {String} [primer_apellido] Primer apellido (Sólo en caso de extranjero).
  * @apiParam (Body) {String} [segundo_apellido] Segundo apellido (Sólo en caso de extranjero
  * @apiParam (Body) {String} [telefono] telefono.
  * @apiParam (Body) {String} tipo_documento Tipo de documento de identidad (TD_CI o TD_EXT).
  * @apiParam (Body) {String} tipo_equipo Tipo de equipo tecnico
  * @apiParamExample {json} Ejemplo petición:
  {
    "cargo": "CG_SUPERVISOR",
    "correo": "testing@mail.tst",
    "doc_designacion": {
      "base64": "JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3...",
      "nombre": "001_Documento_Designacion_Supervidor.pdf",
      "path": "001_Documento_Designacion_Supervidor.pdf",
      "size": 16150,
      "tipo": "application/pdf"
    },
    "estado": "ACTIVO",
    "fecha_nacimiento": "13-06-1987",
    "id_persona": 39,
    "nombres": "GLORIA",
    "numero_documento": "10815017",
    "primer_apellido": "MALE",
    "segundo_apellido": "SOSA",
    "telefono": "77777777",
    "tipo_documento": "TD_CI",
    "tipo_equipo": "TE_BENEFICIARIO"
  }
  * @apiSuccessExample Respuesta exitosa:
  HTTP/1.1 200 OK
  {
    "finalizado": true,
    "mensaje": "Se registró los datos exitosamente.",
    "datos": {
      "cargo": "CG_SUPERVISOR",
      "cargo_usuario": {
        "codigo": "CG_SUPERVISOR",
        "nombre": "Supervisor",
        "tipo": "CARGO"
      },
      "doc_designacion": "[DET_rJWp0Y7XQ] Designación equipo técnico.pdf",
      "equipo": {
        "codigo": "TE_BENEFICIARIO",
        "nombre": "BENEFICIARIO",
        "tipo": "TIPO_EQUIPO"
      },
      "estado": "ACTIVO",
      "fid_proyecto": 1,
      "fid_usuario": 37,
      "id_equipo_tecnico": 3,
      "tipo_equipo": "TE_BENEFICIARIO",
      "usuario": {
        "fid_persona": 39,
        "id_usuario": 37,
        "persona": {
          "complemento": "1I",
          "complemento_visible": false,
          "correo": "testing@mail.tst",
          "direccion": null,
          "estado": "ACTIVO",
          "fecha_nacimiento": "1987-06-13T04:00:00.000Z",
          "id_persona": 39,
          "lugar_nacimiento_pais": "BOLIVIA",
          "nombres": "GLORIA",
          "numero_documento": "10815017",
          "primer_apellido": "MALE",
          "segundo_apellido": "SOSA",
          "telefono": "77777777",
          "tipo_documento": "TD_CI"
        },
        "usuario": "10815017"
      },
      "_fecha_creacion": "2018-07-11T14:05:45.635Z",
      "_fecha_modificacion": "2018-07-11T14:05:45.635Z"
    }
  }
  * @apiErrorExample Ejemplo.- Error 422:
  {
      "finalizado": false,
      "codigo": 422,
      "mensaje": "El 'correo' ingresado 'testing@mail.tst' corresponde a otro usuario registrado."
  }
  */
  app.api.post('/proyectos/:id_proyecto/equipos_tecnicos', validate(paramValidation.crear), app.controller.equipo_tecnico.crear);

  /**
  * @api {post} /proyectos/:id_proyecto/equipos_tecnicos/:id/notificar Notificar Equipo Tecnico
  * @apiName Notificar Equipo Tecnico
  * @apiGroup Equipo Tecnico
  * @apiVersion 0.1.0
  * @apiDescription Envía un correo electrónico al equipo tecnico, el cual contiene información acerca del proyecto que le fue asignado,
  * el cargo que ocupa en el mismo y el usuario con el que puede acceder al sistema.
  *
  * @apiParam (Params) {Number} id_proyecto ID Identificador único del registro <code>proyecto</code>
  * @apiParam (Params) {Number} id ID Identificador único del registro <code>equipo_tecnico</code>
  * @apiSuccessExample Respuesta exitosa:
  * HTTP/1.1 200 OK
  {
      "finalizado": true,
      "mensaje": "Se envió una notificación por correo exitosamente.",
      "datos": null
  }
  * @apiErrorExample Ejemplo.- Error 404:
  {
      "finalizado": false,
      "codigo": 404,
      "mensaje": "No existe el registro del equipo tecnico solicitado."
  }
  */
  app.api.post('/proyectos/:id_proyecto/equipos_tecnicos/:id/notificar', validate(paramValidation.notificar), app.controller.equipo_tecnico.notificar);

  /**
  * @api {put} /proyectos/:id_proyecto/equipos_tecnicos/:id Modificar Equipo Tecnico
  * @apiName Modificar Equipo Tecnico
  * @apiGroup Equipo Tecnico
  * @apiVersion 0.1.0
  * @apiDescription Modifica un registro <code>equipo_tecnico</code>.
  * @apiParam (Params) {Number} id_proyecto ID Identificador único del registro <code>proyecto</code>
  * @apiParam (Params) {Number} id ID Identificador único del registro <code>equipo_tecnico</code>
  * @apiParam (Body) {String} cargo Cargo de la persona: CG_SUPERVISOR, CG_FISCAL, CG_TECNICO, CG_ENCARGADO.
  * @apiParam (Body) {Object} cargo_usuario Cargo de usuario.
  * @apiParam (Body) {String} cargo_usuario.codigo Codigo.
  * @apiParam (Body) {String} cargo_usuario.nombre Nombre.
  * @apiParam (Body) {String} cargo_usuario.tipo Tipo.
  * @apiParam (Body) {String} [correo] correo.
  * @apiParam (Body) {String} direccion Direccion.
  * @apiParam (Body) {Object} doc_designacion Documento de designación.
  * @apiParam (Body) {String} doc_designacion.base64 Archivo en formato base64.
  * @apiParam (Body) {String} doc_designacion.nombre Nombre del archivo.
  * @apiParam (Body) {String} doc_designacion.path Nombre del archivo.
  * @apiParam (Body) {String} doc_designacion.size Tamaño del archivo.
  * @apiParam (Body) {String} doc_designacion.tipo Tipo de archivo.
  * @apiParam (Body) {Object} equipo Equipo.
  * @apiParam (Body) {String} equipo.codigo Codigo.
  * @apiParam (Body) {String} equipo.nombre Nombre.
  * @apiParam (Body) {String} equipo.tipo Tipo.
  * @apiParam (Body) {String} estado Estado del registro.
  * @apiParam (Body) {String} fecha_nacimiento Fecha de nacimiento.
  * @apiParam (Body) {Number} fid_proyecto Identificador de proyecto.
  * @apiParam (Body) {Number} fid_usuario Identificador de usuario.
  * @apiParam (Body) {Number} id_equipo_tecnico Identificador de equipo tecnico.
  * @apiParam (Body) {Number} id_persona Identificador de persona.
  * @apiParam (Body) {String} lugar_nacimiento_pais Lugar de nacimiento.
  * @apiParam (Body) {String} nombres Nombres.
  * @apiParam (Body) {String} numero_documento Número de documento de identidad.
  * @apiParam (Body) {String} primer_apellido Primer Apellido.
  * @apiParam (Body) {String} segundo_apellido Segundo Apellido.
  * @apiParam (Body) {String} [telefono] Telefono.
  * @apiParam (Body) {String} tipo_documento Tipo de documento de identificación.
  * @apiParam (Body) {String} tipo_equipo Tipo de equipo tecnico.
  * @apiParamExample {json} Ejemplo petición:
  {
    "cargo": "CG_FISCAL",
    "cargo_usuario": {
      "codigo": "CG_FISCAL",
      "nombre": "Fiscal",
      "tipo": "CARGO"
    },
    "complemento": "1K",
    "complemento_visible": false,
    "correo": "fegyh@vcvt.lpo",
    "direccion": null,
    "doc_designacion": {
      "base64": "JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoI...",
      "nombre": "001_Modificacion_OrdenCambio.pdf",
      "path": "001_Modificacion_OrdenCambio.pdf",
      "size": 18477,
      "tipo": "application/pdf"
    },
    "equipo": {
      "codigo": "TE_BENEFICIARIO",
      "nombre": "BENEFICIARIO",
      "tipo": "TIPO_EQUIPO"
    },
    "estado": "ACTIVO",
    "fecha_nacimiento": "25-08-1992",
    "fid_proyecto": 1,
    "fid_usuario": 38,
    "id_equipo_tecnico": 4,
    "id_persona": 41,
    "lugar_nacimiento_pais": "BOLIVIA",
    "nombres": "LILIA",
    "numero_documento": "1702230",
    "primer_apellido": "MARTINEZ",
    "segundo_apellido": "MUÑOZ",
    "telefono": "74854855",
    "tipo_documento": "TD_CI",
    "tipo_equipo": "TE_BENEFICIARIO",
    "_fecha_creacion": "2018-07-11T14:53:29.228Z",
    "_fecha_modificacion": "2018-07-11T14:53:29.228Z"
  }
  *
  * @apiSuccessExample Respuesta exitosa:
  HTTP/1.1 200 OK
  {
    "finalizado": true,
    "mensaje": "Se actualizaron los datos exitosamente.",
    "datos": {
      "cargo": "CG_FISCAL",
      "cargo_usuario": {
        "codigo": "CG_FISCAL",
        "nombre": "Fiscal",
        "tipo": "CARGO"
      },
      "doc_designacion": "[DET_Hy@eq9Q77] Designación equipo técnico.pdf",
      "equipo": {
        "codigo": "TE_BENEFICIARIO",
        "nombre": "BENEFICIARIO",
        "tipo": "TIPO_EQUIPO"
      },
      "estado": "ACTIVO",
      "fid_proyecto": 1,
      "fid_usuario": 38,
      "id_equipo_tecnico": 4,
      "tipo_equipo": "TE_BENEFICIARIO",
      "usuario": {
        "fid_persona": 41,
        "id_usuario": 38,
        "persona": {
          "complemento": "1K",
          "complemento_visible": false,
          "correo": "fegyh@vcvt.lpo",
          "direccion": null,
          "estado": "ACTIVO",
          "fecha_nacimiento": "1992-08-25T04:00:00.000Z",
          "id_persona": 41,
          "lugar_nacimiento_pais": "BOLIVIA",
          "nombres": "LILIA",
          "numero_documento": "1702230",
          "primer_apellido": "MARTINEZ",
          "segundo_apellido": "MUÑOZ",
          "telefono": "74854855",
          "tipo_documento": "TD_CI"
        },
        "usuario": "1702230"
      },
      "_fecha_creacion": "2018-07-11T14:53:29.228Z",
      "_fecha_modificacion": "2018-07-11T14:53:29.228Z"
    }
  }
  * @apiErrorExample Ejemplo.- Error 404:
  {
      "finalizado": false,
      "codigo": 404,
      "mensaje": "No existe el registro de la persona solicitada."
  }
  */
  app.api.put('/proyectos/:id_proyecto/equipos_tecnicos/:id', validate(paramValidation.modificar), app.controller.equipo_tecnico.modificar);
  app.api.put('/proyectos/:id_proyecto/equipos_tecnicos/:id/reemplazar', validate(paramValidation.reemplazar), app.controller.equipo_tecnico.reemplazar);

  /**
  * @api {delete} /proyectos/:id_proyecto/equipos_tecnicos/:id Eliminar Equipo Tecnico
  * @apiName Eliminar Equipo Técnico
  * @apiGroup Equipo Tecnico
  * @apiVersion 0.1.0
  * @apiDescription Elimina un registro <code>equipo_tecnico</code>.
  * @apiParam (Params) {Number} id_proyecto ID Identificador único del registro <code>proyecto</code>
  * @apiParam (Params) {Number} id ID Identificador único del registro <code>equipo_tecnico</code>
  *
  * @apiSuccessExample Respuesta exitosa:
  HTTP/1.1 200 OK
  {
      "finalizado": true,
      "mensaje": "Registro persona eliminado exitosamente del equipo tecnico.",
      "datos": null
  }
  * @apiErrorExample Ejemplo.- Error 404:
  {
      "finalizado": false,
      "codigo": 404,
      "mensaje": "No existe el registro de la persona solicitado."
  }
  */
  app.api.delete('/proyectos/:id_proyecto/equipos_tecnicos/:id', validate(paramValidation.eliminar), app.controller.equipo_tecnico.eliminar);
};
