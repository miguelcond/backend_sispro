const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getId: {
    params: {
      id: Joi.number().required()
    }
  },
  get: {
    params: { }
  },
  create: {
    body: {
      fid_modulo: Joi.number().required(),
      nombre: Joi.string().required(),
      unidad_medida: Joi.string().required(),
      cantidad: Joi.number().required(),
      precio_unitario: Joi.number().allow(null).allow('').optional(),
      tiempo_ejecucion: Joi.number().allow(null).allow('').optional()
    }
  },
  update: {
    params: {
      id: Joi.number().required()
    },
    body: {
      fid_modulo: Joi.number().allow(null).allow('').optional(),
      nombre: Joi.string().allow(null).allow('').optional(),
      unidad_medida: Joi.string().allow(null).allow('').optional(),
      cantidad: Joi.number().allow(null).allow('').optional(),
      precio_unitario: Joi.number().allow(null).allow('').optional(),
      tiempo_ejecucion: Joi.number().allow(null).allow('').optional()
    }
  }
};
