import validate from 'express-validation';
import paramValidation from './adjudicacion.validation';

module.exports = (app) => {
  app.api.get('/proyectos/adjudicaciones/:id_adjudicacion', app.controller.adjudicacion.getPorId);

  app.api.get('/proyectos/:id_proyecto/adjudicaciones', validate(paramValidation.listar), app.controller.adjudicacion.listar);
  app.api.post('/proyectos/:id_proyecto/adjudicaciones', validate(paramValidation.crear), app.controller.adjudicacion.crear);
  app.api.get('/proyectos/:id_proyecto/adjudicaciones/supervision', app.controller.adjudicacion.obtenerPorSupervisor);
  app.api.put('/proyectos/:id_proyecto/adjudicaciones/:id_adjudicacion', validate(paramValidation.actualizar), app.controller.adjudicacion.actualizar);
  app.api.put('/proyectos/:id_proyecto/adjudicaciones/:id_adjudicacion/guardar', validate(paramValidation.procesar), app.controller.adjudicacion.guardar);
  app.api.put('/proyectos/:id_proyecto/adjudicaciones/:id_adjudicacion/respaldo', validate(paramValidation.procesar), app.controller.adjudicacion.eliminarRespaldo);
  app.api.put('/proyectos/:id_proyecto/adjudicaciones/:id_adjudicacion/procesar', validate(paramValidation.procesar), app.controller.adjudicacion.procesar);
  app.api.put('/proyectos/:id_proyecto/adjudicaciones/:id_adjudicacion/eliminar', validate(paramValidation.eliminar), app.controller.adjudicacion.eliminar);
  app.api.put('/proyectos/:id_proyecto/adjudicaciones/:id_adjudicacion/esp_tecnicas', app.controller.adjudicacion.cargarEspTecnicas);
};
