import errors from '../../../lib/errors';
let fs = require('fs');

module.exports = (app) => {
  app.controller.servicioKhipus = {};

  async function obtenerProyectos (req, res, next) {
    try {
      const CONSULTA = {};
      const ID_ROL = 1;
      const ID_USUARIO= 1;
      const campos = ['codigo','limit','page'];
      for (let i in req.query) {
        if(campos.indexOf(i)<0 || !req.query[i]) {
          delete req.query[i];
        }
      }
      let result = await app.dao.khipus.getProyectos(CONSULTA, ID_ROL, ID_USUARIO, req.query);
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        cantidad: result.cantidad,
        proyectos: result.formatoKhipus,
      });
    } catch (error) {
      next(error);
    }
  }
  app.controller.servicioKhipus.obtenerProyectos = obtenerProyectos;

  async function obtenerDesembolsos (req, res, next) {
    try {
      const CONSULTA = {
        where: {
          codigo: req.params.codigo
        }
      };
      const ID_ROL = 1;
      const ID_USUARIO= 1;
      let result = await app.dao.khipus.getDesembolsos(CONSULTA, ID_ROL, ID_USUARIO, req.query);
      res.json({
        finalizado: (result.codigo)?true:false,
        mensaje: (result.codigo)?`Desembolsos del proyecto.`:`No existe el proyecto.`,
        codigo: result.codigo,
        nombre: result.nombre,
        desembolsos: result.desembolsos
      });
    } catch (error) {
      next(error);
    }
  }
  app.controller.servicioKhipus.obtenerDesembolsos = obtenerDesembolsos;

  async function obtenerFotografia (req, res, next) {
    try {
      let exists = fs.existsSync(`${_path}/uploads/${req.params.nombre}`);
      if (exists) {
        fs.readFile(`${_path}/uploads/${req.params.nombre}`, function(err, data) {
          if (err) {
            throw new errors.NotFoundError(`No se ha encontrado la imágen.`);
          }
          res.writeHead(200, {'Content-Type': 'image/jpeg'});
          res.end(data); // Send the file data to the browser.
        });
      } else if (fs.existsSync(`${_path}/uploads/proyectos/${req.params.codigo}/fotos/${req.params.nombre}`)) {
        fs.readFile(`${_path}/uploads/proyectos/${req.params.codigo}/fotos/${req.params.nombre}`, function(err, data) {
          if (err) {
            throw new errors.NotFoundError(`No se ha encontrado la imágen.`);
          }
          res.writeHead(200, {'Content-Type': 'image/jpeg'});
          res.end(data); // Send the file data to the browser.
        });
      } else {
        // TODO enviar una imagen con logo No encontrado
        res.json({
          mensaje: `La imagen no fue encontrada.`
        });
      }
    } catch (error) {
      next(error);
    }
  }
  app.controller.servicioKhipus.obtenerFotografia = obtenerFotografia;
};
