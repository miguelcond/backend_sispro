import fs from 'fs-extra';
import util from '../../../lib/util';
import fileType from 'file-type';
import shortid from 'shortid';
import seqOpt from '../../../lib/sequelize-options';
import errors from '../../../lib/errors';
import logger from '../../../lib/logger';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const proyectoModel = app.src.db.models.proyecto;
  const sequelize = app.src.db.sequelize;

  const output = {
    model: 'proyecto',
    fields: {
      id_proyecto: ['int', 'pk'],
      version: ['int'],
      codigo: ['str'],
      tipo: ['str'],
      nombre: ['str'],
      monto_fdi: ['float'],
      monto_contraparte: ['float'],
      monto_total_convenio: ['float'],
      monto_total: ['float'],
      plazo_ejecucion: ['int'],
      plazo_ejecucion_convenio: ['int'],
      entidad_beneficiaria: ['str'],
      fecha_suscripcion_convenio: ['date'],
      desembolso: ['float'],
      porcentaje_desembolso: ['float'],
      fecha_desembolso: ['date'],
      anticipo: ['float'],
      porcentaje_anticipo: ['float'],
      sector: ['str'],
      fid_autoridad_beneficiaria: ['int'],
      cargo_autoridad_beneficiaria: ['str'],
      fcod_municipio: ['str'],
      coordenadas: ['str'],
      orden_proceder: ['date'],
      fecha_fin_proyecto: ['date'],
      matricula: ['str'],
      estado_proyecto: ['enum'],
      nro_invitacion: ['str'],
      estado: ['enum'],
      documentacion_exp: ['str'],
      tipo_proyecto: {
        model: 'parametrica',
        fields: {
          codigo: ['str'],
          tipo: ['enum'],
          nombre: ['str']
        }
      },
      municipio: {
        model: 'municipio',
        fields: {
          codigo: ['str'],
          nombre: ['str'],
          point: ['str'],
          codigo_provincia: ['str'],
          estado: ['enum'],
          provincia: {
            model: 'provincia',
            fields: {
              codigo: ['str'],
              nombre: ['str'],
              codigo_departamento: ['str'],
              estado: ['enum'],
              departamento: {
                model: 'departamento',
                fields: {
                  codigo: ['str'],
                  nombre: ['str']
                }
              }
            }
          }
        }
      },
      estado_actual: {
        model: 'estado',
        fields: {
          codigo: ['str', 'pk'],
          nombre: ['str'],
          tipo: ['enum'],
          acciones: ['str'],
          atributos: ['str'],
          requeridos: ['str'],
          areas: ['str'],
          roles: ['array']
        }
      },
      autoridad_beneficiaria: {
        model: 'persona',
        fields: {
          tipo_documento: ['enum'],
          numero_documento: ['str'],
          fecha_nacimiento: ['date'],
          nombres: ['str'],
          primer_apellido: ['str'],
          segundo_apellido: ['str'],
          correo: ['str'],
          telefono: ['str']
        }
      },
      observacion: {
        fields: {
          mensaje: ['str'],
          usuario: {
            fields: {
              persona: {
                fields: {
                  nombres: ['str'],
                  primer_apellido: ['str'],
                  segundo_apellido: ['str']
                }
              }
            }
          }
        }
      },
      supervisiones: {
        fields: {
          id_supervision: ['int', 'pk'],
          nro_supervision: ['int'],
          estado_supervision: ['enum', 'fk'],
          fecha_inicio: ['date'],
          fecha_fin: ['date'],
          informe: ['str'],
          desembolso: ['float'],
          descuento_anticipo: ['float'],
          version_proyecto: ['int'],
          version_adjudicacion: ['int'],
          fid_proyecto: ['int', 'fk'],
          observacion_proceso: {
            fields: {
              mensaje: ['str'],
              fid_usuario: ['int', 'fk'],
              usuario: {
                fields: {
                  persona: {
                    fields: {
                      nombres: ['str'],
                      primer_apellido: ['str'],
                      segundo_apellido: ['str']
                    }
                  }
                }
              }
            }
          },
          estado_actual: {
            fields: {
              codigo: ['enum', 'pk'],
              codigo_proceso: ['enum'],
              nombre: ['str'],
              tipo: ['enum'],
              fid_rol: ['array']
            }
          },
          fotos_supervision: {
            fields: {
              id_foto_supervision: ['int'],
              path_fotografia: ['str'],
              fecha_creacion: ['date'],
              coordenadas: ['str']
            }
          }
        }
      },
      equipo_tecnico: {
        fields: {
          tipo_equipo: ['str'],
          cargo: ['str'],
          fid_rol: ['int'],
          usuario: {
            fields: {
              persona: {
                fields: {
                  numero_documento: ['str'],
                  nombres: ['str'],
                  primer_apellido: ['str'],
                  segundo_apellido: ['str']
                }
              }
            }
          }
        }
      },
      formularios: {
        fields: {
          form_model: ['str'],
          codigo_estado: ['str'],
          fid_plantilla: ['str'],
          nombre: ['str']
        }
      },
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date']
    }
  };

  const getProyectos = async (consulta, idRol, idUsuario, query = {}) => {

    const sectores = await models.parametrica.findAll({
      attributes: ['codigo', 'tipo', 'nombre', 'descripcion', 'orden'],
      where: {
        tipo: 'SECTOR'
      }
    });
    consulta = {
      distinct: true,
      include: [{
        model: models.municipio,
        attributes: ['codigo', 'nombre', 'codigo_provincia', 'point'],
        as: 'municipio',
        include: {
          model: models.provincia,
          attributes: ['codigo', 'nombre', 'codigo_departamento'],
          as: 'provincia',
          include: {
            attributes: ['codigo', 'nombre'],
            model: models.departamento,
            as: 'departamento'
          }
        }
      },{
        model: models.supervision,
        attributes: ['id_supervision','nro_supervision','fecha_inicio','fecha_fin','informe','estado_supervision','desembolso','descuento_anticipo','version_proyecto','version_adjudicacion'],
        as: 'supervisiones',
        where: {
          estado: 'ACTIVO',
          estado_supervision: 'EN_ARCHIVO_FDI'
        },
        required: false,
        include: {
          model: models.estado,
          as: 'estado_actual'
        }
      },{
        model: models.persona,
        as: 'autoridad_beneficiaria',
        attributes: ['nombres','primer_apellido','segundo_apellido'],
        required: false
      },{
        model: models.equipo_tecnico,
        as: 'equipo_tecnico',
        attributes: ['fid_rol','cargo'],
        required: false,
        where: {
          cargo: ['CG_SUPERVISOR','CG_FISCAL'],
          tipo_equipo: 'TE_BENEFICIARIO'
        },
        include: {
          model: models.usuario,
          as: 'usuario',
          include: {
            model: models.persona,
            as: 'persona',
            attributes: ['nombres','primer_apellido','segundo_apellido']
          }
        }
      },{
        model: models.formulario,
        as: 'formularios',
        attributes: ['form_model','codigo_estado','fid_plantilla','nombre'],
      }],
      order: [['_fecha_modificacion','DESC'],[{model: models.supervision,as: 'supervisiones'},'nro_supervision','ASC']],
    };
    if(!consulta.where) { consulta.where = {}; }
    if(query.codigo&&query.codigo!=='0') {
      consulta.where.codigo=query.codigo;
    }
    consulta.limit = (query.limit && parseInt(query.limit) >= 0) ? parseInt(query.limit) : 50;
    consulta.offset = (query.page && parseInt(query.page) > 0) ? ((parseInt(query.page) - 1) * consulta.limit) : 0;
    const result = await proyectoModel.findAndCountAll(consulta);

    let resOptimo = {
      count: result.count,
      rows: seqOpt.createResult(query, util.json(result.rows), output)
    };

    let formatoKhipus = [];
    for(let i in resOptimo.rows) {
      let res = resOptimo.rows[i];

      // Obtener datos de formularios dinamicos
      let forms = res.formularios;
      for (let j in forms) {
        res[forms[j].nombre] = forms[j].form_model;
      }
      delete res.formularios;

      // Obtner nombre de sector
      res.sector = util.json(sectores).find((sector)=>{ return sector.codigo===res.sector; })
      // Si no se registro las coordenadas del proyecto se toma como referencia el municipio.
      if(!res.coordenadas) {
        res.coordenadas = res.municipio.point;
      }
      // Calcular porcentaje avace fisico
      let fecha_fin = null;
      let observaciones;
      let monto_total_ejecutado = 0;
      let monto_total_desembolsado = 0;
      let estado_supervision;
      let supervision = {};
      for(let j in res.supervisiones) {
        monto_total_desembolsado += res.supervisiones[j].desembolso;
        monto_total_ejecutado += res.supervisiones[j].desembolso + res.supervisiones[j].descuento_anticipo;
        if (res.supervisiones[j].estado_supervision==='EN_ARCHIVO_FDI') {
          supervision.observaciones = res.supervisiones[j].informe;
        }
        if(res.estado_proyecto==='CERRADO_FDI') {
          fecha_fin = res.supervisiones[j].fecha_fin
        }
        supervision.estado = res.supervisiones[j].estado_actual.nombre;
        supervision.nro = res.supervisiones[j].nro_supervision;
        res._fecha_modificacion = res.supervisiones[j].fecha_fin;
      }
      // Autoridad y equipotecnico
      let autoridad_beneficiaria;
      let equipo_tecnico;
      if (res.autoridad_beneficiaria) {
        autoridad_beneficiaria = res.autoridad_beneficiaria;
      }
      if (res.equipo_tecnico.length>0) {
        equipo_tecnico = {
          supervisor: res.equipo_tecnico.find((p)=>{return p.cargo==='CG_SUPERVISOR';}),
          fiscal: res.equipo_tecnico.find((p)=>{return p.cargo==='CG_FISCAL';})
        }
        if(equipo_tecnico.supervisor) equipo_tecnico.supervisor = equipo_tecnico.supervisor.usuario.persona;
        if(equipo_tecnico.fiscal) equipo_tecnico.fiscal = equipo_tecnico.fiscal.usuario.persona;
      }
      // Empresa
      let empresa;
      if (res.documentacion_exp && res.documentacion_exp.datos_empresa && res.documentacion_exp.datos_empresa.razon_social) {
        empresa = {
          razon_social: res.documentacion_exp.datos_empresa.razon_social
        }
      }
      // Avance fisico
      let avance_fisico = util.calcularPorcentaje(monto_total_ejecutado, res.monto_total);

      // Obtener configuración calc fecha
      let configCalcFecha = await app.dao.parametrica.obtenerPorCodigoTipo('CFG_CALC_FECHA', 'CONFIG');
      let configRestarDias = configCalcFecha && configCalcFecha.descripcion ? parseInt(configCalcFecha.descripcion) : 0;

      // Calcular fecha final de proyecto.
      let fecha_provisional = null
      if (res.orden_proceder) {
        fecha_provisional = new Date(res.orden_proceder);
        fecha_provisional.setDate(fecha_provisional.getDate() + res.plazo_ejecucion - configRestarDias);
      } else {
        observaciones = 'Proyecto no aprobado.';
      }

      if (!res.sector && res.beneficiarios) {
        res.sector = {};
        switch(res.beneficiarios.tipo) {
          case 'OP_TP_PRODUCTIVO': res.sector.nombre = 'Productivo'; break;
          case 'OP_TP_RIEGO': res.sector.nombre = 'Recursos hídricos y riego'; break;
          case 'OP_TP_PUENTES': res.sector.nombre = 'Transporte'; break;
          default: res.sector.nombre = '-';
        }
        res.tipo = res.beneficiarios.subtipo_productivo ? res.beneficiarios.subtipo_productivo.replace('ST_PROD_','') : res.beneficiarios.tipo ? res.beneficiarios.tipo.replace('OP_TP_','') : undefined;
      }
      if (res.cronograma_desembolsos) {
        observaciones = 'Desembolsos FDI: ';
        let n = 1;
        for (let i in res.cronograma_desembolsos) {
          if (i.startsWith('monto')) {
            observaciones += ' '+res.cronograma_desembolsos[`monto_${n}`]+' ';
            observaciones += (res.cronograma_desembolsos[`fecha_${n}`] ? res.cronograma_desembolsos[`fecha_${n}`] : '') + ' |';
            n++;
          }
          // console.log(res.cronograma_desembolsos);
        }
      }

      if (query.codigo == '0') {
        formatoKhipus.push({
          codigo: res.codigo,
          modificado: res._fecha_modificacion,
        });
      } else {
        formatoKhipus.push({
          codigo: res.codigo,
          nombre: res.nombre,
          sisin: res.cronograma_desembolsos? res.cronograma_desembolsos.codigo_sisin: '',
          descripcion: '',
          tipo: res.tipo,
          fecha_inicio: res.orden_proceder,
          plazo_ejecucion: res.plazo_ejecucion,
          fecha_fin,
          fecha_provisional,
          monto_total: res.monto_total,
          monto_total_ejecutado,
          avance_fisico,
          observaciones,
          revisor: '',
          director: '',
          departamento: res.municipio.provincia.departamento.nombre,
          provincia: res.municipio.provincia.nombre,
          municipio: res.municipio.nombre,
          sector: res.sector?res.sector.nombre:'-',
          latitude: (res.coordenadas && res.coordenadas.coordinates)?res.coordenadas.coordinates[1]:null,
          longitude: (res.coordenadas && res.coordenadas.coordinates)?res.coordenadas.coordinates[0]:null,
          localidad: res.municipio.nombre,
          estado: res.estado_proyecto,
          supervision,
          institucion: res.entidad_beneficiaria,
          autoridad_beneficiaria,
          equipo_tecnico,
          empresa,
          fecha_suscripcion: res.fecha_suscripcion_convenio,
          modificado: res._fecha_modificacion,
        });
      }
    }
    return {
      formatoKhipus,
      cantidad: resOptimo.count
    };
  };

  const getDesembolsos = async (consulta, idRol, idUsuario, query = {}) => {

    const result = await proyectoModel.find({
      where: consulta.where,
      include: [{
        model: models.municipio,
        attributes: ['codigo', 'nombre', 'codigo_provincia', 'point'],
        as: 'municipio',
        include: {
          model: models.provincia,
          attributes: ['codigo', 'nombre', 'codigo_departamento'],
          as: 'provincia',
          include: {
            attributes: ['codigo', 'nombre'],
            model: models.departamento,
            as: 'departamento'
          }
        }
      }, {
        model: models.supervision,
        attributes: ['id_supervision','nro_supervision','fecha_inicio','fecha_fin','informe','estado_supervision','desembolso','descuento_anticipo','version_proyecto','version_adjudicacion'],
        as: 'supervisiones',
        where: {
          estado: 'ACTIVO',
          estado_supervision: 'EN_ARCHIVO_FDI'
        },
        required: false,
        include: {
          model: models.foto_supervision,
          attributes: ['path_fotografia','fecha_creacion','coordenadas'],
          as: 'fotos_supervision',
          required: false,
          where: {
            fid_item: null,
            tipo: 'TF_SUPERVISION'
          }
        }
      }],
      order: [[{model: models.supervision,as: 'supervisiones'},'nro_supervision','ASC']],
    });

    if (!result) {
      return {
        desembolsos: []
      };
    }
    let res = seqOpt.createResult(query, util.json(result), output);

    let formatoKhipus = {
      codigo: res.codigo,
      nombre: res.nombre,
      desembolsos: []
    };
    let monto_total_ejecutado = 0;
    let monto_total_desembolsado = 0;
    for (let j in res.supervisiones) {
      let sup = res.supervisiones[j];
      monto_total_desembolsado += sup.desembolso;
      monto_total_ejecutado += sup.desembolso + sup.descuento_anticipo;
      formatoKhipus.desembolsos.push({
        nro: sup.nro_supervision,
        fecha: sup.fecha_fin,
        monto_total: sup.desembolso,
        avance_fisico: util.calcularPorcentaje(monto_total_ejecutado, res.monto_total),
        avance_financiero: util.calcularPorcentaje(monto_total_desembolsado, res.monto_total),
        fotos: sup.fotos_supervision
      });
    }
    return formatoKhipus;
  };

  _app.dao.khipus = {
    getProyectos,
    getDesembolsos
  };
};
