import util from '../../../lib/util';
import errors from '../../../lib/errors';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  _app.dao.estado = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function destinosEstado (proceso, atributoEstado, idRol, daoProceso) {
    let options;
    if (idRol === 16 || idRol === 18) {
      options = {
        attributes: ['codigo', 'acciones'],
        where: {
          $and: [
            {
              codigo: proceso[atributoEstado]
            }
          ]
        }
      };
    } else {
      options = {
        attributes: ['codigo', 'acciones'],
        where: {
          $and: [
            {
              codigo: proceso[atributoEstado]
            },
            sequelize.literal(idRol + ' = ANY(fid_rol)')
          ]
        }
      };
    }
    let estado = await models.estado.findOne(options);
    if (!estado) {
      return null;
    }
    estado = JSON.parse(JSON.stringify(estado));
    if (!Array.isArray(estado.acciones)) {
      estado.acciones = estado.acciones[idRol];
    }
    const res = {};
    for (let i = 0; i < estado.acciones.length; i++) {
      let accion = estado.acciones[i];
      if (accion.estado && (!accion.condicion || (await evaluar(accion.condicion, proceso, daoProceso)) > 0)) {
        res[accion.estado] = estado.codigo;
      }
    }
    return res;
  }

  async function siguiente (cod, idRol, proyecto, t) {
    let estado = await models.estado.findOne({
      attributes: ['codigo', 'acciones'],
      where: {
        $and: [
          {
            codigo: cod
          },
          sequelize.literal(idRol + ' = ANY(fid_rol)')
        ]
      },
      transaction: t
    });
    if (!estado) {
      return null;
    }
    estado = JSON.parse(JSON.stringify(estado));
    if (!Array.isArray(estado.acciones)) {
      estado.acciones = estado.acciones[idRol];
    }
    let res;
    for (let i = 0; i < estado.acciones.length; i++) {
      let accion = estado.acciones[i];
      if (accion.estado) {
        if (accion.condicion && await app.dao.proyecto.evaluar(accion.condicion, proyecto) === 0) {
          continue;
        }
        if (res === undefined) {
          res = accion.estado;
        } else {
          res = null;
        }
      }
    }
    return res;
  }

  async function obtenerEstadosPorRol (idRol) {
    return models.estado.findAll({
      attributes: ['codigo', 'codigo_proceso', 'tipo'],
      where: {
        $and: [
          app.src.db.sequelize.literal(idRol + ' = ANY(fid_rol)')
        ]
      }
    });
  }

  async function validarSiguiente (objProceso, cambio, idRol, t) {
    let estado = await app.src.db.models.estado.findOne({
      where: {
        codigo: cambio.origen
      },
      transaction: t
    });
    estado = util.json(estado);
    const ACCIONES = getDatos(estado, 'acciones', idRol);
    for (let i = 0; i < ACCIONES.length; i++) {
      if (ACCIONES[i].estado && ACCIONES[i].estado === cambio.destino && (ACCIONES[i].observacion || ACCIONES[i].rechazar)) {
        return null;
      }
    }
    const REQUERIDOS = getDatos(estado, 'requeridos', idRol);
    for (let i = 0; i < REQUERIDOS.length; i++) {
      const ATRIBUTO = REQUERIDOS[i];
      const ATRIBUTOS = ATRIBUTO.split('.');
      if (ATRIBUTOS.length === 1) {
        if (objProceso[ATRIBUTO] === null || (Array.isArray(objProceso[ATRIBUTO]) && objProceso[ATRIBUTO].length === 0)) {
          throw new errors.ValidationError(`Debe introducir un valor para ${ATRIBUTO}.`);
        }
      } else {
        validarAtributo(objProceso, ATRIBUTOS, ATRIBUTO);
      }
    }
    return null;
  }

  function validarAtributo (objeto, atributos, atributo) {
    if (atributos.length > 0) {
      if (Array.isArray(objeto)) {
        let a = atributos.shift();
        for (let i = 0; i < objeto.length; i++) {
          validarAtributo(objeto[i][a], atributos, atributo);
        }
      } else {
        let a = atributos.shift();
        validarAtributo(objeto[a], atributos, atributo);
      }
    } else {
      if (!objeto || (Array.isArray(objeto) && objeto.length === 0)) {
        throw new errors.ValidationError(`Debe introducir un valor para ${atributo}.`);
      }
    }
  }

  function getDatos (objeto, elemento, idRol) {
    let datos = [];
    if (Array.isArray(objeto[elemento])) {
      datos = objeto[elemento];
    } else if (objeto[elemento][idRol]) {
      datos = objeto[elemento][idRol];
    }
    return datos;
  }

  async function ejecutarMetodos (objProceso, cambio, idRol, daoProceso, idUsuario) {
    let estado = await app.src.db.models.estado.findOne({
      where: {
        codigo: cambio.origen
      }
    });

    estado = util.json(estado);
    const ACCIONES = getDatos(estado, 'acciones', idRol);
    const ACCION = _.find(ACCIONES, {estado: cambio.destino});
    if (!ACCION.ejecutar) {
      return null;
    }
    return daoProceso.ejecutarMetodos(objProceso, ACCION.ejecutar, idUsuario);
  }

  async function getPorCodigo (codigo) {
    const ESTADO = await models.estado.findOne({
      where: {
        codigo: codigo
      }
    });
    if (!ESTADO) {
      throw new errors.NotFoundError(`No existe el código indicado.`);
    }
    return ESTADO;
  }

  async function validarDestinos (procesoActual, proceso, atributoEstado, estadoProcesoActual, idRol, daoProceso) {
    console.log("PERMISOS PARA VALIDAR DESTINOS "+idRol+" ===> "+proceso[atributoEstado]+" ==== "+estadoProcesoActual);
    if (proceso[atributoEstado] !== estadoProcesoActual || (proceso[atributoEstado] === estadoProcesoActual && (idRol === 16 || idRol === 18))) {
      const DESTINOS = await destinosEstado(procesoActual, atributoEstado, idRol, daoProceso);
      console.log("***** VALIDANDO DESTINOS ******");
      if (!DESTINOS || !DESTINOS[proceso[atributoEstado]] || (DESTINOS[proceso[atributoEstado]] !== estadoProcesoActual)) {
        throw new errors.ValidationError(`No puede avanzar al estado indicado.`);
      }
    } else {
      throw new errors.ValidationError(`El proceso ya se encuentra en el estado indicado.`);
    }
  }

  async function verificarPermisos (procesoActual, estado, idRol, idUsuario, idProceso) {
    console.log(`ROL USUARIO: ${idRol}`);
    if(idRol!=1){
      if (estado.fid_rol.indexOf(idRol) === -1 && !(estado.codigo === 'APROBADO_FDI' &&  [16,18].indexOf(idRol)!==-1) && !(estado.codigo === 'EN_EJECUCION_FDI' && [14,15,16,17,18].indexOf(idRol)!==-1)) {
        throw new errors.ValidationError(`No tiene los privilegios requeridos para realizar esta acción.`);
      }
      if (idProceso && estado.fid_rol_asignado.indexOf(idRol) !== -1 && procesoActual.fid_usuario_asignado && procesoActual.fid_usuario_asignado !== idUsuario && idRol !== 18 ) {
        throw new errors.ValidationError(`No tiene los privilegios requeridos para realizar esta acción.`);
      }
    }
    return null;
  }

  async function getAtributosPermitidos (estadoActual, idRol) {
    return getDatos(estadoActual, 'atributos', idRol);
  }

  async function agregarAtributosPermitidos (data, proceso, atributosPermitidos) {
    for (let i = 0; i < atributosPermitidos.length; i++) {
      const atributo = atributosPermitidos[i];
      if (proceso[atributo] !== null && proceso[atributo] !== undefined) {
        data[atributo] = proceso[atributo];
      }
    }
    return data;
  }

  async function ejecutarAutomaticos (data, estadoActual, daoProceso) {
    let automaticos = estadoActual.automaticos;
    const key = automaticos.keys ? automaticos.keys : Object.keys(automaticos);
    for (let i = 0; i < key.length; i++) {
      data[key[i]] = await evaluar(automaticos[key[i]], data, daoProceso);
    }
  }

  async function evaluar (erpn, proceso, daoProceso) {
    switch (typeof erpn) {
      case 'string':
        return getValor(erpn, proceso, daoProceso);
      case 'object':
        if (await getValor(erpn.condicion, proceso, daoProceso) === 1) {
          return getValor(erpn.valor, proceso, daoProceso);
        }
        break;
    }
  }

  async function getValor (erpn, proceso, daoProceso) {
    let rpn = erpn.split(' ');
    let cola = [];
    let v;
    while (rpn.length > 0) {
      var e = rpn.shift();
      switch (e) {
        case '^':
          v = cola.pop();
          cola.push(Math.pow(cola.pop(), v));
          break;
        case '*':
          v = cola.pop();
          cola.push(util.multiplicar([cola.pop(), v]));
          break;
        case '/':
          v = cola.pop();
          cola.push(cola.pop() / v);
          break;
        case '%':
          v = cola.pop();
          cola.push(cola.pop() % v);
          break;
        case '+':
          v = cola.pop();
          cola.push(cola.pop() + v);
          break;
        case '-':
          v = cola.pop();
          cola.push(cola.pop() - v);
          break;
        case '==':
          v = cola.pop();
          cola.push(cola.pop() === v ? 1 : 0);
          break;
        default:
          if (/^[0-9|.]+$/.test(e)) {
            cola.push(parseFloat(e));
          } else {
            if (/^[A-Z|_]+$/.test(e)) {
              cola.push(e);
            } else {
              if (proceso[e] !== undefined) {
                cola.push(proceso[e]);
              } else {
                await daoProceso.getValoresAutomaticos(cola, e, proceso);
              }
            }
          }
          break;
      }
    }
    return cola.pop();
  }

  async function getEstadoInicial (tipo) {
    return sequelize.models.estado.findOne({
      attributes: ['codigo', 'fid_rol'],
      where: {
        codigo_proceso: tipo,
        tipo: 'INICIO'
      }
    });
  }

  async function getEstadoFinal (tipo) {
    return sequelize.models.estado.findOne({
      attributes: ['codigo', 'fid_rol'],
      where: {
        codigo_proceso: tipo,
        tipo: 'FIN'
      }
    });
  }

  async function getEstadosPorCodigo (codigo) {
    const ESTADO = await getPorCodigo(codigo);
    const ESTADOS = await sequelize.models.estado.findAll({
      attributes: ['codigo'],
      where: {
        codigo_proceso: ESTADO.codigo_proceso
      }
    });
    return _.map(util.json(ESTADOS), 'codigo');
  }

  function getDatosPermitidos (datos, idRol) {
    if (!Array.isArray(datos)) {
      if (!datos[idRol]) {
        return [];
      }
      return datos[idRol];
    }
    return datos;
  }

  async function revisarCondiciones (objeto, elemento) {
    let i = elemento.length;
    while (i--) {
      if (_.isObject(elemento[i])) {
        if (await getValor(elemento[i].condicion, objeto) === 1) {
          elemento[i] = elemento[i].valor;
        } else {
          elemento.splice(i, 1);
        }
      }
    }
    return elemento;
  }

  const prepararDatosPermitidos = async (objeto, idRol) => {
    objeto.estado_actual.atributos = getDatosPermitidos(objeto.estado_actual.atributos, idRol);
    objeto.estado_actual.requeridos = getDatosPermitidos(objeto.estado_actual.requeridos, idRol);
    objeto.estado_actual.areas = getDatosPermitidos(objeto.estado_actual.areas, idRol);
    objeto.estado_actual.atributos = await revisarCondiciones(objeto, objeto.estado_actual.atributos);
  };

  const esModificacionProyecto = (estado) => {
    return estado.codigo === 'MODIFICADO_FDI';
  };

  const esModificacionAdjudicacion = (estado) => {
    return estado.codigo === 'MODIFICADO_FDI';
  };

  _app.dao.estado = {
    destinosEstado,
    siguiente,
    obtenerEstadosPorRol,
    validarSiguiente,
    ejecutarMetodos,
    getPorCodigo,
    validarDestinos,
    verificarPermisos,
    getAtributosPermitidos,
    agregarAtributosPermitidos,
    ejecutarAutomaticos,
    getEstadoInicial,
    getEstadoFinal,
    getEstadosPorCodigo,
    evaluar,
    getValor,
    getDatosPermitidos,
    revisarCondiciones,
    prepararDatosPermitidos,
    esModificacionProyecto,
    esModificacionAdjudicacion
  };
};
