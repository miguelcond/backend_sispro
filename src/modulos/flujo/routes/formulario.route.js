import sequelizeFormly from 'sequelize-formly';
import validate from 'express-validation';
import paramValidation from './formulario.validation';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {
  const R = app.src.config.config.api.main;

  /**
   * @apiVersion 1.0.0
   * @apiGroup Formulario
   * @apiName Post formularios
   * @api {post} /api/v1/proyectos/:id_proyecto/evaluaciones Crear registro de formulario
   *
   * @apiDescription Post formulario, agrega un nuevo registro de formulario de evaluación
   *
   * @apiParamExample {json} Ejemplo de petición:
    {
      "$_nombre": "evaluacion",
      "$_plantilla": 101,
      "beneficiarios": {
        "$_nombre": "beneficiarios",
        "$_plantilla": 1,
        "comunidades": [
          {
            "coordenadas": {
              "lat": -16.49361009499995,
              "lng": -68.13469102499994
            },
            "nombre": "loc",
            "nro": 1
          }
        ],
        "indicadores_impacto": "123 t",
        "indicadores_impacto_numero": "123",
        "indicadores_impacto_unidades": "t",
        "nro_comunidades": 1,
        "nro_familias": 123,
        "plazo_ejecucion": 123,
        "productivo": {
          "agricola": true
        },
        "referencia": "test",
        "subtipo_productivo": "ST_PROD_AGRICOLA",
        "tamanio_proyecto": "OP_TP_MEDIANO",
        "tipo": "OP_TP_PRODUCTIVO",
        "tipo_financiamiento": "OP_TF_PUBLICA",
        "version": 1
      },
      "cod": 1,
      "codigo": "r1Kqb8rXQ",
      "datos": [...],
      "datos2": [...],
      "datos3": [...],
      "datos4": [...],
      "datos5": [...],
      "departamento": {
        "codigo": "02",
        "nombre": "La Paz"
      },
      "financiamiento": {
        "$_nombre": "financiamiento",
        "$_plantilla": 2,
        "cuadro": {
          "datos": [...],
          "tipo": 1,
          "totales": {
            "beneficiarios": 0,
            "costo_total": 269872,
            "fdi": 234936,
            "gam": 34936,
            "otros": 0,
            "privado": 0,
            "publico": 0
          }
        },
        "monto_beneficiarios": 0,
        "monto_contraparte": 34936,
        "monto_fdi": 234936,
        "monto_otros": 0,
        "monto_total_proyecto": 269872,
        "version": 1
      },
      "jefe_area": {
        "cargo": "Jefe Area Revision Tecnica de Proyectos",
        "nombre": "JEFE REVISION"
      },
      "jefe_depto": {
        "cargo": "Jefe del Departamento Tecnico de Proyectos",
        "nombre": "Grover Oblitas Roselio"
      },
      "municipio": {
        "codigo": "020101",
        "codigo_provincia": "0201",
        "nombre": "Nuestra Señora de La Paz",
        "point": {
          "coordinates": [-68.13469102499994, -16.49361009499995],
          "type": "Point"
        }
      },
      "nombre": "test",
      "nro_convenio": "FDI/DTP/ARTP/0001-2018",
      "provincia": {
        "codigo": "0201",
        "codigo_departamento": "02",
        "nombre": "Murillo"
      },
      "responsable_edtp": "MARY MEJIA MOOR<br>gdfgdfg@hyju.olp - 74666666",
      "responsable_gam_gaioc": "MARY MATORRA APONTE<br>werwet@rete.ghy - 74851111",
      "revisor": {
        "cargo": "Tecnico Revisor",
        "nombre": "TECNICO FDI"
      }
    }
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Documento de evaluación generado correctamente."
      "datos": {
        "codigo_documento": "rJl6nIrQm",
        "fecha_documento": "2018-07-12T22:56:56.024Z",
        "nombre": "evaluacion"
      }
    }
   */
  app.api.post('/proyectos/:id_proyecto/evaluaciones', app.controller.evaluacion.generar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Formulario
   * @apiName Listar formularios
   * @api {get} /api/v1/proyectos/:id_proyecto/evaluaciones Obtiene una lista de formularios de evaluación
   *
   * @apiDescription Listar formularios, obtiene una lista de formularios de evaluación
   *
   * @apiSuccess (Respuesta) {boolean} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {String} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {Object} datos Objeto que contiene información sobre los formularios de evaluación

   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   * SIN FORMULARIOS
    {
      "finalizado": true,
      "mensaje": "Historial de evaluaciones obtenido correctamente",
      "datos": []
    }
   */
  app.api.get('/proyectos/:id_proyecto/evaluaciones', app.controller.evaluacion.listarHistorico);
  app.api.get('/proyectos/:id_proyecto/evaluaciones/:id_formulario', validate(paramValidation.obtenerHistorico), app.controller.evaluacion.obtenerHistorico);
  // Servicio temporal para modificar documento evaluacion e informe
  app.api.put('/proyectos/:id_proyecto/formulario/:nombre/:id_formulario', validate(paramValidation.corregirDocumento), app.controller.evaluacion.corregirDocumento);
};
