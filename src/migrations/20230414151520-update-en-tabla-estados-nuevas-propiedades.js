'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `UPDATE public.estado
        SET atributos_detalle='{
         "item": {
          "put": ["nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario", "peso_ponderado"], 
          "post": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario", "peso_ponderado","regularizar","variacion"], 
          "acciones": ["post", "put"]
         },
         "modulo": {
          "put": ["nombre"], 
          "post": ["fid_proyecto", "fid_adjudicacion", "nombre"], 
          "acciones": ["post", "put"]
         }
        }'::jsonb
        WHERE codigo='APROBADO_FDI';


        UPDATE public.estado
        SET atributos_detalle='{
          "item": {
            "put": ["nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario", "peso_ponderado","regularizar","variacion"], 
            "post": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario", "peso_ponderado","regularizar","variacion"], 
            "acciones": ["post", "put"]
          },
          "modulo": {
            "put": ["nombre"], 
            "post": ["fid_proyecto", "fid_adjudicacion", "nombre"], 
            "acciones": ["post", "put"]
          }
        }'::jsonb
        WHERE codigo='ADJUDICADO_FDI';`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};