'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let ruta = migration.sequelize.import('../models/autenticacion/ruta');
    ruta.findOne({
      where: {
        ruta: '/api/v1/estados'
      }
    }).then((result) => {
      if (!result) {
        migration.bulkInsert('ruta',
          [
            {
              ruta: '/api/v1/estados',
              descripcion: 'Ruta para administrar estados',
              method_get: true,
              method_post: false,
              method_put: true,
              method_delete: false,
              estado: 'ACTIVO',
              _usuario_creacion: '1',
              _fecha_creacion: new Date(),
              _fecha_modificacion: new Date()
            }
          ]
        , {});
      } else {
        migration.bulkUpdate('ruta',
        {
          ruta: '/api/v1/estados',
          descripcion: 'Ruta para administrar estados',
          method_get: true,
          method_post: false,
          method_put: true,
          method_delete: false,
          estado: 'ACTIVO',
          _usuario_creacion: '1',
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date()
        },
        {
          ruta: '/api/v1/estados'
        }, {
          returning: true, raw: true
        });
      }
    });
    const sql = (
      ``
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      ``
      // `DELETE FROM ruta WHERE ruta = '/api/v1/estados';`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
