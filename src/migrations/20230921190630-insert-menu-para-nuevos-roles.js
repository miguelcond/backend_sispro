'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
//insercion bandejas pendientes en roles jefe_cierre y tecnico_cierre
    const sql = (
       `INSERT INTO public.rol_menu
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_menu, fid_rol)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 10, 22);
        INSERT INTO public.rol_menu
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_menu, fid_rol)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 11, 22);
        INSERT INTO public.rol_menu
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_menu, fid_rol)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 10, 21);
        INSERT INTO public.rol_menu
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_menu, fid_rol)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 11, 21);

        `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};