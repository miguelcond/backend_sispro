'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        acciones: `[{"icon":"fa-rotate-left","tipo":"boton","class":"btn-warning","label":"Observar","estado":"REGISTRO_PROYECTO_FDI","mensaje":"Observar y devolver al técnico del proyecto","observacion":true},{"icon":"fa-save","tipo":"boton","class":"btn-primary","label":"Guardar","estado":"REGISTRO_CONVENIO_FDI","guardar":true,"mensaje":"Guardar datos del proyecto"},{"icon":"fa-send","tipo":"boton","class":"btn-success","label":"Enviar","estado":"REGISTRO_DESEMBOLSO_FDI"}]`,
    }, {
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'REGISTRO_CONVENIO_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {
      return migration.bulkUpdate('estado', {
          acciones: `[{"ruta":"/proyecto/:id_proyecto","tipo":"ventana"},{"icon":"fa-rotate-left","tipo":"boton","class":"btn-warning","label":"Observar","estado":"REGISTRO_PROYECTO_FDI","mensaje":"Observar y devolver al técnico del proyecto","observacion":true},{"icon":"fa-save","tipo":"boton","class":"btn-primary","label":"Guardar","estado":"REGISTRO_DESEMBOLSO_FDI","guardar":true,"mensaje":"Guardar datos del proyecto"},{"icon":"fa-send","tipo":"boton","class":"btn-success","label":"Enviar","estado":"ASIGNACION_EQUIPO_TECNICO_FDI","mensaje":""}]`,
      }, {
        codigo_proceso: 'PROYECTO-FDI',
        codigo: 'REGISTRO_DESEMBOLSO_FDI'
      }, {
        returning: true, raw: true
      });
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        acciones: `[{"ruta":"/proyecto/:id_proyecto","tipo":"ventana"},{"icon":"fa-rotate-left","tipo":"boton","class":"btn-warning","label":"Observar","estado":"REGISTRO_PROYECTO_FDI","mensaje":"Observar y devolver al técnico del proyecto","observacion":true},{"icon":"fa-send","tipo":"boton","class":"btn-success","label":"Enviar","estado":"ASIGNACION_EQUIPO_TECNICO_FDI","mensaje":""}]`,
    }, {
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'REGISTRO_DESEMBOLSO_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {
      return migration.bulkUpdate('estado', {
          acciones: `[{"icon":"fa-rotate-left","tipo":"boton","class":"btn-warning","label":"Observar","estado":"REGISTRO_PROYECTO_FDI","mensaje":"Observar y devolver al técnico del proyecto","observacion":true},{"icon":"fa-send","tipo":"boton","class":"btn-success","label":"Enviar","estado":"REGISTRO_DESEMBOLSO_FDI"}]`,
      }, {
        codigo_proceso: 'PROYECTO-FDI',
        codigo: 'REGISTRO_CONVENIO_FDI'
      }, {
        returning: true, raw: true
      });
    }).finally(done);
  },
};
