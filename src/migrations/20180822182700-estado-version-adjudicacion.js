'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      automaticos: '{"desembolso":"$calculoDesembolso", "descuento_anticipo":"$calculoDescuentoPorAnticipo", "version_proyecto":"$versionProyecto", "version_adjudicacion":"$versionAdjudicacion"}',
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_FISCAL_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      automaticos: '{"desembolso":"$calculoDesembolso", "descuento_anticipo":"$calculoDescuentoPorAnticipo", "version_proyecto":"$versionProyecto"}',
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_FISCAL_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);
  },
};
