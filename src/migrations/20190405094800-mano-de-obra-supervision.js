'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('supervision');
    return migration.addColumn('supervision', 'mano_obra_calificada', {
      type: DataTypes.INTEGER,
      xlabel: 'Mano de obra calificada',
      allowNull: false,
      defaultValue: 0
    },{
      returning: true, raw: true
    }).then((resp) => {
      return migration.addColumn('supervision', 'mano_obra_no_calificada', {
        type: DataTypes.INTEGER,
        xlabel: 'Mano de obra no calificada',
        allowNull: false,
        defaultValue: 0
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      console.timeEnd('supervision');
    }).catch((e) => {
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE supervision DROP COLUMN IF EXISTS mano_obra_calificada;
      ALTER TABLE supervision DROP COLUMN IF EXISTS mano_obra_no_calificada;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
