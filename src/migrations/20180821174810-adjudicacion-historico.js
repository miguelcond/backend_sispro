'use strict';

module.exports = {
  up: (migration, DataType) => {
    return migration.addColumn('adjudicacion', 'version',{
      type: DataType.INTEGER,
      xlabel: 'Versión de la Adjudicación',
      allowNull: false,
      defaultValue: 1
    },{
      returning: true, raw: true
    }).catch((e)=>{
      console.error('E: addColumn',e.message);
    }).finally((resp)=>{
      return migration.bulkUpdate('adjudicacion', { version: 1 },
      {},{
        returning: true, raw: true
      });
    }).then(()=>{
      return migration.createTable('adjudicacion_historico', {
        id_adjudicacion: {
          type: DataType.INTEGER,
          primaryKey: true,
          // autoIncrement: true,
          xlabel: 'ID'
        },
        fid_proyecto: {
          type: DataType.INTEGER,
          xlabel: 'Proyecto',
          allowNull: true
        },
        version: {
          type: DataType.INTEGER,
          primaryKey: true,
          xlabel: 'Version'
        },
        referencia: {
          type: DataType.STRING,
          xlabel: 'Referencia adjudicación',
          allowNull: true
        },
        monto: {
          type: DataType.FLOAT,
          xlabel: 'Referencia adjudicación',
          allowNull: true
        },
        monto_supervision: {
          type: DataType.FLOAT,
          xlabel: 'Monto según items de supervision',
          allowNull: true
        },
        doc_adjudicacion: {
          type: DataType.STRING,
          xlabel: 'Documento adjudicación',
          allowNull: true
        },
        doc_especificaciones_tecnicas: {
          type: DataType.STRING(100),
          xlabel: 'Especificaciones técnicas items',
          allowNull: true
        },
        orden_proceder: {
          type: DataType.DATE,
          xlabel: 'Orden de proceder',
          allowNull: true
        },
        estado_adjudicacion: {
          type: DataType.STRING(50),
          xlabel: 'Estado de la adjudicación',
          allowNull: false,
          defaultValue: 'ADJUDICADO_FDI'
        },
        tipo_modificacion: {
          type: DataType.STRING(50),
          xlabel: 'Tipo de modificación',
          allowNull: true
        },
        plazo_ampliacion: {
          type: DataType.JSON,
          xlabel: 'Ampliación de plazo',
          allowNull: 'true'
        },
        doc_respaldo_modificacion: {
          type: DataType.STRING(100),
          xlabel: 'Documento de respaldo a la modificación',
          allowNull: true
        },
        fecha_modificacion: {
          type: DataType.DATE,
          xlabel: 'Fecha de modificación de volúmenes y plazos',
          allowNull: true
        },
        estado: {
          type: DataType.STRING(),
          defaultValue: 'ACTIVO',
          allowNull: false,
          validate: {
            isIn: {
              args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
              msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
            }
          }
        },
        _usuario_creacion: {
          type: DataType.INTEGER,
          xlabel: 'Usuario de creación',
          allowNull: false
        },
        _usuario_modificacion: {
          type: DataType.INTEGER,
          xlabel: 'Usuario de modificación',
          allowNull: true
        },
        _fecha_creacion: {
          type: DataType.DATE,
          xlabel: 'Fecha de creación',
          allowNull: true
        },
        _fecha_modificacion: {
          type: DataType.DATE,
          xlabel: 'Fecha de modificación',
          allowNull: true
        },
      });
    }).then((resp)=>{
      return migration.changeColumn(
            'adjudicacion',
            'tipo_modificacion',
            {
              type: DataType.STRING(50),
              xlabel: 'Tipo de modificación',
              allowNull: true,
              references: {
                model: 'parametrica',
                key: 'codigo',
              },
              onUpdate: 'CASCADE',
              onDelete: 'SET NULL',
            }
      );
    }).then((resp)=>{
      return migration.changeColumn(
            'adjudicacion_historico',
            'tipo_modificacion',
            {
              type: DataType.STRING(50),
              xlabel: 'Tipo de modificación',
              allowNull: true,
              references: {
                model: 'parametrica',
                key: 'codigo',
              },
              onUpdate: 'CASCADE',
              onDelete: 'SET NULL',
            }
      );
    }).catch((e)=>{
      console.log('E', e.message);
    });

  },

  down: (migration, DataTypes) => {

    const sql = (
      `DROP TABLE IF EXISTS adjudicacion_historico;`
    );
    return migration.sequelize.query(sql);
  }
};
