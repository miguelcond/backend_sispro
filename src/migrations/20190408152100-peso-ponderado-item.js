'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('item');
    return migration.addColumn('item', 'peso_ponderado', {
      type: DataTypes.FLOAT,
      xlabel: 'Peso ponderado',
      allowNull: false,
      defaultValue: 1
    }, {
      returning: true, raw: true
    }).then((resp) => {
      return migration.addColumn('item_historico', 'peso_ponderado', {
        type: DataTypes.FLOAT,
        xlabel: 'Peso ponderado',
        allowNull: false,
        defaultValue: 1
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      return migration.addColumn('item_temp', 'peso_ponderado', {
        type: DataTypes.STRING(32),
        xlabel: 'Peso ponderado',
        allowNull: false,
        defaultValue: 1
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      return migration.addColumn('item_temp', 'fpeso_ponderado', {
        type: DataTypes.FLOAT,
        xlabel: 'Peso ponderado',
        allowNull: false,
        defaultValue: 1
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      console.timeEnd('item');
    }).catch((e) => {
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE item DROP COLUMN IF EXISTS peso_ponderado;
      ALTER TABLE item_historico DROP COLUMN IF EXISTS peso_ponderado;
      ALTER TABLE item_temp DROP COLUMN IF EXISTS peso_ponderado;
      ALTER TABLE item_temp DROP COLUMN IF EXISTS fpeso_ponderado;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
