'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 12, 17);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 12, 7);`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};