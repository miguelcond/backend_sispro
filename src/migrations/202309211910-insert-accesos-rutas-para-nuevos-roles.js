'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
//insercion bandejas pendientes en roles jefe_cierre y tecnico_cierre
    const sql = (
       `INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, false, 'ACTIVO', 1, NULL, now(), now(), 21, 11);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, false, false, 'ACTIVO', 1, NULL, now(), now(), 21, 12);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 21, 13);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 21, 21);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(false, false, true, false, 'ACTIVO', 1, NULL, now(), now(), 21, 22);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 21, 25);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 21, 26);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, true, false, 'ACTIVO', 1, NULL, now(), now(), 21, 24);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 21, 28);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 21, 17);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 21, 7);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 21, 10);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 7);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, false, 'ACTIVO', 1, NULL, now(), now(), 22, 8);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 9);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 10);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, false, 'ACTIVO', 1, NULL, now(), now(), 22, 11);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 12);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 13);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 14);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 15);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 16);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, false, 'ACTIVO', 1, NULL, now(), now(), 22, 17);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 18);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 21);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(false, false, true, false, 'ACTIVO', 1, NULL, now(), now(), 22, 22);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 22, 26);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, true, false, 'ACTIVO', 1, NULL, now(), now(), 22, 27);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 22, 28);
        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, true, true, true, 'ACTIVO', 1, NULL, now(), now(), 22, 29);


        `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};