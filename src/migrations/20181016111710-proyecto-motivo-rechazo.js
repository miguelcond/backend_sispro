'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `ALTER TABLE proyecto ALTER COLUMN motivo_rechazo TYPE text;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};
