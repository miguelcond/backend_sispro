'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `alter table proyecto add column cartera int4 null;
       alter table proyecto add foreign key (cartera) references cartera;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};