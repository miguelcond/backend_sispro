'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    // const estado = require('../models/flujo/estado');
    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      acciones: '{"13":[{"tipo":"ventana","ruta":"/temporal"},{"tipo":"boton","label":"Enviar","class":"btn-success","icon":"fa-send","mensaje":"","estado":"MODIFICADO_FDI"}],"15":[{"tipo":"boton","label":"Guardar","class":"btn-primary","icon":"fa-save","mensaje":"Guardar datos del proyecto","estado":"EN_EJECUCION_FDI","guardar":true}],"16":[{"tipo":"ventana","ruta":"/temporal"},{"tipo":"boton","label":"Guardar","class":"btn-primary","icon":"fa-save","mensaje":"Guardar datos del proyecto","estado":"EN_EJECUCION_FDI","guardar":true},{"tipo":"boton","label":"Proceder","class":"btn-success","icon":"fa-check","mensaje":"Registrar orden de proceder","estado":"EN_EJECUCION_FDI","ejecutar":["$validarOrdenProceder"]}],"18":[{"tipo":"ventana","ruta":"/temporal"},{"icon": "fa-send", "tipo": "boton", "class": "btn-success", "label": "Cerrar Proyecto", "estado": "CERRADO_FDI", "mensaje": "Cerrar Proyecto"}]}',
      atributos: '{"13": ["estado_proyecto"], "15": ["cronograma_desembolsos"], "16": ["beneficiarios","orden_proceder", "doc_especificaciones_tecnicas", "modulos", "estado_proyecto", "adj"], "18": ["equipo_tecnico", "estado_proyecto"]}'
    },{
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'EN_EJECUCION_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};
