'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `ALTER TABLE public.supervision ADD anticipo numeric NULL;

        ALTER TABLE public.supervision ADD dpanticipo numeric NULL;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};