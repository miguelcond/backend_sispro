'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `ALTER TABLE public.supervision ADD retencion bool NULL DEFAULT true;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};