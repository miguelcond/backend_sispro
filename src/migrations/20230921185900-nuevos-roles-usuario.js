'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `INSERT INTO public.rol
        (nombre, descripcion, fid_rol_dependiente, peso, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion")
        VALUES('JEFE_CIERRE_FDI', 'Jefe de la Unidad de Cierre', NULL, 0, 'ACTIVO', 1, NULL, now(), now());
        INSERT INTO public.rol
        (nombre, descripcion, fid_rol_dependiente, peso, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion")
        VALUES('TECNICO_CIERRE', 'Tecnico Cierre', NULL, 0, 'ACTIVO', 1, NULL, now(), now());
        `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};