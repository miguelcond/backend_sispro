'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    return migration.bulkUpdate('contenido_formulario', {
      estructura: `[{"cols": [{"contenido": "El proyecto debe haber sido concebido de manera participativa y bajo pleno consenso por todos los actores involucrados de la jurisdicción municipal o del territorio de las Autonomía Indígena Originario Campesina."},{"contenido": "Acta de consenso sobre la  conformidad del EDTP, firmada por la comisión del proyecto, autoridades Indígena Originario Campesinas y/o  autoridades del GAM/AIOC <br>- Informe técnico de verificación en campo."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El proyecto debe beneficiar a pequeños productores de la agricultura familiar comunitaria de las comunidades más pobres."},{"contenido": "- Informe técnico de verificación en campo."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El estudio de Diseño Técnico de  Pre inversión debe estar en el formato y contenido de presentación de proyectos  productivos (menor o mediano) del FDI"},{"contenido": "- Contenido del EDTP."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El monto del proyecto deberá estar  enmarcado en las cuantías según la categoría de los proyectos (menor y mediano)."},{"contenido": "Estructura  del  financiamiento  del proyecto."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "La cantidad de beneficiarios debe enmarcarse en el factor de categorización de proyectos menores y medianos, según corresponda."},{"contenido": "- Lista de beneficiarios con N° de Cédula de Identidad y firma."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El tiempo de ejecución del proyecto no deberá exceder los límites establecidos para los proyectos según su categoría (menor y mediano)."},{"contenido": "- Cronograma  de  ejecución  del proyecto."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El proyecto no debe afectar a tierras, fiscales, reservas, derechos de vía y otros."},{"contenido": "- Declaración jurada notariada."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]}]`
    },{
      nombre: 'evaluacion',
      seccion: '2'
    },{
      returning: true, raw: true
    }).then((rows) => {
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    console.time('contenido_formulario');
    return migration.bulkUpdate('contenido_formulario', {
      estructura: `[{"cols": [{"contenido": "El proyecto debe haber sido concebido de manera participativa y bajo pleno consenso por todos los actores involucrados de la jurisdicción municipal o del territorio de las Autonomía Indígena Originario Campesina."},{"contenido": "Acta de consenso sobre la  conformidad del EDTP, firmada por la comisión del proyecto, autoridades Indígena Originario Campesinas y/o  autoridades del GAM/AIOC <br>- Informe técnico de verificación en campo."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El proyecto debe beneficiar a pequeños productores de la agricultura familiar comunitaria de las comunidades más pobres."},{"contenido": "- Informe técnico de verificación en campo."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El estudio de Diseño Técnico de  Pre inversión debe estar en el formato y contenido de presentación de proyectos  productivos (menor o mediano) del FDI"},{"contenido": "- Contenido del EDTP."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El monto del proyecto deberá estar  enmarcado en las cuantías según la categoría de los proyectos (menor y mediano)."},{"contenido": "Estructura  del  financiamiento  del proyecto."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "La cantidad de beneficiarios debe enmarcarse en el factor de categorización de proyectos menores y medianos, según corresponda."},{"contenido": "- Lista de beneficiarios con N° de Cédula de Identidad y firma."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El tiempo de ejecución del proyecto no deberá exceder los límites establecidos para los proyectos según su categoría (menor y mediano)."},{"contenido": "- Cronograma  de  ejecución  del proyecto."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]},
          {"cols": [{"contenido": "El proyecto no debe afectar a tierras, fiscales, reservas, derechos de vía y otros."},{"contenido": "- Declaración jurada notariada."},{ "contenido": { "type": "radio", "value": false, "name": "CUMPLE" } },{ "contenido": { "type": "radio", "value": true, "name": "NO_CUMPLE" } }]}]`
    },{
      nombre: 'evaluacion',
      seccion: '2'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('contenido_formulario');
    }).finally(done);
  },
};
