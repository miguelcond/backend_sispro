module.exports = (sequelize, DataType) => {
  const comunidades = sequelize.define('comunidades', {
    fid: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'fid',
      autoIncrement: true
    },
    departamento: {
      type: DataType.STRING(64),
      xlabel: 'departamento',
      allowNull: false
    },
    provincia: {
      type: DataType.STRING(64),
      xlabel: 'provincia',
      allowNull: true
    },
    municipio: {
      type: DataType.STRING(64),
      xlabel: 'municipio',
      allowNull: true
    },
    comunidad: {
      type: DataType.STRING(64),
      xlabel: 'comunidad',
      allowNull: true
    },
    latitud: {
      type: DataType.STRING(64),
      xlabel: 'latitud',
      allowNull: true
    },
    longitud: {
      type: DataType.STRING(64),
      xlabel: 'longitud',
      allowNull: true
    },
    x_long: {
      type: DataType.STRING(64),
      xlabel: 'x_long',
      allowNull: true
    },
    y_lat: {
      type: DataType.STRING(64),
      xlabel: 'y_lat',
      allowNull: true
    },
    cod_municipio: {
      type: DataType.STRING(64),
      xlabel: 'cod_municipio',
      allowNull: true
    },
    cod_provincia: {
      type: DataType.STRING(64),
      xlabel: 'cod_provincia',
      allowNull: true
    },
    cod_departamento: {
      type: DataType.STRING(64),
      xlabel: 'cod_departamento',
      allowNull: true
    }
  });

  
  return comunidades;
};
