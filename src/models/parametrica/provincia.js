/**
 * Módulo provincia
 *
 * @module
 *
 **/
module.exports = (sequelize, DataType) => {
  const provincia = sequelize.define('provincia', {
    codigo: {
      type: DataType.STRING(4),
      primaryKey: true,
      xlabel: 'Código de provincia',
      allowNull: false,
      validate: {
        len: {
          args: [4],
          msg: 'El campo \'código de provincia\' permite caracteres de 4.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código de provincia\' permite sólo numeros.'
        }
      }
    },
    nombre: {
      type: DataType.STRING(64),
      xlabel: 'Nombre de Provincia',
      allowNull: false,
      validate: {
        len: {
          args: [5, 25],
          msg: 'El campo \'Nombre de provincia\' permite un mínimo de 5 caracter y un máximo de 25 caracteres'
        },
        is: {
          args: /^[0-9|A-Z|-|-|.]+$/i,
          msg: 'El campo \'Nombre de provincia\' permite sólo letras, números, guiones y puntos.'
        }
      }
    },
    codigo_departamento: {
      type: DataType.STRING(2),
      xlabel: 'Código de Departamento',
      allowNull: false,
      validate: {
        len: {
          args: [2],
          msg: 'El campo \'código de provincia\' permite caracteres de 2.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código de provincia\' permite sólo numeros.'
        }
      }
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'provincia',
    classMethods: {}
  });

  Object.assign(provincia, {
    associate: function (models) {
      models.provincia.belongsTo(models.departamento, {
        as: 'departamento',
        foreignKey: 'codigo_departamento',
        targetKey: 'codigo'
      });
      models.provincia.hasMany(models.municipio, {
        as: 'municipio',
        foreignKey: {name: 'codigo_provincia', allowNull: true}
      });
    }
  });

  return provincia;
};
