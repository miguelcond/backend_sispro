module.exports = (sequelize, DataType) => {
  const cierre = sequelize.define('cierre', {
    id_cierre: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'id_cierre',
      autoIncrement: true
    },
    fid_proyecto:{
      type: DataTypes.INTEGER,
      field: 'fid_proyecto',
      xlabel: 'Proyecto',
      allowNull: false
    },
    plazo_cierre: {
      type: DataType.INTEGER,
      xlabel: 'Plazo cierre de proyecto',
      allowNull: false,
      defaultValue: 60
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'cierre',
    classMethods: {}
  });

  Object.assign(cierre, {
    associate: (models) => {
	    models.cierre.belongsTo(models.proyecto, {
	        as: 'proyecto',
	        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
	        targetKey: 'id_proyecto'
	    });
    }
  });

  return cierre;
};