/**
 * Módulo para adjudicacion
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const adjudicacion = sequelize.define('adjudicacion', {
    id_adjudicacion: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto',
      allowNull: true
    },
    version: {
      type: DataType.INTEGER,
      xlabel: 'Version'
    },
    referencia: {
      type: DataType.STRING,
      xlabel: 'Referencia adjudicación',
      allowNull: true
    },
    monto: {
      type: DataType.FLOAT,
      xlabel: 'Referencia adjudicación',
      allowNull: true
    },
    monto_supervision: {
      type: DataType.FLOAT,
      xlabel: 'Monto según items de supervision',
      allowNull: true
    },
    doc_adjudicacion: {
      type: DataType.STRING,
      xlabel: 'Documento adjudicación',
      allowNull: true
    },
    doc_especificaciones_tecnicas: {
      type: DataType.STRING(100),
      xlabel: 'Especificaciones técnicas items',
      allowNull: true
    },
    documentos: {
      type: DataType.JSON,
      xlabel: 'Documentos',
      allowNull: 'true'
    },
    orden_proceder: {
      type: DataType.DATE,
      xlabel: 'Orden de proceder',
      allowNull: true
    },
    estado_adjudicacion: {
      type: DataType.STRING(50),
      xlabel: 'Estado de la adjudicación',
      allowNull: false,
      defaultValue: 'ADJUDICADO_FDI'
    },
    tipo_modificacion: {
      type: DataType.STRING(50),
      xlabel: 'Tipo de modificación',
      allowNull: true
    },
    plazo_ampliacion: {
      type: DataType.JSON,
      xlabel: 'Ampliación de plazo',
      allowNull: 'true'
    },
    doc_respaldo_modificacion: {
      type: DataType.STRING(100),
      xlabel: 'Documento de respaldo a la modificación',
      allowNull: true
    },
    fecha_modificacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de modificación de volúmenes y plazos',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'adjudicacion',
    classMethods: {}
  });

  Object.assign(adjudicacion, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.adjudicacion.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
        targetKey: 'id_proyecto'
      });
      models.adjudicacion.hasMany(models.supervision, {
        as: 'supervision',
        foreignKey: 'fid_adjudicacion'
      });
      models.adjudicacion.belongsTo(models.estado, {
        as: 'estado_actual',
        foreignKey: 'estado_adjudicacion',
        targetKey: 'codigo'
      });
      models.adjudicacion.belongsTo(models.parametrica, {
        as: 'tipo_modificacion_adjudicacion',
        foreignKey: { name: 'tipo_modificacion', xchoice: 'tipos', xlabel: 'Tipo Modificación' },
        targetKey: 'codigo'
      });
      models.adjudicacion.hasMany(models.adjudicacion_historico, {
        as: 'adjudicacion_historico',
        foreignKey: 'id_adjudicacion'
      });
      models.adjudicacion.hasMany(models.transicion, {
        as: 'transicion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.adjudicacion.hasOne(models.transicion, {
        as: 'observacion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
    }
  });

  return adjudicacion;
};
