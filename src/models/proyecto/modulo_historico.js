/**
 * Módulo par documento
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const moduloHistorico = sequelize.define('modulo_historico', {
    id_modulo: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'ID'
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto',
      allowNull: true
    },
    fid_adjudicacion: {
      type: DataType.INTEGER,
      xlabel: 'Adjudicacion',
      allowNull: true
    },
    version: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'Version'
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: true
    },
    precio: {
      type: DataType.FLOAT,
      xlabel: 'Precio de items del módulo',
      allowNull: false,
      defaultValue: 0
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    classMethods: {}
  });

  Object.assign(moduloHistorico, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.modulo_historico.belongsTo(models.proyecto_historico, {
        as: 'proyecto',
        foreignKey: ['fid_proyecto', 'version']
      });
      models.modulo_historico.belongsTo(models.adjudicacion, {
        as: 'adjudicacion',
        foreignKey: { name: 'fid_adjudicacion', xchoice: 'adjudicaciones', xlabel: 'Adjudicacion' }
      });
      models.modulo_historico.hasMany(models.item_historico, {
        as: 'items',
        foreignKey: 'fid_modulo'
      });
    }
  });

  return moduloHistorico;
};
