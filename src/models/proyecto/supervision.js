/**
 * Módulo para supervision
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const supervision = sequelize.define('supervision', {
    id_supervision: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto',
      allowNull: false
    },
    fid_adjudicacion: {
      type: DataType.INTEGER,
      xlabel: 'Adjudicacion',
      allowNull: false
    },
    nro_supervision: {
      type: DataType.INTEGER,
      xlabel: 'Nro supervición',
      allowNull: false
    },
    fecha_inicio: {
      type: DataType.DATE,
      xlabel: 'Fecha inicio',
      allowNull: true
    },
    fecha_fin: {
      type: DataType.DATE,
      xlabel: 'Fecha fin',
      allowNull: true
    },
    fecha_pago: {
      type: DataType.DATE,
      xlabel: 'Fecha Pago',
      allowNull: true
    },
    path_comprobante: {
      type: DataType.STRING(),
      xlabel: 'Comprobante de Pago',
      allowNull: true
    },
    informe: {
      type: DataType.STRING(3000),
      xlabel: 'Informe técnico de conclusiones',
      allowNull: true
    },
    estado_supervision: {
      type: DataType.STRING(50),
      defaultValue: 'REGISTRO_PLANILLA_FDI',
      allowNull: false
    },
    uuid: {
      type: DataType.STRING(36),
      allowNull: true
    },
    tipo_descuento_anticipo: {
      type: DataType.STRING(15),
      allowNull: true,
      validate: {
        isIn: {
          args: [['TOTAL', 'PROGRESIVO']],
          msg: 'El campo tipo descuento de anticipo sólo permite valores: TOTAL o PROGRESIVO.'
        }
      }
    },
    desembolso: {
      type: DataType.FLOAT,
      xlabel: 'Desembolso',
      allowNull: true
    },
    descuento_anticipo: {
      type: DataType.FLOAT,
      xlabel: 'Descuento por anticipo',
      allowNull: true
    },
    mano_obra_calificada: {
      type: DataType.INTEGER,
      xlabel: 'Mano de obra calificada',
      allowNull: false,
      defaultValue: 0
    },
    mano_obra_no_calificada: {
      type: DataType.INTEGER,
      xlabel: 'Mano de obra no calificada',
      allowNull: false,
      defaultValue: 0
    },
    anticipo: {
      type: DataType.INTEGER,
      xlabel: 'Anticipo',
      allowNull: false,
      defaultValue: 0
    },
    dpanticipo: {
      type: DataType.INTEGER,
      xlabel: 'dpanticipo',
      allowNull: false,
      defaultValue: 0
    },
    retencion: {
      type: DataType.BOOLEAN,
      xlabel: 'retencion',
      defaultValue: true
    },
    // La versión del proyecto cuando se aprobó la planilla o supervisión
    version_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Versión de la Supervisión',
      allowNull: true
    },
    // La versión de la adjudicación cuando se aprobó la planilla o supervisión
    version_adjudicacion: {
      type: DataType.INTEGER,
      xlabel: 'Versión de la adjudicación',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    fecha_creacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de creación movil',
      allowNull: true
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    },
    fecha_modificacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de modificación movil',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'supervision',
    classMethods: {}
  });

  Object.assign(supervision, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.supervision.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
        targetKey: 'id_proyecto'
      });
      models.supervision.belongsTo(models.adjudicacion, {
        as: 'adjudicacion',
        foreignKey: { name: 'fid_adjudicacion', xchoice: 'adjudicaciones', xlabel: 'Adjudicacion' },
        targetKey: 'id_adjudicacion'
      });
      models.supervision.hasMany(models.computo_metrico, {
        as: 'computo_metrico',
        foreignKey: 'fid_supervision'
      });
      models.supervision.belongsTo(models.estado, {
        as: 'estado_actual',
        foreignKey: 'estado_supervision',
        targetKey: 'codigo'
      });
      models.supervision.hasMany(models.transicion, {
        as: 'transicion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.supervision.hasOne(models.transicion, {
        as: 'observacion_proceso',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.supervision.hasMany(models.foto_supervision, {
        as: 'fotos_supervision',
        foreignKey: 'fid_supervision',
        constraints: false
      });
    },
    // extra options para aplicacion movil
    supervisionIncluye: (consulta) => sequelize.models.supervision.findAndCount({
      include: [{
        attributes: ['id_adjudicacion', 'referencia', 'monto'],
        model: sequelize.models.adjudicacion,
        as: 'adjudicacion'
      }, {
        model: sequelize.models.computo_metrico,
        as: 'computo_metrico',
        where: {
          estado: 'ACTIVO'
        },
        required: false
      }, {
        attributes: ['id_foto_supervision', 'fid_item', 'coordenadas', 'uuid', 'fid_supervision', 'path_fotografia', 'hash', 'tipo', '_fecha_modificacion', '_usuario_creacion'],
        model: sequelize.models.foto_supervision,
        as: 'fotos_supervision',
        where: {
          tipo: 'TF_SUPERVISION',
          estado: 'ACTIVO'
        },
        required: false
      }, {
        attributes: [['observacion', 'mensaje']],
        model: sequelize.models.transicion,
        as: 'observacion_proceso',
        where: {
          fid_usuario_asignado: null
        },
        required: false
      }],
      where: consulta
    }),
    maxSupervision: (idProyecto, idAdjudicacion, t) => sequelize.models.supervision.findOne({
      attributes: [[sequelize.fn('coalesce', sequelize.fn('max', sequelize.col('nro_supervision')), 0), 'nro']],
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion
      },
      transaction: t
    })
  });

  return supervision;
};
