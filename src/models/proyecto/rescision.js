/**
 * Módulo para registrar las rescisiones de contrato.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const rescision = sequelize.define('rescision', {
    id_rescision: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    proyectos: {
      type: DataType.ARRAY(DataType.INTEGER),
      xlabel: 'Contraseña',
      allowNull: false
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'rescision',
    classMethods: {}
  });

  return rescision;
};
