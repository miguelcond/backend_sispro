/**
 * Módulo para item
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const itemTemp = sequelize.define('item_temp', {
    id_item_temp: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    componente: {
      type: DataType.STRING,
      xlabel: 'Modulo',
      allowNull: false
    },
    modulo: {
      type: DataType.STRING,
      xlabel: 'Modulo',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: false
    },
    unidad: {
      type: DataType.STRING(32),
      xlabel: 'Unidad de medida',
      allowNull: false
    },
    cantidad: {
      type: DataType.STRING(32), // Tipo string para verificar formato y luego convertir a float
      xlabel: 'Cantidad',
      allowNull: false
    },
    fcantidad: {
      type: DataType.FLOAT,
      xlabel: 'Cantidad',
      allowNull: true
    },
    precio_unitario: {
      type: DataType.STRING(32), // Tipo string para verificar formato y luego convertir a float
      xlabel: 'Precio',
      allowNull: false
    },
    fprecio_unitario: {
      type: DataType.FLOAT,
      xlabel: 'Precio',
      allowNull: true
    },
    peso_ponderado: {
      type: DataType.STRING(32), // Tipo string para verificar formato y luego convertir a float
      xlabel: 'Peso ponderado',
      allowNull: false,
      defaultValue: 1
    },
    fpeso_ponderado: {
      type: DataType.FLOAT,
      xlabel: 'Peso ponderado',
      allowNull: false,
      defaultValue: 1
    }
  }, {
    freezeTableName: true,
    tableName: 'item_temp',
    classMethods: {}
  });

  return itemTemp;
};
