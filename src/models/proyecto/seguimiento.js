module.exports = (sequelize, DataType) => {
  const seguimiento = sequelize.define('seguimiento', {
    id_seguimiento: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'id_seguimiento',
      autoIncrement: true
    },
    fid_proyecto:{
      type: DataTypes.INTEGER,
      field: 'fid_proyecto',
      xlabel: 'Proyecto',
      allowNull: false
    },
    situacion_general: {
      type: DataType.text,
      xlabel: 'situacion general',
      allowNull: false
    },
    objetivo_general: {
      type: DataType.text,
      xlabel: 'objetivo general',
      allowNull: true
    },
    resultados: {
      type: DataType.text,
      xlabel: 'resultados',
      allowNull: true
    },
    fecha_acta_provisional: {
      type: DataType.DATE,
      xlabel: 'fecha_acta_provisional',
      allowNull: true
    },
    doc_acta_provisional: {
      type: DataType.STRING(100),
      xlabel: 'Documento acta provisional',
      allowNull: true
    },
    fecha_acta_definitiva: {
      type: DataType.DATE,
      xlabel: 'fecha_acta_definitiva',
      allowNull: true
    },
    doc_acta_definitiva: {
      type: DataType.STRING(100),
      xlabel: 'Documento acta definitiva',
      allowNull: true
    },
    fecha_informe: {
      type: DataType.DATE,
      xlabel: 'fecha_informe',
      allowNull: true
    },
    doc_informe: {
      type: DataType.STRING(100),
      xlabel: 'Documento informe',
      allowNull: true
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'seguimiento',
    classMethods: {}
  });

  Object.assign(seguimiento, {
    associate: (models) => {
	    models.seguimiento.belongsTo(models.proyecto, {
	        as: 'proyecto',
	        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
	        targetKey: 'id_proyecto'
	    });
    }
  });

  return seguimiento;
};