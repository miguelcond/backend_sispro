module.exports = (sequelize, DataType) => {
  const cartera = sequelize.define('cartera', {
    id_cartera: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'id_cartera',
      autoIncrement: true
    },
    num_cartera: {
      type: DataType.INTEGER,
      xlabel: 'Numero de cartera',
      allowNull: false
    },
    descripcion: {
      type: DataType.STRING(100),
      xlabel: 'Descripción',
      allowNull: true
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'cartera',
    classMethods: {}
  });

  Object.assign(cartera, {
    associate: (models) => {
    }
  });

  return cartera;
};
