
module.exports = (sequelize, DataTypes) => {
  const plantilla = sequelize.define('plantilla', {
    id_plantilla: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      xlabel: 'ID',
      autoIncrement: true
    },
    estructura: {
      type: DataTypes.JSONB,
      field: 'estructura',
      xlabel: 'Estructura',
      allowNull: true
    },
    version: {
      type: DataTypes.INTEGER,
      field: 'version',
      xlabel: 'Version',
      allowNull: true
    },
    estado: {
      type: DataTypes.STRING(30),
      field: 'estado',
      xlabel: 'Estado',
      allowNull: false,
      defaultValue: 'ELABORADO',
      validate: {
        isIn: { args: [['ELABORADO', 'PUBLICADO', 'INACTIVO', 'ELIMINADO']], msg: 'El campo estado sólo permite valores: ELABORADO, PUBLICADO, INACTIVO o ELIMINADO' }
      }
    },
    _usuario_creacion: {
      type: DataTypes.INTEGER,
      field: '_usuario_creacion',
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataTypes.INTEGER,
      field: '_usuario_modificacion',
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'plantilla',
    classMethods: {}
  });

  Object.assign(plantilla, {
    associate: (models) => {
      // plantilla.belongsTo(models.tipo_documento, {
      //   as: 'tipo_documento',
      //   foreignKey: {
      //     name: 'fid_tipo_documento',
      //     allowNull: false,
      //   },
      // });
    }
  });

  return plantilla;
};
