/**
 * Módulo para transicion
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const transicion = sequelize.define('transicion', {
    id_transicion: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    objeto_transicion: {
      type: DataType.INTEGER,
      xlabel: 'Objeto',
      allowNull: false
    },
    tipo_transicion: {
      type: DataType.STRING(64),
      xlabel: 'Tipo Objeto',
      allowNull: false
    },
    codigo_estado: {
      type: DataType.STRING,
      xlabel: 'Estado',
      allowNull: false
    },
    observacion: {
      type: DataType.STRING,
      xlabel: 'Observación',
      allowNull: true
    },
    fid_usuario: {
      type: DataType.INTEGER,
      xlabel: 'Usuario',
      allowNull: false
    },
    fid_usuario_rol: {
      type: DataType.INTEGER,
      xlabel: 'Rol de Usuario',
      allowNull: false
    },
    fid_usuario_asignado: {
      type: DataType.INTEGER,
      xlabel: 'Usuario asignado',
      allowNull: true
    },
    fid_usuario_asignado_rol: {
      type: DataType.INTEGER,
      xlabel: 'Rol de Usuario asignado',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'transicion',
    classMethods: {}
  });

  Object.assign(transicion, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.transicion.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: 'objeto_transicion',
        targetKey: 'id_proyecto',
        constraints: false
      });
      models.transicion.belongsTo(models.estado, {
        as: 'estado_objeto',
        foreignKey: 'codigo_estado',
        targetKey: 'codigo'
      });
      models.transicion.belongsTo(models.usuario, {
        as: 'usuario',
        foreignKey: 'fid_usuario',
        targetKey: 'id_usuario',
        constraints: false
      });
      models.transicion.belongsTo(models.rol, {
        as: 'usuario_rol',
        foreignKey: 'fid_usuario_rol',
        targetKey: 'id_rol',
        constraints: false
      });
      models.transicion.belongsTo(models.usuario, {
        as: 'usuario_asignado',
        foreignKey: 'fid_usuario_asignado',
        targetKey: 'id_usuario',
        constraints: false
      });
      models.transicion.belongsTo(models.rol, {
        as: 'usuario_asignado_rol',
        foreignKey: 'fid_usuario_asignado_rol',
        targetKey: 'id_rol',
        constraints: false
      });
      models.transicion.belongsTo(models.supervision, {
        as: 'supervision',
        foreignKey: 'objeto_transicion',
        targetKey: 'id_supervision',
        constraints: false
      });
      models.transicion.belongsTo(models.adjudicacion, {
        as: 'adjudicacion',
        foreignKey: 'objeto_transicion',
        targetKey: 'id_adjudicacion',
        constraints: false
      });
    }
  });

  return transicion;
};
