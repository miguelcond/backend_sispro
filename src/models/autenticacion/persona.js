/**
 * Módulo que mapea las PERSONAS existentes, cada persona sólo debería estar
 * registrada una vez en esta tabla.
 *
 * @module
 *
 */

module.exports = (sequelize, DataType) => {
  const persona = sequelize.define('persona', {
    id_persona: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'Id de la persona'
    },
    tipo_documento: {
      type: DataType.STRING(50),
      xlabel: 'Tipo de documento',
      unique: 'uniqueSelectedItem',
      allowNull: false
    },
    tipo_persona: {
      type: DataType.STRING(8),
      xlabel: 'Persona nacional, extranjero',
      allowNull: true
    },
    complemento_visible: {
      type: DataType.BOOLEAN,
      xlabel: 'Complemento visible',
      allowNull: false,
      defaultValue: false
    },
    numero_documento: {
      type: DataType.STRING(20),
      xlabel: 'Documento de identidad',
      allowNull: false,
      unique: 'uniqueSelectedItem',
      validate: {
        len: { args: [5, 20], msg: 'El campo \'Documento de identidad\' permite un mínimo de 5 caracter y un máximo de 25 caracteres' },
        is: { args: /^[0-9|A-Z|-|-|.]+$/i, msg: 'El campo \'Documento de identidad\' permite sólo letras, números, guiones y puntos.' }
      }
    },
    complemento: {
      type: DataType.STRING(2),
      xlabel: 'Complemento del documento',
      unique: 'uniqueSelectedItem',
      validate: {
        len: { args: [0, 20], msg: 'El campo \'Complemento del documento\' permite un mínimo de 0 caracteres y un máximo de 20 caracteres' }
      }
    },
    nombres: {
      type: DataType.STRING(100),
      xlabel: 'Nombres',
      allowNull: false,
      validate: {
        len: { args: [1, 100], msg: 'El campo \'Nombres\' permite un mínimo de 1 caracter y un máximo de 100 caracteres' },
        is: { args: /^[A-Z|a-z|À-Ö|Ø-ö|ø-ÿ|ñ|Ñ|\-|'|´| ]+$/i, msg: 'El campo \'Nombres\' permite sólo letras' }
      }
    },
    primer_apellido: {
      type: DataType.STRING(100),
      xlabel: 'Primer apellido',
      allowNull: true,
      validate: {
        len: { args: [0, 100], msg: 'El campo \'Primer apellido\' permite un mínimo de 1 caracter y un máximo de 100 caracteres' },
        is: { args: /^[A-Z|a-z|À-Ö|Ø-ö|ø-ÿ|ñ|Ñ|\-|'|´| ]+$/i, msg: 'El campo \'Primer apellido\' permite sólo letras' }
      }
    },
    segundo_apellido: {
      type: DataType.STRING(100),
      xlabel: 'Segundo apellido',
      allowNull: true,
      validate: {
        len: { args: [0, 100], msg: 'El campo \'Segundo apellido\' permite un máximo de 100 caracteres' },
        is: { args: /^[A-Z|a-z|À-Ö|Ø-ö|ø-ÿ|ñ|Ñ|\-|'|´| ]+$/i, msg: 'El campo \'Segundo apellido\' permite solo letras' }
      }
    },
    fecha_nacimiento: {
      type: DataType.DATEONLY,
      xlabel: 'Fecha de nacimiento',
      unique: 'uniqueSelectedItem',
      allowNull: false
    },
    lugar_nacimiento_pais: {
      type: DataType.STRING(50),
      xlabel: 'País de nacimiento',
      allowNull: true,
      validate: {
        len: { args: [0, 50], msg: 'El campo \'País de nacimiento\' permite un máximo de 50 caracteres' },
        is: { args: /^[A-Z|a-z|À-Ö|Ø-ö|ø-ÿ|ñ|Ñ|\-|'|´| ]+$/i, msg: 'El campo \'País de nacimiento\' permite solo letras' }
      }
    },
    lugar_nacimiento_departamento: {
      type: DataType.STRING(50),
      xlabel: 'Departamento de nacimiento',
      allowNull: true,
      validate: {
        len: { args: [0, 50], msg: 'El campo \'Departamento de nacimiento\' permite un máximo de 100 caracteres' },
        is: { args: /^[A-Z|a-z|À-Ö|Ø-ö|ø-ÿ|ñ|Ñ|\-|'|´| ]+$/i, msg: 'El campo \'Departamento de nacimiento\' permite solo letras' }
      }
    },
    correo: {
      type: DataType.STRING(100),
      xlabel: 'Correo electrónico',
      allowNull: true,
      unique: false,
      validate: {
        isEmail: { args: true, msg: 'El campo \'Correo Electrónico\' no tiene el formato correcto' },
        len: { args: [4, 100], msg: 'El campo \'Correo Electrónico\' permite un mínimo de 4 caracteres y un máximo de 100 caracteres' }
      }
    },
    telefono: {
      type: DataType.STRING(20),
      xlabel: 'Teléfono',
      allowNull: true,
      validate: {
        len: { args: [7, 20], msg: 'El campo \'Teléfono\' permite un mínimo de 7 caracter y un máximo de 20 caracteres' },
        is: { args: /^[0-9|-|-|.]+$/i, msg: 'El campo \'Teléfono\' permite sólo números, guiones y puntos.' }
      }
    },
    direccion: {
      type: DataType.STRING(100),
      xlabel: 'Dirección',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'persona',
    classMethods: {}
  });

  Object.assign(persona, {
    associate: (models) => {
      models.persona.belongsTo(models.parametrica, {
        as: 'tipo_documento_identidad',
        foreignKey: { name: 'tipo_documento', unique: 'uniqueSelectedItem', allowNull: false },
        targetKey: 'codigo'
      });
    }
  });

  return persona;
};
