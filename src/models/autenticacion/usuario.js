/**
 * Módulo par usuario
 *
 * @module
 *
 **/
// import crypto from 'crypto';

module.exports = (sequelize, DataType) => {
  const usuario = sequelize.define('usuario', {
    id_usuario: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_institucion: {
      type: DataType.INTEGER,
      xlabel: 'Institución'
    },
    fid_persona: {
      type: DataType.INTEGER,
      xlabel: 'Persona'
    },
    nit: {
      type: DataType.STRING(20),
      xlabel: 'NIT'
    },
    usuario: {
      type: DataType.STRING(100),
      xlabel: 'Usuario',
      allowNull: false,
      unique: true,
      validate: {
        len: { args: [3, 100], msg: 'El campo \'Nombre de Usuario\' permite un mínimo de 3 caracteres y un máximo de 100 caracteres' }
      }
    },
    contrasena: {
      type: DataType.STRING,
      xlabel: 'Contraseña',
      allowNull: false,
      defaultValue: ''
    },
    fcod_departamento: {
      type: DataType.ARRAY(DataType.STRING(2)),
      xlabel: 'Contraseña',
      allowNull: false
    },
    codigo_contrasena: {
      type: DataType.STRING(8),
      xlabel: 'Código Contraseña',
      allowNull: true
    },
    fecha_expiracion: {
      type: DataType.DATE,
      xlabel: 'Fecha Expiración',
      allowNull: true
    },
    observaciones: {
      type: DataType.STRING(100),
      xlabel: 'Observaciones',
      allowNull: true,
      validate: {
        len: { args: [0, 100], msg: 'El campo \'Observaciones\' permite un máximo de 100 caracteres' }
      }
    },
    token: {
      type: DataType.STRING(50),
      xlabel: 'Certificado digital',
      allowNull: true,
      validate: {
        len: { args: [0, 50], msg: 'El campo \'Certificado digital\' permite un máximo de 50 caracteres.' },
        is: { args: /^([A-Z|a-z|0-9| ]|)+$/i, msg: 'El campo \'Certificado digital\' sólo permite letras sin tíldes.' }
      }
    },
    cargo: {
      type: DataType.STRING(),
      defaultValue: '',
      allowNull: true,
      validate: {
        len: { args: [0, 200], msg: 'El campo \'Cargo\' permite un máximo de 200 caracteres' }
      }
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'usuario',
    classMethods: {}
  });

  Object.assign(usuario, {
    associate: (models) => {
      usuario.belongsTo(models.persona, {
        as: 'persona',
        foreignKey: { name: 'fid_persona', xchoice: 'nombres', xlabel: 'Nombres' },
        targetKey: 'id_persona'
      });
      usuario.belongsTo(models.institucion, {
        as: 'institucion',
        foreignKey: { name: 'fid_institucion', xchoice: 'instituciones', xlabel: 'Instituciones' },
        targetKey: 'id_institucion'
      });
      usuario.hasMany(models.usuario_rol, { as: 'usuarios_roles', foreignKey: { name: 'fid_usuario', allowNull: false } });
    },
    buscarIncluye: (Persona, UsuarioRol, Rol, condicion) => {
      let subSql = `SELECT ARRAY(SELECT DISTINCT r.nombre FROM usuario_rol ur INNER JOIN rol r ON r.id_rol=ur.fid_rol WHERE ur.fid_usuario=id_usuario AND r.estado='ACTIVO' AND ur.estado='ACTIVO')`;
      return usuario.findAndCountAll({
        attributes: ['id_usuario', 'usuario', 'estado', 'observaciones', ['fcod_departamento', 'departamentos'], [sequelize.literal(`(${subSql})`), 'roles']],
        where: condicion.where,
        offset: condicion.offset,
        limit: condicion.limit,
        order: condicion.order,
        distinct: true,
        subQuery: condicion.subQuery,
        include: [{
          model: Persona,
          as: 'persona',
          attributes: ['numero_documento', 'correo', 'complemento', 'fecha_nacimiento', 'nombres', 'primer_apellido', 'segundo_apellido'],
          required: true
        },
        {
          model: UsuarioRol,
          as: 'usuarios_roles',
          attributes: [['fid_rol', 'id_rol'], 'fid_usuario'],
          separate: true,
          offset: null,
          where: {
            estado: 'ACTIVO'
          },
          include: [{
            model: Rol,
            as: 'rol',
            attributes: ['descripcion']
          }]
        }
        ]
      });
    },
    buscarIncluyeOne: (id, Persona, UsuarioRol, Rol) => usuario.findOne({
      attributes: ['id_usuario', 'usuario', 'fcod_departamento', 'cargo', 'observaciones', 'estado'],
      where: {
        id_usuario: id
      },
      include: [{
        model: Persona,
        as: 'persona',
        attributes: ['id_persona', 'numero_documento', 'correo', 'complemento', 'fecha_nacimiento', 'nombres', 'primer_apellido',
          'segundo_apellido', 'telefono', 'direccion', 'tipo_documento', 'estado']
      },
      {
        model: UsuarioRol,
        as: 'usuarios_roles',
        attributes: ['fid_rol'],
        where: {
          estado: 'ACTIVO'
        },
        include: [{
          model: Rol,
          as: 'rol',
          attributes: ['id_rol', 'nombre']
        }]
      }
      ]
    }),
    buscarIncluyePorPersona: (idPersona, Persona, UsuarioRol, Rol) => usuario.findOne({
      attributes: ['id_usuario', 'fid_persona', 'usuario', 'estado', '_fecha_creacion', '_fecha_modificacion'],
      where: {
        fid_persona: idPersona
      },
      include: [
        {
          model: Persona,
          as: 'persona',
          attributes: ['id_persona', 'numero_documento', 'correo', 'complemento', 'fecha_nacimiento', 'nombres', 'primer_apellido',
            'segundo_apellido', 'telefono', 'direccion', 'tipo_documento', 'estado', 'complemento_visible', 'tipo_persona']
        },
        {
          model: UsuarioRol,
          as: 'usuarios_roles',
          attributes: ['fid_rol'],
          where: {
            estado: 'ACTIVO'
          },
          include: [{
            model: Rol,
            as: 'rol',
            attributes: ['id_rol', 'nombre']
          }]
        }
      ]
    })
  });

  // Hash password usuario MD5 para eventos de actualizacion y creacion
  // const hashPasswordHook = (instance) => {
  //   if (!instance.changed('contrasena')) return false;
  //   if (instance.get('contrasena').length < 6) {
  //     throw new Error('La contraseña debe contener al menos 6 caracteres.');
  //   }
  //   const contrasena = instance.get('contrasena');
  //   const password = crypto.createHash('md5').update(contrasena).digest('hex');
  //   instance.set('contrasena', password);
  // };
  // usuario.beforeCreate((usuario, options) => {
  //  hashPasswordHook(usuario);
  //  usuario.usuario = usuario.usuario.toLowerCase();
  // });

  // usuario.beforeUpdate(hashPasswordHook);
  return usuario;
};
