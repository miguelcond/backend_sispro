import program from 'commander';
import config from './src/config/config';
import util from './src/lib/util';

program.version('0.1.0');

program.command('token [duracion]').action((duracion) => {
  const ID_USUARIO = 2; // SERV_PROYECTOS = 2
  const DURACION = duracion || 315360000; // 10 años
  const CLAVE = config().jwtSecret;
  const datosToken = { id_usuario: ID_USUARIO };
  process.stdout.write(` CREACIÓN DE TOKEN\n - Sistema: 'Khipus'.\n - Tiempo de expiración: ${DURACION} seg.\n\n`);
  const token = util.generarToken(datosToken, CLAVE, DURACION);
  process.stdout.write(`${token}\n\n`);
  process.exit(0);
});

program.parse(process.argv);
