# SQL Scripts

## Agregar nueva gestion de techos presupuestarios

```sql
DO $$ 
<<first_block>>
DECLARE
  _gestion VARCHAR := 2020;
  _cantidad INTEGER := 0;
BEGIN
  SELECT count(*) INTO _cantidad FROM techo_presupuestario tp WHERE gestion = _gestion::VARCHAR;
  IF _cantidad > 0
  THEN
    RAISE NOTICE 'Ya existen % datos para la gestion %', _cantidad, _gestion;
    RETURN;
  END IF;
 
  SELECT count(*) INTO _cantidad FROM techo_presupuestario tp WHERE gestion = (_gestion::INT - 1)::VARCHAR;
  IF _cantidad > 0 
  THEN
    RAISE NOTICE 'Generando datos para la gestion %', _gestion;

    INSERT INTO techo_presupuestario 
    (codigo,gestion,nombre_municipio,num_concejales,techo_presupuestal,monto_usado,monto_disponible,estado,"_usuario_creacion","_usuario_modificacion","_fecha_creacion","_fecha_modificacion")
    SELECT codigo,gestion::INT + 1,nombre_municipio,num_concejales,techo_presupuestal,0,monto_disponible,'ACTIVO',"_usuario_creacion","_usuario_modificacion",now(),now()
    FROM techo_presupuestario 
    WHERE gestion = (_gestion::INT - 1)::VARCHAR;

    RAISE NOTICE 'Regstros insertados %', _cantidad;
  ELSE
    -- display a message
    RAISE NOTICE '===========================================';
    RAISE NOTICE 'No se puede generar datos para la gestion %', _gestion;
    RAISE NOTICE 'Porque no hay datos de la gestion anterior.';
  END IF;
END first_block $$;

```